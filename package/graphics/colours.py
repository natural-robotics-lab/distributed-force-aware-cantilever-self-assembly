"""Colour RGB values"""
import copy

import numpy as np
import seaborn as sns
from matplotlib import cm
from matplotlib.colors import ListedColormap

BLUE = [0, 0.4470, 0.7410]       # Top of cantilever
GREEN = [0.466, 0.674, 0.188]    # Last element placed
PURPLE = [0.494, 0.184, 0.556]   # Element placed due to shear limit
CYAN = [0.301, 0.745, 0.933]     # Element placed due to longitudinal limit
RED = [0.635, 0.078, 0.184]      # Element placed in the wrong location
YELLOW = [0.929, 0.694, 0.125]   # Element being placed
ORANGE = [0.85, 0.325, 0.098]	 # Right y-axis colour
PEACH = [0.933, 0.824, 0.8]
GREY = '0.6'

BLUE_HEX = '#0070c2'
GREY_HEX = '#9a9a9a'

# Copy of basic colourmap in order to suppress warnings later on
BASIC_CMAP = copy.copy(cm.get_cmap("RdYlGn_r"))


# Custom colourmap
hot_cmap = cm.get_cmap('hot_r', 80)
cool_cmap = cm.get_cmap('summer', 100)

ALLOWABLE_CMAP = ListedColormap(np.vstack([cool_cmap(np.linspace(0, 1, 100))[:100, :],
                                           hot_cmap(np.linspace(0, 1, 80))[38:78, :]]))


def failure_cmap(allowable_limits, failure_limits):
    """Creates a colourmap of the following format:

    Black   ->  failure
    Red     ->  critical
    Green   ->  safe
    """

    cbar_height = 1000

    # Safety factor to 4 by default
    if failure_limits['moment'] is None:
        safety_factor = 4
    else:
        safety_factor = failure_limits['moment'] / allowable_limits['moment']
    allowable_height = int(cbar_height//safety_factor)
    failure_height = cbar_height-allowable_height

    # Define the partial colourmaps
    cool_cmap = cm.get_cmap('summer', allowable_height)  # All of cool
    hot_cmap = cm.get_cmap('hot_r', 2*failure_height)    # Top half of warm

    # Custom colours: green->yellow; orange->black; pink
    colours = np.vstack([cool_cmap(np.linspace(0, 1, allowable_height)),
                         hot_cmap(np.linspace(0, 1, 2*failure_height))[2*failure_height//2:]])

    # Define the custom colourmap
    custom_cmap = ListedColormap(colours)
    custom_cmap.set_over(CYAN + [1])

    # Define y axis
    ticks = [0, 1]
    ticklabels = ['0', 'Allow']

    if failure_limits['moment'] is not None:
        ticks.append(safety_factor)
        ticklabels.apppend('Fail')

    return custom_cmap, ticks, ticklabels, safety_factor


def colours_iterator(start=0):
    """Iterates through the colours in MATLAB order"""

    colours = [BLUE,
               ORANGE,
               YELLOW,
               PURPLE,
               GREEN,
               CYAN,
               RED]

    return iter(colours[start:])

# Custom seaborn colour palette


def custom_sns_palette(colours=None, start=0):
    """A seaborn colour palette with the same order as the colours iterator"""

    if colours is not None:
        raise NotImplementedError('Possibility of redefining colours here')
    else:
        colours_iter = colours_iterator(start)

    # Convert RGB to hex
    colours_hex = []
    for r, g, b in colours_iter:
        colours_hex.append("#{:02x}{:02x}{:02x}".format(int(r*256),
                                                        int(g*256),
                                                        int(b*256)))

    return sns.color_palette(colours_hex)
