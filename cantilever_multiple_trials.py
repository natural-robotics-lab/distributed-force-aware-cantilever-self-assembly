"""Runs multiple trials of the stochastic algorithm in one script

There are six modes, each called with different parameters:

    run
    -------------------------------
        Runs the local stochastic algorithm for specific limits a specified number
        of times.

        Inputs:
            algorithm_name      The name of the algorithm to run
            no_trials           The number of trials
            elements_per_trial  The number of elements placed per trial
            M_allowable         Allowable moment
            F_allowable         Allowable axial force
            element_delay       (PARALLEL ONLY) Number of steps between elements being added
        Outputs:
            Directory of the limit pair containing a .csv file per trial.
                For sequential algorithm, each file contains length, no. elements, and configuration
            for all stable configurations, as well as the maximum moment and axial force reached at
            all stages.
                For the parallel algorithm, the .csv file contains step number, length, number of
            elements (total , placed, active, and placed due to timeout), and the maximum M and F at
            that instant. There's also an additional .pbz2 file that contains the Cantilever object
            at each step to allow drawing later.


    process_limit_pair
    -------------------------------
        Processes the data for a single limit pair.

        Inputs:
            algorithm_name      The name of the algorithm to compare
            M_allowable         Allowable moment
            F_allowable         Allowable axial force
            elements_per_trial  (SEQUENTIAL ONLY) The expected number of elements per trial
            plots               Whether to 'show' or 'save' the plots
                                (use save_pdf / save_eps to also save vector images)
        Outputs:
            Graphs in the limit pair directory.

    process_parallel_delay0
    -------------------------------
        Processes the data for the parallel_delay0 trials.

        Outputs:
            Data in the directory.

    collate
    -------------------------------
        Collates the metadata from all the trials in the directory into a graph and boxplot

        Inputs:
            algorithm_name      The name of the algorithm to collate (without local, extended, etc.)
            elements_per_trial  (SEQUENTIAL ONLY) The expected number of elements per trial
            plots               Whether to 'show' or 'save' the plot
                                (use save_pdf / save_eps to also save vector images)
        Outputs:
            Boxplot showing maximum axial / moment exceptions
            Line graphs of all limit pairs, comparing local, message passing, and optimal

    compare_sequential
    -------------------------------
        Compares multiple versions of the sequential algorithm

        Inputs:
            algorithm names     The names of the algorithms we're comparing (without sequential)
            elements_per_trial  The expected number of elements per trial
            plots               Whether to 'show' or 'save' the plots
                                (use save_pdf / save_eps to also save vector images)

    compare_parallel
    -------------------------------
        Compares multiple versions of the parallel algorithm

        Inputs:
            algorithm names     The names of the algorithms we're comparing (without parallel, so
                                the standard one is called 'standard')
            plots               Whether to 'show' or 'save' the plots
                                (use save_pdf / save_eps to also save vector images)
"""

import sys
from time import time

from package.multiple_trials.cantilever.collate import collate_data
from package.multiple_trials.cantilever.process import process_limit_pair
from package.multiple_trials.cantilever.process_parallel import (collate_parallel_data,
                                                                 compare_parallel_trials,
                                                                 process_parallel_delay0)
from package.multiple_trials.cantilever.compare_parallel import compare_parallel_versions
from package.multiple_trials.cantilever.compare_sequential import compare_sequential_versions
from package.multiple_trials.cantilever.run import run_trials

MODE = 'compare_parallel'
ALGORITHM_NAME = 'parallel_delay1'
NO_TRIALS = 4
ELEMENTS_PER_TRIAL = 40
LIMITS = {'moment': 139,
          'axial': 579}
ELEMENT_DELAY = 0
ALGORITHMS_TO_COMPARE = ['standard', 'random']
PLOTS = 'show'

# Input parser
if len(sys.argv) > 1:
    MODE = sys.argv[1]

    if MODE == 'process_parallel_delay0':
        # No other argments
        pass
    else:
        ALGORITHM_NAME = sys.argv[2]
    if MODE == 'run':
        NO_TRIALS = int(sys.argv[3])
        ELEMENTS_PER_TRIAL = int(sys.argv[4])
        LIMITS['moment'] = int(sys.argv[5])
        LIMITS['axial'] = int(sys.argv[6])
        if 'parallel' not in ALGORITHM_NAME:
            ELEMENT_DELAY = None
        else:
            ELEMENT_DELAY = int(sys.argv[7])
    elif MODE == 'process_limit_pair':
        if ALGORITHM_NAME == 'parallel_delay0':
            pass
        else:
            LIMITS['moment'] = int(sys.argv[3])
            LIMITS['axial'] = int(sys.argv[4])
            if 'sequential' in ALGORITHM_NAME:
                ELEMENTS_PER_TRIAL = int(sys.argv[5])
                PLOTS = sys.argv[6]
            elif 'parallel' in ALGORITHM_NAME:
                PLOTS = sys.argv[5]
            else:
                raise ValueError(
                    "Algorithm name must include 'sequential' or 'parallel'")

    elif MODE == 'collate':
        if 'sequential' in ALGORITHM_NAME:
            ELEMENTS_PER_TRIAL = int(sys.argv[3])
            PLOTS = sys.argv[4]
        elif 'parallel' in ALGORITHM_NAME:
            PLOTS = sys.argv[3]
        else:
            raise ValueError(
                "Algorithm name must include 'sequential' or 'parallel'")
    elif MODE == 'compare_sequential':
        ALGORITHMS_TO_COMPARE = sys.argv[2:-2]
        ELEMENTS_PER_TRIAL = int(sys.argv[-2])
        PLOTS = sys.argv[-1]
    elif MODE == 'compare_parallel':
        ALGORITHMS_TO_COMPARE = sys.argv[2:-1]
        PLOTS = sys.argv[-1]


def main():
    """Main function"""
    start = time()

    if MODE == 'run':
        run_trials(ALGORITHM_NAME, NO_TRIALS, ELEMENTS_PER_TRIAL, LIMITS, ELEMENT_DELAY)
    elif MODE == 'process_limit_pair':
        if 'sequential' in ALGORITHM_NAME:
            process_limit_pair(ALGORITHM_NAME, LIMITS, ELEMENTS_PER_TRIAL, PLOTS)
        else:
            compare_parallel_trials(ALGORITHM_NAME, LIMITS, PLOTS)
    elif MODE == 'process_parallel_delay0':
        process_parallel_delay0()
    elif MODE == 'collate':
        if 'sequential' in ALGORITHM_NAME:
            collate_data(ALGORITHM_NAME, ELEMENTS_PER_TRIAL, PLOTS)
        else:
            collate_parallel_data(ALGORITHM_NAME, PLOTS)
    elif MODE == 'compare_sequential':
        compare_sequential_versions(ALGORITHMS_TO_COMPARE,
                                    ELEMENTS_PER_TRIAL, PLOTS)
    elif MODE == 'compare_parallel':
        compare_parallel_versions(ALGORITHMS_TO_COMPARE, PLOTS)
    else:
        raise ValueError(
            'Invalid mode: must be "run", "process_limit_pair", "collate", "compare_sequential", or "compare_parallel"')

    print('Finished in {:.2f} minutes'.format((time()-start)/60))


# Run main function
if __name__ == "__main__":
    main()
