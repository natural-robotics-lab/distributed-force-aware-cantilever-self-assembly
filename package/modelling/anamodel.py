"""Module containing functions related to anaStruct"""

import numpy as np
from anastruct import SystemElements

from ..basics import active_element_override

# Raw geometric constants required by anaStruct
L_ROBOT = 0.9       # Element length, dm
L_LINK = 0.1        # Link length, dm
E = 700E6           # Elastic modulus, N/dm^2
RHO = 2.7           # Density, kg/dm^3 (Al = 2.7,)

N_ROBOT = 6         # Number of truss elements in each robot square
N_LINK = 4          # Number of truss elements in each link square

# Calculated constants
W = pow(L_ROBOT, 3) * RHO * 98.1 / 10         # Weight of element, N
A_CUBE = pow(L_ROBOT, 2)                      # Area of a cubic face, dm^2

A_ROBOT = A_CUBE / N_ROBOT          # Truss CSA in robot members, dm^2
A_LINK = A_CUBE / N_LINK            # Truss CSA in link members, dm^2
EA_ROBOT = E * A_ROBOT
EA_LINK = E * A_LINK

# Angles
SIN_THETA = L_ROBOT / np.sqrt(pow(L_ROBOT, 2) + pow(L_LINK, 2))
COS_THETA = L_LINK / np.sqrt(pow(L_ROBOT, 2) + pow(L_LINK, 2))


class Link:
    """Class for the links between elements.

    Fields:
        orientation     Whether this is a 'row' or 'column' link
        short_ids       Element ids of the short members
        long_ids        Element ids of the long members
        stress          Equivalent von Mises stress
        axial_force     Equivalent axial force
        moment          Equivalent moment
    """

    def __init__(self, orientation='Not set', short_ids=[0, 0], long_ids=[0, 0]):
        self.orientation = orientation
        self.short_ids = short_ids
        self.long_ids = long_ids
        self.stress = 0.0
        self.axial_force = 0.0
        self.moment = 0.0

    def set_link(self, orientation, final_id):
        """Set the current link to the give type"""
        self.orientation = orientation

        self.short_ids = [final_id-3, final_id-2]
        self.long_ids = [final_id-1, final_id]

    def calculate_internal_forces(self, ss):
        """Calculates the axial force, moment, and equivalent von Mises stress in the link"""

        # Return straight away if the link hasn't been set
        if self.orientation == 'Not set':
            self.stress = 0.0
            self.axial_force = 0.0
            self.moment = 0.0
        else:

            f1 = ss.get_element_results(element_id=self.short_ids[0])['N']
            f2 = ss.get_element_results(element_id=self.short_ids[1])['N']
            f3 = ss.get_element_results(element_id=self.long_ids[0])['N']
            f4 = ss.get_element_results(element_id=self.long_ids[1])['N']

            # Define unit vectors and force vectors
            if self.orientation == 'row':
                # Unit vectors
                unit_perpendicular = np.array([1, 0])
                unit_plane = np.array([0, 1])

                # Force vectors
                F1 = np.array([f1, 0])
                F2 = np.array([f2, 0])
                F3 = np.array([f3*COS_THETA, -f3*SIN_THETA])
                F4 = np.array([f4*COS_THETA, f4*SIN_THETA])

                # Moment induced by force about centre of link
                M1 = np.cross(np.array([0, L_ROBOT/2]), F1)
                M2 = np.cross(np.array([0, -L_ROBOT/2]), F2)

            elif self.orientation == 'column':
                # Unit vectors
                unit_perpendicular = np.array([0, -1])
                unit_plane = np.array([1, 0])

                # Force vectors
                F1 = np.array([0, -f1])
                F2 = np.array([0, -f2])
                F3 = np.array([f3*SIN_THETA, -f3*COS_THETA])
                F4 = np.array([-f4*SIN_THETA, -f4*COS_THETA])

                # Moment induced by force about centre of link
                M1 = np.cross(np.array([-L_ROBOT/2, 0]), F1)
                M2 = np.cross(np.array([L_ROBOT/2, 0]), F2)

            else:
                raise Exception('Invalid orientaiton')

            # Calculate forces on link
            F_tot = F1 + F2 + F3 + F4       # Resultant force vector
            force_perpendicular = np.dot(F_tot, unit_perpendicular)
            force_parallel = np.dot(F_tot, unit_plane)

            # Calculate force due to moment
            M_tot = M1 + M2

            # Calculate stresses

            longitudinal_stress = 1/pow(L_ROBOT, 2) * \
                (abs(force_perpendicular) + 3 * abs(M_tot)/L_ROBOT)

            shear_stress = abs(force_parallel) / pow(L_ROBOT, 2)

            von_mises_stress = np.sqrt(pow(longitudinal_stress, 2) +
                                       3*pow(shear_stress, 2))

            # Update forces and stresses
            self.stress = von_mises_stress / 10         # kPa
            self.axial_force = force_perpendicular      # N
            self.moment = M_tot                         # Ndm

        return {'stress': self.stress,
                'axial': self.axial_force,
                'moment': self.moment}


def calculate_internals(structure):
    """
    Calculates the internal moment and axial force in each link of a structure

    Inputs:
        structure       the structure object
    Outputs:
        row_criticals   maximum 'stress', 'axial', or 'moment' in row links down each column
        col_criticals   maximum 'stress', 'axial', or 'moment' in column links down each column
        ss              the anaStruct System Elements object
    """
    [ss, link_elements] = generate_model(structure)

    row_link_criticals, col_link_criticals, complete_link_internals = calculate_link_internals(
        link_elements, ss)

    # Cut entries in stresses that don't have any elements in that column
    row_link_criticals_trimmed, col_link_criticals_trimmed = trim_arrays(structure,
                                                                         row_link_criticals,
                                                                         col_link_criticals)

    return row_link_criticals_trimmed, col_link_criticals_trimmed, complete_link_internals, ss


def generate_model(structure):
    """Generates an anaStruct model from the defined structure

    Inputs:
        structure   the Structure object
    Outputs:
        ss          the anaStruct System Elements object
    """
    # Calculate dimensions
    M, N = structure.calculate_size()

    # Initialise grid of element IDs for the links
    links = [[Link() for c in range(2*N)] for r in range(M)]

    # Create ss object
    ss = SystemElements()

    # x and y coordinates of TR node of the first element
    x0, y0 = initialise_TR_node_coordinates(structure)

    x = x0
    y = y0

    # ID of the last element to be placed
    last_element_id = 0

    for m in range(M):
        # Loop over each row in the structure
        for n in range(N):
            # Loop over each element in that row

            # Define nodes
            top_right = [x, y]
            bottom_right = [x, y-L_ROBOT]
            top_left = [x-L_ROBOT, y]
            bottom_left = [x-L_ROBOT, y-L_ROBOT]
            llink_top = [x-(L_ROBOT+L_LINK), y]
            llink_bottom = [x-(L_ROBOT+L_LINK), y-L_ROBOT]
            alink_right = [x, y+L_LINK]
            alink_left = [x-L_ROBOT, y+L_LINK]

            if structure.map[m, n] in [1, 2, 3]:
                # Skip if active element on the right attached to support or another active
                # element on the right that is attached to support

                # Create robot elements
                robot_elements = [[top_left, top_right],
                                  [top_right, bottom_right],
                                  [bottom_right, bottom_left],
                                  [bottom_left, top_left],
                                  [top_left, bottom_right],
                                  [top_right, bottom_left]]

                for element in robot_elements:
                    ss.add_truss_element(location=element, EA=EA_ROBOT)
                    last_element_id += 1

                # Create link elements to the left if necessary
                place_left = False
                # Condition if the boundary is default
                if -1 not in structure.map:
                    # Condition if the boundary is default
                    if x == (L_ROBOT+L_LINK) or structure.map[m, n-1] in [1, 3]:
                        place_left = True
                else:
                    # Condition if boundary is explicitly defined
                    if n > 0 and structure.map[m, n-1] != 0:
                        place_left = True

                # Override for active elements
                if place_left:
                    place_left = active_element_override('left', [m, n],
                                                         structure)

                if place_left:
                    link_elements = [[llink_top, top_left],
                                     [llink_bottom, bottom_left],
                                     [llink_top, bottom_left],
                                     [llink_bottom, top_left]]

                    for element in link_elements:
                        ss.add_truss_element(location=element, EA=EA_LINK)
                        last_element_id += 1

                        links[m][2*n].set_link('row', last_element_id)

            # Allow for creating links above boundary
            if structure.map[m, n] in [1, -1, 2, 3]:
                # Create link elements above if necessary
                place_above = bool(m != 0 and structure.map[m-1, n] in [1, 2, 3])

                # Override for active elements
                if place_above:
                    place_above = active_element_override('above', [m, n], structure)

                if place_above:
                    link_elements = [[top_left, alink_left],
                                     [top_right, alink_right],
                                     [top_right, alink_left],
                                     [top_left, alink_right]]

                    for element in link_elements:
                        ss.add_truss_element(location=element, EA=EA_LINK)
                        last_element_id += 1

                        links[m][2*n+1].set_link('column', last_element_id)

            # Move to next location
            x += L_ROBOT+L_LINK

        x = x0      # Reset x to x0
        y -= (L_ROBOT+L_LINK)      # Reduce y by 1

    # Go through each node and apply a load or boundary condition
    nodes_list = ss.nodes_range('both')

    for node_idx, node_coords in enumerate(nodes_list):
        # node ID starts at 1, not zero
        node_id = node_idx+1

        # Add hinges at support nodes
        if is_support(node_coords, -L_LINK/2):
            ss.add_support_hinged(node_id=node_id)
        else:
            # Apply weight away from supports
            node_load = -W/4

            ss.point_load(node_id=node_id, Fy=node_load)

    if nodes_list:
        # Solve forces only if any have been added
        try:
            ss.solve()
        except Exception as err:
            with np.printoptions(threshold=np.inf):
                print('Failed to solve {} with map:'.format(structure.configuration().lower()))
                print(structure.map[:structure.row_offset+structure.no_rows+1])

            raise err

    return ss, links


def initialise_TR_node_coordinates(structure):
    """Sets the x-y coordinates of the top right node of the element
    in the top left

    Inputs:
        structure   the structure object
    Outputs:
        x           x-coordinate
        y           y-coordinate
    """

    x = (L_ROBOT+L_LINK) * (1-structure.col_offset) - L_LINK/2
    y = (L_ROBOT+L_LINK) * structure.row_offset - L_LINK/2

    return x, y


def is_support(coordinate, x_support):
    """Checks if the coordinate is on a support or not

    Use isclose and 1e-5 instead of 0 to account for float errors
    """

    # Side of left support
    if np.isclose(coordinate[0], x_support) and coordinate[1] <= -L_LINK/2 + 1e-5:
        return True

    # Above left support
    if coordinate[0] <= x_support+1e-5 and np.isclose(coordinate[1], -L_LINK/2):
        return True


def calculate_link_internals(links, ss):
    """Calculates dictionaries of arrays containing the link VM stresses, axial forces, and bending
    moments

    The max_xxx_links outputs are all dictionaries, with 1xN numpy arrays in each entry:
    'stress':   VM stress(always positive, so take max)
    'axial':    axial force(only interested in tension, so take max)
    'moment':   bending moment(sign is irrelevant, so take max of absolute
                value)

    Inputs:
        links   an Mx2N list of the links between elements
        ss      the anaStrust Sustem Elements object
    Outputs:
        max_row_links       dictionary for row links
        max_col_links       dictionary for column links
        combined            dictionary for all links, where each entry is a
                            Mx2N numpy array

    """
    # Calculate dimensions
    M = len(links)
    N = int(0.5 * len(links[0]))

    # Arrays of Von Mises stress in each column
    row_links = {'stress': np.zeros((M, N+1)),    # Links along rows
                 'axial': np.zeros((M, N+1)),
                 'moment': np.zeros((M, N+1))}
    col_links = {'stress': np.zeros((M, N+1)),    # Links down columns
                 'axial': np.zeros((M, N+1)),
                 'moment': np.zeros((M, N+1))}

    # Array for Von Mises stress in every link
    combined = {'stress': np.zeros((M, 2*N)),
                'axial': np.zeros((M, 2*N)),
                'moment': np.zeros((M, 2*N))}

    r = 0
    c = 0

    for r in range(M):
        # Loop over rows
        for c in range(N):
            # Loop over columns

            # Calculate internal forces in link
            internal_forces = links[r][2*c].calculate_internal_forces(ss)

            # Assign internal forces to arrays
            for key in internal_forces:
                row_links[key][r, c] = internal_forces[key]
                combined[key][r][2*c] = internal_forces[key]

            if M > 1 and r > 0:
                # Only add to rows array if there's more than one row

                # Calculate internal forces in link
                internal_forces = links[r][2*c+1].calculate_internal_forces(ss)

                # Assign internal forces to arrays
                for key in internal_forces:
                    col_links[key][r-1, c] = internal_forces[key]
                    combined[key][r][2*c+1] = internal_forces[key]

    # Calculate maximum stress in each column
    if M == 1:
        raise NotImplementedError('Not updated to the dictionary format')
    else:
        max_row_links = {'stress': row_links['stress'].max(0),
                         'axial': row_links['axial'].max(0),
                         'moment': abs(row_links['moment']).max(0)}
        max_col_links = {'stress': col_links['stress'].max(0),
                         'axial': col_links['axial'].max(0),
                         'moment': abs(col_links['moment']).max(0)}

    # Take magnitude of moment
    combined['moment'] = abs(combined['moment'])

    return max_row_links, max_col_links, combined


def trim_arrays(structure, row_link_criticals, col_link_criticals):
    """Trims the arrays for the structure"""

    row_link_criticals_trimmed = {}
    col_link_criticals_trimmed = {}

    if structure.configuration() == 'Cantilever':
        # Cantilever configuration

        index_first = structure.first_filled_col_index

        index_last_cols = index_first + structure.no_cols
        index_last_rows = index_first + structure.no_cols + 1

    else:
        raise NotImplementedError('Only structures are cantilevers')

    for key in col_link_criticals:
        row_link_criticals_trimmed[key] = row_link_criticals[key][index_first: index_last_rows]
        col_link_criticals_trimmed[key] = col_link_criticals[key][index_first: index_last_cols]

    return row_link_criticals_trimmed, col_link_criticals_trimmed
