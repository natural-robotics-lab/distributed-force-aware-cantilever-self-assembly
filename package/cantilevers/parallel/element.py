"""The Element class"""
import copy
import random as rand

import numpy as np

from ...graphics.colours import BLUE, ORANGE, RED, YELLOW
from ..sequential.traverse_data import TraverseData
from . import parallelfunc as parfunc

try:
    from ...structures import Cantilever
except ImportError:
    # Don't need these imports, it's just for type hinting
    pass


class Element():
    """The class for the parallel elements. Initialises with a moving element
    above the first element in the cantilever."""

    def __init__(self, unique_id, figure, location=None, mode='gathering', sequential=False):
        if location is None:
            self.location = [-1, 0]
        else:
            self.location = location

        # Physical details
        self.traverse_data = TraverseData(local=True)
        if sequential:
            # Track what columns are valid
            self.traverse_data.valid_cols = []
        self.row_internals = {'moment': [0.0, 0.0], 'axial': [0.0, 0.0]}
        self.col_internals = {'moment': [0.0, 0.0], 'axial': [0.0, 0.0]}
        self.placement_options = []

        # Links
        self.active_link = 's'
        self.passive_links = ''

        # Movement trackers
        self.destination_column = -1
        self.direction = 'right'

        # Graphics information
        self.figure = figure
        self.obj_square = None
        self.obj_idlabel = None
        self.mode = mode
        self.id = unique_id

        # Extra flags
        self.advanced_this_step = False
        self.override_next_step = None
        self.stationary_count = 0

    def __repr__(self):
        """String representation"""

        final_str = 'Element {}: {}, currently in location {} and heading {}'.format(
            self.id, self.mode, self.location, self.direction)

        final_str += '\n  Active link: {}'.format(self.active_link)
        final_str += '\n  Passive links: {}'.format(self.passive_links)

        # Add information about where it's placing
        if self.mode == 'placing':
            final_str += '\n  Placing in columns:'
            for placement_option in self.placement_options:
                # Put asteriks around destination column
                if placement_option.column == self.destination_column:
                    final_str += ' *{}'.format(placement_option.column)
                else:
                    final_str += ' {}'.format(placement_option.column)
                # Denote which placement option corresponds to placing at the tip
                if placement_option.type == 'tip':
                    final_str += 't'
                if placement_option.column == self.destination_column:
                    final_str += '*'

        if self.stationary_count > 0:
            final_str += '\n  Stationary for {} steps'.format(
                self.stationary_count)

        return final_str

    def copy(self, minimal=False):
        """Returns a copy of the Element object

        Inputs:
            minimal     bool; whether to reduce the copy to only include plotting data
        """

        # Initialise element (no figure object)
        element_copy = Element(self.id, None, self.location.copy(), self.mode)

        # Copy traverse_data and internals (only if not reducing)
        if not minimal:
            element_copy.traverse_data = self.traverse_data.copy()
            element_copy.row_internals = copy.deepcopy(self.row_internals)
            element_copy.col_internals = copy.deepcopy(self.col_internals)
            element_copy.placement_options = copy.deepcopy(
                self.placement_options)

        # Set other fields
        element_copy.active_link = self.active_link
        element_copy.passive_links = self.passive_links

        element_copy.destination_column = self.destination_column
        element_copy.direction = self.direction

        element_copy.override_next_step = self.override_next_step

        return element_copy

    def plot(self, colour=None):
        """Plot the element on the given axis with the correct colour

        Inputs:
            colour      (optional) element colour
        Outputs:
            obj_square  the object of the fill shape
        """
        centre = self.location[1]+0.5, -self.location[0]

        if colour is None:
            colour = self.auto_colour()

        # Delete old figure object if it exists
        if self.obj_square is not None:
            try:
                self.obj_square.remove()
            except Exception:
                print('Could not remove element {} from plot'.format(self.id))

        # Draw new square
        self.obj_square = self.figure.structure_plot.plot_square(
            centre, colour)

        # Also delete and draw ID if still moving
        if self.obj_idlabel is not None:
            try:
                self.obj_idlabel.remove()
                self.obj_idlabel = None
            except Exception:
                print('Could not remove element id {} from plot'.format(self.id))

        if self.mode != 'placed':
            self.obj_idlabel = self.figure.structure_plot.plot_id(
                centre, self.id)

    def auto_colour(self):
        """Returns the colour that the object should be plotted"""

        if self.mode == 'gathering':
            colour = ORANGE
        elif self.mode == 'placing':
            colour = YELLOW
        elif 'swapping' in self.mode:
            colour = RED
        elif self.mode == 'placed':
            colour = BLUE
        else:
            raise ValueError(
                "mode must be 'gathering', 'placing', or 'placed'")
        return colour

    def update_sensors(self, complete_internals, cantilever: 'Cantilever'):
        """Update the readings of the sensors of this element.

        Inputs:
            complete_internals  complete link internals, dictionary with
                                'stress', 'moment' and 'axial'
            noise               noise to add(if any)
        """
        # Calculate offset index in the link stresses matrix
        idx = [self.location[0]+cantilever.row_offset,
               2*(self.location[1]+cantilever.col_offset)]

        # Update sensors to actual readings
        for key in self.row_internals:
            self.row_internals[key] = [complete_internals[key][idx[0]][idx[1]],
                                       complete_internals[key][idx[0]][idx[1]+2]]
            self.col_internals[key] = [complete_internals[key][idx[0]][idx[1]+1],
                                       complete_internals[key][idx[0]+1][idx[1]+1]]

    def advance(self, cantilever: 'Cantilever', limits, max_stationary_count,
                random=False, simple=False):
        """Advances the element by one step

        Element can stay still for up to max_stationary_count steps before it's deemed
        stuck and just places where it is. This value is set to max(2, element_delay) to
        allow for situations where element_delay=0"""

        step_successful = False
        timed_out = False

        if self.mode == 'gathering':
            # Measure the internal readings here (if an edge element is there)
            try:
                external_element = cantilever.elements[self.location[0]-np.sign(self.location[0]),
                                                       self.location[1]]
                if external_element.mode == 'placed':
                    # Only read the data if this is a placed element
                    self.traverse_data.append_criticalness_local(external_element,
                                                                 limits,
                                                                 self.location)
            except KeyError:
                # No element at this location
                pass

            # Select whether this column is valid or not
            self.traverse_data.column_valid(self.location, cantilever)

            # Switch to placing mode if back at the support
            if self.location[0] > 0 and self.location[1] == 0:
                self.mode = 'placing'

                # Calculate placement options
                self.generate_placement_options(random, simple)

                # Set the destination column
                self.set_destination()
                self.set_direction(cantilever)
            else:

                # Take a step in the right direction
                step_successful = self.make_step(cantilever, limits)

        elif self.mode == 'placing':
            # Try to place here
            if self.location[1] == self.destination_column:
                self.attempt_placement(cantilever)

            # Take a step in the right direction
            step_successful = self.make_step(cantilever, limits)

        elif 'swapping' in self.mode:
            # Element started swapping placed with another one in its previous step

            # Don't move, but allow active link to change
            self.release_active_link(cantilever)
            self.set_active_link(cantilever)

            self.swap_mode(cantilever)

        elif self.mode == 'placed':
            # Element placed in previous step when it was a slave
            self.plot()
            return 0
        else:
            raise ValueError(
                "mode must be 'gathering', 'placing', or 'placed'")

        # Increment counter for number of stationary steps
        if step_successful:
            self.stationary_count = 0
        else:
            self.stationary_count += 1

        # Try to place here if been stationary too long
        if self.stationary_count > max_stationary_count:
            self.attempt_placement(cantilever, True)
            if self.mode == 'placed':
                timed_out = True

        # Update location of object
        self.plot()

        # This element has advanced
        self.advanced_this_step = True

        # Return whether the element placed due to timeout, otherwise 0
        return timed_out

    def make_step(self, cantilever: 'Cantilever', limits, recursion_depth=0):
        """Moves the element by one step in the specified direction if possible

        Returns whether it was possible to move or not
        """

        if recursion_depth > 0:
            print('Recursion depth = {}'.format(recursion_depth))

        # Update direction
        self.set_direction(cantilever)

        # Tried to move with this override, so reset it
        self.override_next_step = ''

        # Can't move if:
        #   - Not required
        #   - There are any passive links
        #   - Moving left in column 0
        #   - Moving right at the tip
        if self.direction == 'none' or self.passive_links \
                or (self.direction == 'left' and self.location[1] == 0) \
                or (self.direction == 'right' and self.location[0] == 0):

            # Still might need to change around active link
            self.release_active_link(cantilever)
            self.set_active_link(cantilever)

            return False

        # Get locations of Moore neighbourhood in direction of motion in order of preference
        locations = parfunc.moore_neighbourhood_preferences(self.direction, self.location,
                                                            self.active_link)

        # Cycle through each location until you find an empty space or active element
        for candidate_location in locations:
            next_element = cantilever.element_at(candidate_location)

            if next_element == 0:
                # Empty space: check the element will still be attached here
                adjacents = parfunc.adjacent_locations(candidate_location)

                # Don't consider element in previous location
                try:
                    adjacents.remove(self.location)
                except ValueError:
                    pass

                # Move here if any of these locations are occupied
                if any(cantilever.element_at(loc) != 0 for loc in adjacents):

                    # Release active link
                    self.release_active_link(cantilever)

                    # Update locations
                    initial_location = self.location.copy()
                    self.location = candidate_location

                    # Check this new location is actually valid
                    if self.location[0] >= 0 and self.location[1] < 0:
                        raise Exception('Element moved into boundary: {}'.format(
                            self.location))

                    # Update the Cantilever object accordingly
                    cantilever.move_parallel_element(self, initial_location)

                    # Set the active link
                    self.set_active_link(cantilever)

                    return True

            elif next_element == 2 and not parfunc.diagonal_locations(self.location,
                                                                      candidate_location):
                # Active element: try to swap with it
                move_again = parfunc.swap_elements(self,
                                                   cantilever.elements[candidate_location],
                                                   cantilever,
                                                   limits)

                # Try to move again if required (only once)
                if move_again and recursion_depth < 1:
                    print('Master trying swap again')
                    recursion_depth += 1
                    step_successful = self.make_step(cantilever, limits,
                                                     recursion_depth)
                    return step_successful

                # Still might need to change around active link
                self.release_active_link(cantilever)
                self.set_active_link(cantilever)

                return False

    def active_location(self):
        """The location of the element the active link is attached to"""

        if self.active_link == 'n':
            location = [self.location[0]-1,
                        self.location[1]]
        elif self.active_link == 'e':
            location = [self.location[0],
                        self.location[1]+1]
        elif self.active_link == 's':
            location = [self.location[0]+1,
                        self.location[1]]
        elif self.active_link == 'w':
            location = [self.location[0],
                        self.location[1]-1]

        return location

    def set_active_link(self, cantilever: 'Cantilever'):
        """Each active element is in control of one link, called its active link.

        Default is the placed element above or below it.
        If that's not possible it will link to the left if one of:
            - No element above
            - Active element above and placed element left
            - Active elements above and left, self moving right, and element
              above isn't swapping
        """

        # Update direction
        self.set_direction(cantilever)

        # Get the type of element in the Von Neumann neighbourhood
        adjacent_locations = parfunc.adjacent_locations(self.location)
        n_element = cantilever.element_at(adjacent_locations[0])
        s_element = cantilever.element_at(adjacent_locations[1])
        e_element = cantilever.element_at(adjacent_locations[2])
        w_element = cantilever.element_at(adjacent_locations[3])

        if n_element != 0:
            n_element_mode = cantilever.elements[adjacent_locations[0]].mode
        else:
            n_element_mode = 'unspecified'

        # Reset active link
        self.active_link = ''

        if self.location[0] < 0:
            # When above the cantilever, choose link below unless element below
            # and element left are active (allows element below to clear)
            if s_element == 2 and w_element == 2:
                self.active_link = 'w'
            else:
                self.active_link = 's'
        elif (n_element == 0) or \
            (n_element == 2 and w_element == 1) or \
                (n_element == 2 and w_element == 2 and self.direction == 'right'
                    and 'swapping' not in n_element_mode):
            # Choose element on left if:
            #    - No element above
            #    - Active element above and placed element left
            #    - Active elements above and left, self moving right, and element
            #      above isn't swapping
            self.active_link = 'w'
        else:
            # Otherwise choose above
            self.active_link = 'n'

        # Raise error if didn't set an active link
        if not self.active_link:
            raise Exception('No active link set for element {} in position {}'.format(
                self.id, self.location))

        # Add this link as a passive link to the neighbouring element
        if self.active_link == 'n' and n_element == 2:
            cantilever.elements[adjacent_locations[0]
                                ].passive_links += 's'
        elif self.active_link == 's' and s_element == 2:
            cantilever.elements[adjacent_locations[1]
                                ].passive_links += 'n'
        elif self.active_link == 'e' and e_element == 2:
            cantilever.elements[adjacent_locations[2]
                                ].passive_links += 'w'
        elif self.active_link == 'w' and w_element == 2:
            cantilever.elements[adjacent_locations[3]
                                ].passive_links += 'e'

        return

    def release_active_link(self, cantilever: 'Cantilever'):
        """Releases the active link and updates the corresponding passive link
        in another element.

        Output:
            prev_active_link    the previous active link"""

        if cantilever.element_at(self.active_location()) == 2:
            if self.active_link == 'n':
                passive_side = 's'
            elif self.active_link == 'e':
                passive_side = 'w'
            elif self.active_link == 's':
                passive_side = 'n'
            elif self.active_link == 'w':
                passive_side = 'e'

            cantilever.elements[self.active_location()].release_passive_link(
                passive_side)

        # Update active_link variable, and return the previous one
        prev_active_link = self.active_link
        self.active_link = ''

        return prev_active_link

    def release_passive_link(self, side):
        """Releases the passive link on the specified side"""

        self.passive_links = self.passive_links.replace(side, '')

    def place(self, cantilever: 'Cantilever'):
        """Places the element in its current location"""

        # Set mode and type
        self.mode = 'placed'

        # Adjust cantilever object
        cantilever.insert_element(self)

        # Plot on graph
        self.plot()

    def generate_placement_options(self, random, simple):
        """ Create placement options list from traverse_data"""

        # Compress local data
        self.traverse_data.compress_local()

        # Calculate probability distributions
        column_probs = self.traverse_data.stochastic_calculate_column_probabilities(
            print_debug=False, simple=simple)

        # Calculate no. placement options to draw
        no_placement_options = len(column_probs)

        # Add tip to start if the cantilever is stable
        if max([max(self.traverse_data.data['row']['moment']),
                max(self.traverse_data.data['row']['axial']),
                max(self.traverse_data.data['col']['moment']),
                max(self.traverse_data.data['col']['axial'])]) < 1:
            self.placement_options.append(parfunc.PlacementOption(len(column_probs),
                                                                  'tip',
                                                                  1))
            no_placement_options += 1

        # Choose reinforcement columns randomly (only use force measurements to decide
        # if cantilever needs reinforcement, not to choose where to reinforce)
        if random:
            reinforcement_options = [parfunc.PlacementOption(col, 'reinforcement', 1) for col
                                     in range(no_placement_options-len(self.placement_options))]
            rand.shuffle(reinforcement_options)
            self.placement_options += reinforcement_options

        else:
            # Draw placement options stochastically
            while len(self.placement_options) < no_placement_options:

                # Draw random sample
                col_no = np.random.choice(len(column_probs), p=column_probs)

                # Extract the placement options in this column
                if col_no not in [option.column for option in self.placement_options]:
                    # Append to the list if required
                    self.placement_options.append(parfunc.PlacementOption(col_no,
                                                                          'reinforcement',
                                                                          column_probs[col_no]))

                    # print('    Adding placement option in col {}'.format(col_no))
                else:
                    # Otherwise don't do anything
                    # print('        Rejected col {}'.format(col_no))
                    pass

        # Clear invalid columns (if set)
        if self.traverse_data.valid_cols is not None:
            self.placement_options = [opt for opt in self.placement_options
                                      if opt.column in self.traverse_data.valid_cols]

        return

    def set_destination(self):
        """ Selects the next column in the list of placement options as the
        destination"""

        # Not selected a destination yet, so choose the first one
        if self.destination_column == -1:
            self.destination_column = self.placement_options[0].column
            return

        # Already chosen one, so choose the next one
        columns_list = [option.column for option in self.placement_options]
        idx_last = columns_list.index(self.destination_column)
        self.destination_column = columns_list[idx_last+1]
        return

    def set_direction(self, cantilever: 'Cantilever'):
        """Determines the direction of travel of the placing element in the next step"""

        # Test override first
        if self.override_next_step:
            self.direction = self.override_next_step
            return

        if 'gathering' in self.mode:
            # Gathering mode: determined by side of the cantilever
            if self.location[0] < 0:
                # Above the cantilever
                if cantilever.element_at([self.location[0]+1,
                                          self.location[1]]) == 2 \
                        and self.active_link == 's':
                    # Above and attached to an active element (step back)
                    self.direction = 'left'
                else:
                    # Default (step towards tip)
                    self.direction = 'right'
            else:
                # Below the cantilever (step away from tip)
                self.direction = 'left'
        elif 'placing' in self.mode:
            # Placing mode: determined by destination column
            if self.location[1] < self.destination_column:
                self.direction = 'right'
            elif self.location[1] > self.destination_column:
                self.direction = 'left'
            elif self.location[0] == 0 or cantilever.element_at([self.location[0]-1,
                                                                 self.location[1]]) == 1:
                # If in destination column in correct row, don't move (will place next time)
                self.direction = 'none'
            else:
                # In destination column but not correct row, so move right
                self.direction = 'right'
        elif self.mode == 'placed':
            self.direction = 'none'
        else:
            raise ValueError('Cannot set direction for mode{}'.format(
                self.mode))

    def attempt_placement(self, cantilever: 'Cantilever', stationary=False, dry_run=False):
        """Try to place in the location that the element is currently in. There needs
        to be a stationary element above and to the left of the current location in
        order for this location to be considered valid.

        Inputs:
            cantilever      the cantilever object
            stationary      whether attempting this placement because the element has
                            been stationary too long (don't reset destination column)
            dry_run         whether to just test placing here, instead of actually
                            placing here (used during swapping)
        """

        # Get the elements adjacent to the  current location
        element_above = cantilever.element_at([self.location[0]-1,
                                               self.location[1]])
        element_left = cantilever.element_at([self.location[0],
                                              self.location[1]-1])

        # Check if this element will try to place here on its next step, but don't place
        if dry_run:
            if self.location[1] == self.destination_column and \
                    (element_above == 1 or self.location[0] == 0):
                # Element to be at the top of the destination column, so will try to
                # place here next time
                return True

            # Element either not in destination column, or not at the top of it
            return False

        # Determine whether placing at the tip in this column is valid or not
        if not stationary:
            # Can choose from placement options as we're in gathering mode
            placement_option = [x for x in self.placement_options
                                if x.column == self.location[1]][0]

            require_tip = bool(placement_option.type == 'tip')
        else:
            require_tip = False

        if (element_above == 1 and element_left in [1, -1]) \
                or (element_left == 1 and self.location[0] == 0):
            # Place here if element above and left, or at the tip that's where we're trying
            # to place
            if require_tip:
                if self.location[0] == 0:
                    self.place(cantilever)
                else:
                    # This placement location isn't valid, so try the next instead
                    self.set_destination()
            else:
                self.place(cantilever)
        elif element_above != 1:
            # Not reached the fixed cantilever yet, so keep moving
            pass
        elif not stationary:
            # This placement location isn't valid, so try the next instead
            self.set_destination()

        return False

    def swap_mode(self, cantilever: 'Cantilever', wait=False):
        """Element operation when in swap mode. If not already in swap mode,
        this function puts the element in swap mode. Otherwise, it returns it
        to its previous mode

        mode is represented as swapping_OLDMODE so we can swap back to the old mode
        with the same variable"""

        if wait:
            # Already in swap mode, but need to make sure this element waits in the next step
            self.mode += 'WAIT'
            return

        if 'swapping' not in self.mode:
            # Entering swap mode
            self.mode = 'swapping_' + self.mode
        elif 'WAIT' in self.mode:
            self.mode = self.mode.split('WAIT')[0]
        else:
            # Leaving swap mode
            self.mode = self.mode.split('swapping_')[1]
            self.set_direction(cantilever)
