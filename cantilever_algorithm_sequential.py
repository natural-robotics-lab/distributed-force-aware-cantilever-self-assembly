"""The distributed cantilever algorithm"""
import argparse
import sys

from package.cantilevers.sequential.seqalg import run_algorithm

# Test parameters. Set allowable stresses to False if we don't want to test for them
MAX_ELEMENTS = 30                           # Maximum number of elements to add
LIMITS = {'allowable': {'moment': 139,      # Allowable bending moment in links, Ndm
                        'axial': 579},      # Allowable axial force in links, N
          'failure': {'moment': None,       # Failure bending moment in links, Ndm
                      'axial':  None}}      # Failure axial force in links, N
TRIAL = 0                                   # Trial number for multiple trials with same settings
LOCAL = True                                # Whether to use truely local information
DETERMINISTIC = False                       # Whether to do the deterministic version
SIMPLE = False                              # Whether to do the simple version
PLOTS = 'All'                               # Which plots: 'All', 'Final', or 'None'
SAVE = False                                # Save output or not
SPECIALS_BEFORE = ''                        # Any special characters to put before the filename
SPECIALS_AFTER = ''                         # Any special characters to put after the filename

SAVE_STABLE_STRUCTURES = False   # Whether to save the stable states or not


# If called with arguments, update the variables
if len(sys.argv) > 1:

    parser = argparse.ArgumentParser()
    # Declare mandatory positional arguments
    parser.add_argument('max_elements', help='Maximum elements to be added',
                        type=int)
    parser.add_argument('M_allowable', help='Allowable moment (Ndm)',
                        type=int)
    parser.add_argument('F_allowable', help='Allowable axial force (N)',
                        type=int)

    # Declare optional arguments
    parser.add_argument('-s', '--save', help='Save the output',
                        action='store_true')
    parser.add_argument('-p', '--plots',
                        help='Plots to draw',
                        choices=['all', 'final', 'none'],
                        default='All')
    parser.add_argument('-t', '--trial',
                        help='Trial number',
                        type=int,
                        default=0)
    parser.add_argument('-sb', '--specials-before',
                        help='Special characters before filename',
                        default='')
    parser.add_argument('-sa', '--specials-after',
                        help='Special characters after filename',
                        default='')
    parser.add_argument('-loc', '--local', help='Local version (default is message-passing)',
                        action='store_true')
    parser.add_argument('-det', '--deterministic',
                        help='Deterministic version (place in columnn of maximum criticalness)',
                        action='store_true')
    parser.add_argument('-simp', '--simple', help='Simple version (no urgency distributions)',
                        action='store_true')

    # Parse arguments
    args = parser.parse_args()
    MAX_ELEMENTS = args.max_elements
    LIMITS['allowable']['moment'] = args.M_allowable
    LIMITS['allowable']['axial'] = args.F_allowable
    SAVE = args.save
    PLOTS = args.plots.capitalize()
    TRIAL = args.trial
    SPECIALS_BEFORE = args.specials_before
    SPECIALS_AFTER = args.specials_after
    LOCAL = args.local
    DETERMINISTIC = args.deterministic
    SIMPLE = args.simple

    # No failure limits in this case
    LIMITS['failure']['moment'] = None
    LIMITS['failure']['axial'] = None


def main():
    """Main function just runs the algorithm with the given parameters"""

    # Run algorithm
    run_algorithm(max_elements=MAX_ELEMENTS,
                  limits=LIMITS,
                  trial=TRIAL,
                  local=LOCAL,
                  deterministic=DETERMINISTIC,
                  simple=SIMPLE,
                  plots=PLOTS,
                  save=SAVE,
                  specials_before=SPECIALS_BEFORE,
                  specials_after=SPECIALS_AFTER,
                  save_stable_structures=SAVE_STABLE_STRUCTURES)


# Run main function
if __name__ == "__main__":
    main()
