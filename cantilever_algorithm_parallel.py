"""The parallel distributed cantilever algorithm

    Run python cantilever_algorithm_parallel.py -h to view command line args
"""
import argparse
import sys

from package.cantilevers.parallel.paralg import run_algorithm

MAX_ELEMENTS = 30                            # Maximum number of elements to add
ELEMENT_DELAY = 8                           # Number of steps between adding another new element
LIMITS = {'allowable': {'moment': 139,      # Allowable bending moment in links, Ndm
                        'axial': 579},      # Allowable axial force in links, N
          'failure': {'moment': None,       # If 0, set automatically later
                      'axial':  None}}      # If 0, set automatically later
PLOTS = 'All'                               # Which plots: 'All', 'Final', or 'None'
SAVE = False                                # Save output (figures and compressed structures)
SAVE_MULTIPLE_TRIALS = False                # Save output (format for multiple trials)
TRIAL = 0
SPECIALS_BEFORE = ''
SPECIALS_AFTER = ''
RANDOM = False
SIMPLE = False


if len(sys.argv) > 1:
    parser = argparse.ArgumentParser()
    # Declare mandatory positional arguments
    parser.add_argument('max_elements', help='Maximum elements to be added',
                        type=int)
    parser.add_argument('M_allowable', help='Allowable moment (Ndm)',
                        type=int)
    parser.add_argument('F_allowable', help='Allowable axial force (N)',
                        type=int)
    parser.add_argument('element_delay',
                        help='Number of steps between adding elements',
                        type=int)
    # Declare optional arguments
    parser.add_argument('-s', '--save', help='Save the output',
                        action='store_true')
    parser.add_argument('-p', '--plots',
                        help='Plots to draw',
                        choices=['all', 'final', 'none'],
                        default='All')
    parser.add_argument('-t', '--trial',
                        help='Trial number',
                        type=int,
                        default=0)
    parser.add_argument('-sb', '--specials-before',
                        help='Special characters before filename',
                        default='')
    parser.add_argument('-sa', '--specials-after',
                        help='Special characters after filename',
                        default='')
    parser.add_argument('-r', '--random', help='Run without taking force information into account',
                        action='store_true')
    parser.add_argument('-simp', '--simple', help='Simple version (no urgency distributions)',
                        action='store_true')

    # Parse arguments
    args = parser.parse_args()
    MAX_ELEMENTS = args.max_elements
    LIMITS['allowable']['moment'] = args.M_allowable
    LIMITS['allowable']['axial'] = args.F_allowable
    ELEMENT_DELAY = args.element_delay
    SAVE = args.save
    PLOTS = args.plots.capitalize()
    TRIAL = args.trial
    SPECIALS_BEFORE = args.specials_before
    SPECIALS_AFTER = args.specials_after
    RANDOM = args.random
    SIMPLE = args.simple


# Run algorithm function
if __name__ == "__main__":
    run_algorithm(max_elements=MAX_ELEMENTS,
                  element_delay=ELEMENT_DELAY,
                  limits=LIMITS,
                  plots=PLOTS,
                  save=SAVE,
                  trial=TRIAL,
                  specials_before=SPECIALS_BEFORE,
                  specials_after=SPECIALS_AFTER,
                  save_multiple_trials=SAVE_MULTIPLE_TRIALS,
                  random=RANDOM,
                  simple=SIMPLE)
