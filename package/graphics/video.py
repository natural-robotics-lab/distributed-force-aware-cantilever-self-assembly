"""Functions to convert images to videos using OpenCV"""
import glob
import os
import re
import shutil

try:
    import cv2
except ImportError:
    # Opencv isn't installed so can't export videos; rest of the code can still run
    print('OpenCv is not installed, so videos cannot be exported')


def video_from_directory(directory, head, check_complete=True, delete=True):
    """Makes a video from a directory full of images of filenames HEADxxx

    Based on code by KANG & ATUL, October 2018, available at: https://theailearner.com/2018/10/15/creating-video-from-images-using-opencv-python/
    (Accessed 11/02/2021)
    """

    # Get frames
    img_array = []
    for filename in sorted(glob.glob(os.path.join(directory, head+'*')),
                           key=lambda f: re.search(head+r'(\d+)\.png', f).group(1)):
        img = cv2.imread(filename)
        height, width, _ = img.shape
        size = (width, height)
        img_array.append(img)

    # Get expected number of frames from final filename
    no_frames_expected = int(filename.split('_')[-1].split('.')[0])

    # Also have step 0 in parallel version
    if 'parallel' in directory:
        no_frames_expected += 1

    # Create video object
    filename = directory.split('TEMP_')[0] \
        + directory.split('TEMP_')[1]+'.mp4'
    out = cv2.VideoWriter(filename,
                          cv2.VideoWriter_fourcc(*'mp4v'), 15, size)

    # Write video
    for img in img_array:
        out.write(img)
    out.release()

    if delete:
        # Delete temporary directory if all frames used
        if not check_complete or no_frames_expected == len(img_array):
            shutil.rmtree(directory)
        else:
            print("Video export didn't include all the frames: not deleting temporary files")
