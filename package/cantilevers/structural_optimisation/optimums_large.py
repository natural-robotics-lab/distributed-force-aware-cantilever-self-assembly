"""Large optimums mode"""

import multiprocessing as mp
import os
import pickle as pkl
from itertools import repeat
import glob

import fasteners

from ...basics import calculate_criticalness, data_dir
from ...modelling.anamodel import calculate_internals
from ...structures import Cantilever
from .enumerate import limit_dir


def large_optimum_structures(no_elements, limits):
    """ Analyses the pre-enumerated configurations and finds the longest structure for given limits
    and numbers of elements"""

    # Load the pickled data
    filename = os.path.join(limit_dir(limits),
                            'configurations_{}elements.pkl'.format(no_elements))
    raw = open(filename, 'rb')
    configurations = pkl.load(raw)
    raw.close()
    print('Loaded predefined data at: ' + filename.replace(data_dir(),
                                                           'cantilever_data'))

    # Analyse the portions
    """
    # Do linearly
    analyse_portion(configurations, limits)

    """
    # Do this by through multiprocessing module
    portions = [configurations[i::mp.cpu_count()]
                for i in range(mp.cpu_count())]

    pool = mp.Pool(mp.cpu_count())
    pool.starmap(analyse_portion, zip(portions,
                                      repeat(limits)))
    pool.close()

    # Delete lockfile
    lockfile = glob.glob(os.path.join(limit_dir(limits), '.~lock.{}elements*'.format(no_elements)))
    if lockfile:
        os.remove(lockfile[0])


def analyse_portion(configurations_portion, limits):
    """Analyses this task's prescribed portion of the data"""

    for configuration in configurations_portion:

        # Generate cantilever
        cantilever = Cantilever('from_rows', elements_in_rows=configuration)
        print('Analysing configuration: {}...'.format(cantilever.as_str()))

        # Analyse structure
        [row_criticals, col_criticals, _, ss] = calculate_internals(cantilever)

        # Calculate maximum criticalness
        max_moment = max(max(row_criticals['moment']),
                         max(col_criticals['moment']))
        max_axial = max(max(row_criticals['axial']),
                        max(col_criticals['axial']))

        criticalness = max(calculate_criticalness(max_moment, limits['moment']),
                           calculate_criticalness(max_axial, limits['axial']))

        if criticalness < 1:
            save_stable_structure(criticalness, cantilever, limits)
            print('  ...stable!')
        else:
            print('  ...unstable')


def save_stable_structure(criticalness, cantilever, limits):
    """This configuration is stable, so it is saved"""

    # Define filenames
    filename = '{}elements_{}l.csv'.format(cantilever.no_elements, cantilever.length)
    filepath = os.path.join(limit_dir(limits), filename)
    lockfile = os.path.join(limit_dir(limits), '.~lock.' + filename)

    # String to append
    str_to_write = '{},{}\n'.format(criticalness,
                                    cantilever.as_str())

    # Acquire lock
    rw_lock = fasteners.InterProcessReaderWriterLock(lockfile)
    rw_lock.acquire_write_lock()

    # Add headers if this is the first stable cantilever
    if not os.path.isfile(filepath):
        str_to_write = 'Criticalness,Configuration\n'+str_to_write

    # Append to file
    with open(filepath, 'a') as f:
        f.write(str_to_write)

    # Release lock
    rw_lock.release_write_lock()
