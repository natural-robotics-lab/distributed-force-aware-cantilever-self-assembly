"""File containing the functions used in the sequential algorithm"""
import os

from ...basics import export_dir_path, make_dir, make_or_clean_dir

try:
    from ...structures import Cantilever
except ImportError:
    # Don't need these imports, it's just for type hinting
    pass


class PlacementOption:
    """Class for the placement options of the new element. Initialises
    with all fields empty

    Fields:
        column              the column of this option
        urgency     	    measured stress / allowable stress
        mechanism           is this reinforcement for 'axial' or 'moment' (or 'top')
    """

    def __init__(self, col, urgency, mechanism):
        self.column = col
        self.urgency = urgency
        self.mechanism = mechanism

    def copy(self):
        """Returns a copy of the current placement option"""
        return PlacementOption(self.column, self.urgency, self.mechanism)


def make_export_directory(local, deterministic, simple, specials_before, specials_after,
                          limits, trial, save, plots):
    """Sets the path for the export directory, and creates the directory if necessary"""

    export_dir = export_dir_path('sequential', local, deterministic, simple, specials_before,
                                 specials_after, limits)

    print('Directory name: ' + export_dir)

    image_dir = os.path.join(export_dir, 'TEMP_Trial_{}'.format(trial))

    if save:
        # Make output directories if they don't exist, clear them if they do
        if plots != 'Final':
            make_dir(export_dir)
            make_or_clean_dir(image_dir)

        # Delete old .csv output
        try:
            os.remove(os.path.join(export_dir, 'Trial_{}.csv'.format(trial)))
        except FileNotFoundError:
            pass

    return export_dir, image_dir


def print_test_info(limit_types, limits):
    """Prints the information about the current test"""

    print('Beginning test with:')

    if 'allowable' in limit_types:
        print('  Allowable moment = {} Ndm'.format(limits['allowable']['moment']))
        print('  Allowable axial force = {} N'.format(limits['allowable']['axial']))
    if 'failure' in limit_types:
        print('  Failure moment = {} Ndm'.format(limits['failure']['moment']))
        print('  Failure axial force = {} N'.format(limits['failure']['axial']))


def update_top_row_readings(cantilever: 'Cantilever', top_row, elements, deterministic):
    """Updates the value of max_internals in the top row elements"""

    for col in range(cantilever.col0, cantilever.no_cols-cantilever.col_offset+1):
        # Find the maximum criticalness in this column

        max_internals = {'row': {'moment': 0.0,
                                 'axial': 0.0},
                         'col': {'moment': 0.0,
                                 'axial': 0.0}}

        for element in [e for e in elements if e.location[1] == col]:
            # Pick out elements in this column

            # Get the maximum internals for this element
            current_internals = {'row': {'moment': 0.0,
                                         'axial': 0.0},
                                 'col': {'moment': 0.0,
                                         'axial': 0.0}, }

            for internal_type in current_internals['row']:
                # Take row link on left
                current_internals['row'][internal_type] = element.row_internals[internal_type][0]

                # Take maximum of col links
                current_internals['col'][internal_type] = max(element.col_internals[internal_type])

                for link_type in current_internals:
                    if current_internals[link_type][internal_type] > max_internals[link_type][internal_type]:
                        # Update max encountered criticalness so far
                        max_internals[link_type][internal_type] = current_internals[link_type][internal_type]

        for top_row_element in [e for e in top_row if e.location[1] == col]:
            # Pick out the top row element of this column
            top_row_element.top_row.set_max_internals(max_internals, deterministic)


def update_top_row_list(top_row, active_element):
    """Updates the top row list when an element is placed above the top row"""

    if active_element.location[0] < 0:
        # The column number we want to update
        col = active_element.location[1]

        # Go through all elements until we find the one of interest
        for idx, top_row_element in enumerate(top_row):
            if top_row_element.location[1] == col:
                entry_exists = True
                break
            else:
                entry_exists = False

        if entry_exists:
            # Overwrite the element at that index
            top_row[idx].is_top_row = False
            top_row[idx] = active_element
        else:
            # Add a new element at the start of the list
            top_row.insert(0, active_element)

        # Set the active element to a top row element
        active_element.make_top_row_element(0)


def update_edges_list(edges, active_element):
    """Updates the edges list when an element is placed"""

    if active_element.location[0] == 0:
        # Top row: add two copies at midpoint
        midpoint = int(len(edges)/2)
        edges.insert(midpoint, active_element)
        edges.insert(midpoint+1, active_element)

    elif active_element.location[0] > 0:
        # Bottom of cantilever: find correct column and replace that element
        col = active_element.location[1]

        # Index of second occurance
        idx = [i for i, element in enumerate(edges)
               if element.location[1] == col][1]

        edges[idx] = active_element
    else:
        raise ValueError('Placed on top of cantilever')
