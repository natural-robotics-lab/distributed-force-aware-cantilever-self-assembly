"""compare_sequential mode"""

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from ...basics import collated_label, data_dir, make_dir, show_or_save
from ...graphics.colours import BLUE, colours_iterator, custom_sns_palette
from .collate import averages, generate_barplot_df
from .shared import add_origin, limits_in_directory, read_optimal_data


def compare_sequential_versions(algorithm_names, elements_per_trial, plots):
    """Compares the different versions of the sequential algorithm"""

    # Make output directories if necessary
    output_dir = os.path.join(data_dir(),
                              'Results',
                              'comparisons',
                              'sequential-'+'-'.join(algorithm_names))
    if 'save' in plots:
        make_dir(output_dir)

    figure = plot_length_steps(algorithm_names, elements_per_trial)
    show_or_save(figure, plots, os.path.join(output_dir,
                                             'Elements to length'))

    figure = plot_criticalness_barplot(algorithm_names)
    show_or_save(figure, plots, os.path.join(output_dir,
                                             'Barplot'))


def common_link_strengths(algorithm_names):
    """Searches through the results for the given algorithms extracts the link strengths that are
    common to all of them"""

    link_strengths = []

    for algorithm_name in algorithm_names:
        for limits in limits_in_directory('sequential_'+algorithm_name):

            # Add limits to link_strengths if not already
            if limits not in link_strengths:
                link_strengths.append(limits)

    return link_strengths


def plot_length_steps(algorithm_names, elements_per_trial):
    """Draws a comparison figure for all limits"""

    # Import data
    link_strengths = common_link_strengths(algorithm_names)

    # Initialise figure with correct number of subplots
    plt.rcParams["font.family"] = "Times New Roman"
    plt.rcParams["font.size"] = "20"
    plt.rcParams["mathtext.fontset"] = "stix"
    fig, axs = plt.subplots(1, len(link_strengths),
                            figsize=(700/96*len(link_strengths),
                                     500/96),
                            dpi=96, sharey=True)

    # Axs needs to be a list
    if not isinstance(axs, np.ndarray):
        axs = [axs]

    for algorithm_name, colour in zip(algorithm_names, colours_iterator(start=1)):
        algorithm_dir = os.path.join(data_dir(), 'Results', 'sequential_{}'.format(algorithm_name))

        algorithm_label = algorithm_name.replace('passing', ' passing').capitalize()

        for axes_idx, limits in enumerate(link_strengths):
            # Create one plot per limits set

            # Load and plot data and errorbars
            limit_dir = os.path.join(algorithm_dir, '{}m_{}a'.format(limits['moment'],
                                                                     limits['axial']))
            elements_to_length = add_origin(averages(limit_dir, limits,
                                                     elements_per_trial)[0])

            axs[axes_idx].plot(elements_to_length['No. elements'], elements_to_length['Average length'],
                               label=algorithm_label.capitalize(), color=colour)

            axs[axes_idx].fill_between(elements_to_length['No. elements'],
                                       elements_to_length['5% quartile'],
                                       elements_to_length['95% quartile'],
                                       alpha=0.2, facecolor=colour)

    # Load and plot optimums
    for axes_idx, limits in enumerate(link_strengths):
        opt_elements_to_length = add_origin(read_optimal_data(limits))
        axs[axes_idx].plot(opt_elements_to_length['No. elements'],
                           opt_elements_to_length['Length'],
                           label='Optimal', color=BLUE)

        # Add titles and limits
        axs[axes_idx].set_title(collated_label(limits['moment'], limits['axial'], links=True),
                                fontweight='bold')
        axs[axes_idx].set_xlim([0, elements_per_trial])

    # Create large hidden frame to give global xaxis title
    fig.add_subplot(111, frameon=False)
    fig.axes[-1].tick_params(labelcolor='none', top=False,
                             bottom=False, left=False, right=False)
    fig.axes[-1].set_xlabel('Number of agents in structure')
    fig.axes[0].set_ylabel('Maximum stable\ncantilever length (dm)')

    axs[0].set_ylim([0, None])
    axs[-1].legend(loc='lower right')

    return fig


def plot_criticalness_barplot(algorithm_names):
    """Plots a comparison of the criticlanesses for each algorithm"""

    barplot_dfs = []

    # Generate the dataframes
    for algorithm_name in algorithm_names:
        algorithm_dir = os.path.join(data_dir(), 'Results', 'sequential_{}'.format(algorithm_name))
        algorithm_df = generate_barplot_df(algorithm_dir)

        # Convert to different format
        link_strengths = []
        moments = []
        axials = []

        for link_strength in algorithm_df['Link strength'].unique():
            # Number of entries with this link strength (half from moment, half from axial)
            no_entries = len(algorithm_df[algorithm_df['Link strength'] == link_strength])//2

            link_strengths.extend([link_strength] * no_entries)

            moments.extend(
                algorithm_df[(algorithm_df['Limit type'] == 'Moment') &
                             (algorithm_df['Link strength'] == link_strength)]['Maximum criticalness'].tolist())
            axials.extend(
                algorithm_df[(algorithm_df['Limit type'] == 'Axial force') &
                             (algorithm_df['Link strength'] == link_strength)]['Maximum criticalness'].tolist())

            if not (len(link_strengths) == len(moments) == len(axials)):
                raise ValueError('Lists are of different lengths')

        algorithm_name_list = [algorithm_name.replace('passing',
                                                      ' passing').capitalize()] * len(link_strengths)

        barplot_dfs.append(pd.DataFrame(list(zip(algorithm_name_list, link_strengths,
                                                 moments, axials)),
                                        columns=['Algorithm', 'Link strength',
                                                 'Maximum moment criticalness',
                                                 'Maximum axial force criticalness']))

    barplot_df = pd.concat(barplot_dfs)

    plt.rcParams["font.family"] = "Times New Roman"
    plt.rcParams["font.size"] = "20"
    plt.rcParams["mathtext.fontset"] = "stix"

    # Plot comparison of M and F on the same graph
    fig, axes = plt.subplots(1, 2, figsize=(260/96*5, 560/96), dpi=96, sharey=True)
    plt.subplots_adjust(wspace=0.01)

    for col_title, axis in zip(['Maximum moment criticalness', 'Maximum axial force criticalness'],
                               axes):
        sns.set_palette(custom_sns_palette(start=1))

        sns.barplot(x='Link strength', y=col_title, ci='sd',
                    data=barplot_df, hue='Algorithm', ax=axis)

        axis.spines['right'].set_color('none')
        axis.spines['top'].set_color('none')

        axis.set_xlabel('')
        axis.set_ylabel('')
        axis.axhline(1, 0, 1, linestyle='dashed', color='k')

        # Only show one legend
        if axis == axes[0]:
            axis.get_legend().remove()

    axes[0].text(0.5, 1, 'Moment', horizontalalignment='center', fontweight='bold',
                 verticalalignment='top', transform=axes[0].transAxes)
    axes[1].text(0.5, 1, 'Axial force', horizontalalignment='center', fontweight='bold',
                 verticalalignment='top', transform=axes[1].transAxes)

    # Create large hidden frame to give global xaxis title
    fig.add_subplot(111, frameon=False)
    fig.axes[-1].tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
    fig.axes[-1].set_xlabel('Link strength')
    fig.axes[0].set_ylabel('Maximum criticalness')

    return fig
