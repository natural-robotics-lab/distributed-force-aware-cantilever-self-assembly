"""The different subplots contained in the visualisation figures"""

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.patches import Rectangle

from ..basics import active_element_override, calculate_criticalness
from .colours import (ALLOWABLE_CMAP, BASIC_CMAP, BLUE, GREY, ORANGE, YELLOW,
                      failure_cmap)


class StructurePlot:
    """Class object for the structure axis"""

    def __init__(self, parent, margins, title_kwargs, axis, axis_cbar=None, units='dm',
                 axis_labels='y'):
        self.parent = parent
        self.margins = margins

        self.axis = axis
        self.title_kwargs = title_kwargs

        self.fixed_support = None

        self.blocks_obj = []

        # Set axis to 1:1 ratio
        if self.margins == 'wide':
            self.axis.axis('scaled')
        elif self.margins != 'tight':
            raise ValueError("Margins must be either 'tight' or 'wide' ")

        self.obj_scatter = None
        self.obj_cbar = None

        self.axis_cbar = axis_cbar

        self.units = '({})'.format(units)

        # Initialise figure and titles
        self.auto_title(init=True)
        if 'x' in axis_labels:
            self.axis.set_xlabel('Structure length ' + self.units)
        if 'y' in axis_labels:
            self.axis.set_ylabel('Structure depth ' + self.units)

    def auto_title(self, structure=None, init=False):
        """Automatically sets the title and its font"""

        if init:
            # Initialising
            title_str = 'Initialising...'
        else:
            title_str = 'Cantilever with {} element'.format(structure.no_elements)
            if structure.no_elements > 1 or structure.no_elements == 0:
                title_str += 's'

        self.axis.set_title(title_str, self.title_kwargs)

    def auto_limits(self, structure, predrawn=True):
        """Sets the limits automatically"""

        # Extract the shape of the structure
        structure_x = np.array([structure.col0, structure.length])

        max_height = max(np.sum(structure.map[:structure.row_offset, ] != 0, axis=0))
        structure_y = np.array([-(structure.no_rows-max_height)+0.5, max_height+0.5])

        # Add 1 to everything if it's an active plot
        if self.margins == 'wide':
            structure_x += np.array([-1, 1])
            structure_y += np.array([-1, 1])
        elif self.margins == 'tight':
            structure_y += np.array([-0.5, 0])

        else:
            raise ValueError("Margins must be either 'tight' or 'wide' ")

        if predrawn:
            # If already drawn an axis, never make it smaller, only larger
            xlim = (min(self.axis.get_xlim()[0], structure_x[0]),
                    max(self.axis.get_xlim()[1], structure_x[1]))
            ylim = (min(self.axis.get_ylim()[0], structure_y[0]),
                    max(self.axis.get_ylim()[1], structure_y[1]))
        else:
            # First time drawing axis, so can be optimal
            xlim = structure_x
            ylim = structure_y

        # Set limits
        self.axis.set_xlim(xlim)
        self.axis.set_ylim(ylim)

    def plot_structure(self, structure, predrawn=True):
        """Plots a representation of the structure

        Inputs:
            structure   the structure object
            predrawn    whether this structure has already been drawn (affects limits)
        """
        # Clear plot if it exists
        if self.blocks_obj:
            for obj in self.blocks_obj:
                obj.remove()
            self.blocks_obj = []

        # Extract variables
        col_offset = structure.col_offset
        extension = structure.length

        # Trim structure
        structure_map_trimmed = structure.map[:, 0: col_offset+extension]

        # Extract dimensions
        M, N = structure_map_trimmed.shape

        # Create copy with 0 in place of -1
        structure_map_binary = np.copy(structure_map_trimmed)
        structure_map_binary[structure_map_binary == -1] = 0

        # Set axis limits and titles
        self.auto_title(structure)
        self.auto_limits(structure, predrawn)

        # Create matrix of X values we'd like to plot points at
        # Value is 0 otherwise
        X = np.multiply(structure_map_binary,
                        np.tile(np.arange(0.5-col_offset, extension+0.5),
                                (M, 1)))

        # Loop over each element and plot it if it's non-zero
        for i in range(M):
            for j in range(N):
                if X[i][j] != 0:
                    # Plot a square at the given centre of correct colour
                    c = [X[i][j], -i+structure.row_offset]

                    self.blocks_obj.append(self.plot_square(c, BLUE))

        # Plot fixed support
        self.plot_fixed_support(structure)

    def plot_square(self, c, colour):
        """Plots a square at centre C, size l, and of given colour

        Inputs:
            c[x, y]     coordinate of centre
            colour      colour as RGB tuple
        Outputs:
            rect        the object of the fill shape (we don't need to save it)
        """
        L = 0.95

        bl = np.array(c) + np.array([-L/2, -L/2])

        patch = Rectangle(bl, L, L,
                          edgecolor=colour,
                          facecolor=colour)

        self.axis.add_patch(patch)

        return patch

    def plot_id(self, centre, unique_id, colour='k'):
        """Draws the unique element ID on the plot too"""

        text = self.axis.text(centre[0], centre[1],
                              str(unique_id),
                              ha='center',
                              va='center',
                              color=colour)
        return text

    def plot_fixed_support(self, structure):
        """ Plots the boundary of the fixed support if it exists"""

        if -1 in structure.map:
            # Only plot if required

            # Delete old boundary if it exists
            if self.fixed_support:
                for side in self.fixed_support:
                    self.fixed_support[side].remove()

            # Left support
            width = (structure.col_offset+1)*10
            height = (structure.no_rows-structure.row_offset+2.5)*10
            bottom_left = (-width, -height+0.5)

            # Define corner coords
            rect = Rectangle(bottom_left, width, height,
                             edgecolor='k', facecolor=GREY)

            self.fixed_support = {'left': self.axis.add_patch(rect)}

    def plot_link_criticalness(self, link_internals, structure, limits, mode, cbar=False):
        """ Plots the stresses in each link on the figure

        Inputs:
            ax                  axis to plot these on
            link_internals      dictionary with fields 'stress', 'axial', and 'moment', each
                                of which is a Mx2N array of corresponding measurement in
                                each link
            structure           the structure object
            limits              'stress', 'axial', and 'moment' limits
            mode                whether we're plotting 'allowable' or 'failure' loads
            cbar                boolean, whether to draw the colourbar
        Outputs:
            obj_scatter         the scatter plot object
            obj_cbar            the colourbar object
        """
        # Remove existing scatterplot
        try:
            self.obj_scatter.remove()
        except AttributeError:
            # Not yet drawn
            pass

        # Calculate dimensions
        M, N = structure.calculate_size()

        # Calculate offsets
        row_offset, col_offset = structure.calculate_offset()

        # Marker size decreases with number of rows
        link_size = self.set_link_size()

        # Generate x and y grid matrices and initialise to NaN
        grid_x = np.empty((M, 2*N))
        grid_x[:] = np.NaN
        grid_y = np.empty((M, 2*N))
        grid_y[:] = np.NaN

        # Loop over structure and assign correct coordinates in grids
        for r in range(M):
            for c in range(N):
                # Only add coordinates if there's an element here
                if draw_link_left(structure, r, c):
                    # Add coordinates to the left if necessary
                    if ((c-col_offset == 0 and r-row_offset >= 0)
                        or (c > 0 and structure.map[r][c-1] in [1, 2, 3])) \
                            and active_element_override('left', [r, c], structure):
                        grid_x[r, 2*c] = c-col_offset
                        grid_y[r, 2*c] = -r+row_offset

                    # Add coordinate above if necessary
                    if (r != 0 and structure.map[r-1][c] in [1, 2, 3]) \
                        and active_element_override('above', [r, c],
                                                    structure):
                        grid_x[r, 2*c+1] = c + 0.5-col_offset
                        grid_y[r, 2*c+1] = -r + 0.5+row_offset

                if structure.map[r][c] == -1 and structure.map[r-1][c] in [1, 2, 3]:
                    # Also add above the boundary
                    grid_x[r, 2*c+1] = c + 0.5-col_offset
                    grid_y[r, 2*c+1] = -r + 0.5+row_offset

        # Flatten matrices
        x = grid_x.flatten()
        y = grid_y.flatten()
        link_internals_flattened = {}

        for key in ['moment', 'axial']:
            link_internals_flattened[key] = link_internals[key].flatten()

        if mode != 'failure':
            no_limits = 0
            for key in ['moment', 'axial']:
                link_internals_flattened[key] = link_internals[key].flatten()
                # Count how many limits are required
                if limits[key]:
                    no_limits += 1
        else:
            # Failure requires two limits to be specified
            no_limits = 2

        # If no limits are given, criticalness is a function of the greatest value
        if no_limits == 0:
            local_limits = {'moment': link_internals['moment'].max(),
                            'axial': link_internals['axial'].max()}

        elif mode == 'failure':
            local_limits = limits['allowable']
        else:
            local_limits = limits

        # Calculate criticalness for each criteria
        z = np.empty(len(x))
        criticalness = {'moment': 0.0, 'axial': 0.0}
        for idx, _ in enumerate(z):
            # Set criticalness of each measurement
            for key in ['moment', 'axial']:
                criticalness[key] = calculate_criticalness(link_internals_flattened[key][idx],
                                                           local_limits[key])

            # Assign criticalness to z
            z[idx] = max([criticalness['moment'], criticalness['axial']])

        # Set colourbar scale and map
        if mode == 'allowable' and no_limits > 0:
            cmap = ALLOWABLE_CMAP
            cmap.set_over('black')
            ticks = [0, 0.5, 1, 1.4]
            ticklabels = None
            vmax = 1.4
        elif mode == 'failure':
            cmap, ticks, ticklabels, vmax = failure_cmap(
                limits['allowable'], limits['failure'])
        else:
            cmap = BASIC_CMAP
            ticks = [0, 0.25, 0.5, 0.75, 1]
            ticklabels = None
            vmax = 1

        # Plot on graph
        self.obj_scatter = self.axis.scatter(x, y, marker='s', s=link_size, c=z, cmap=cmap,
                                             vmin=0, vmax=vmax, zorder=2)

        # Plot colourbar if necessary
        if cbar:
            if self.axis_cbar:
                self.obj_cbar = self.parent.figure.colorbar(self.obj_scatter, cax=self.axis_cbar,
                                                            ticks=ticks)
            else:
                self.obj_cbar = plt.colorbar(self.obj_scatter, ax=self.axis,
                                             ticks=ticks)
            self.obj_cbar.ax.set_ylabel('Link criticalness')
            if ticklabels is not None:
                self.obj_cbar.ax.set_yticklabels(ticklabels)
        else:
            self.obj_cbar = None

        return self.obj_scatter, self.obj_cbar

    def set_link_size(self):
        """Sets the link size"""

        M = self.axis.transData.get_matrix()
        xscale = M[0, 0]

        half_height = 0.1

        # Best guess at link size
        link_size = (xscale*half_height)**2

        # If too large, set to a default value
        if link_size > 1000:
            link_size = 50
        return link_size


def draw_link_left(structure, row, column):
    """When plotting links, should we plot a link left of this location"""

    if structure.map[row][column] in [1, 2]:
        return True


class MomentForcePlots:
    """Class object for the moment and axial force axes"""

    def __init__(self, parent, axes, title_kwargs,  units):
        self.parent = parent

        self.axes = axes
        self.title_kwargs = title_kwargs
        self.line_objs = {}
        self.obj_internals_noisy = {}

        self.units = '({})'.format(units)
        self.initialise_titles()

    def initialise_titles(self, ):
        """Sets the titles"""

        # Initialise titles
        self.axes['moment'].set_title('Maximum magnitude of moment',
                                      self.title_kwargs)
        self.axes['moment'].set_ylabel('Maximum moment\nmagnitude (Ndm)')
        self.axes['axial'].set_title('Maximum positive axial force',
                                     self.title_kwargs)
        self.axes['axial'].set_ylabel('Maximum positive\naxial force (N)')
        self.axes['axial'].set_xlabel('Distance along structure ' + self.units)

    def plot_axial_and_moment(self, row_criticals, col_criticals, limits, modes, length=0, col0=0,
                              vertical_margin=1.4):
        """ Plots the axial force and moment(row and column links). Select what figure and / or
        subfigure you want to plot this on first.

        Inputs:
            row_criticals       maximum 'stress', 'axial', or 'moment' in row links down each column
            col_criticals       maximum 'stress', 'axial', or 'moment' in column links down each
                                column
            limits              {'allowable: {axial', 'moment'},
                                 'failure:   {axial', 'moment'}}
            modes               list containing 'allowable' and / or 'failure'
            length              the length of the structure beyond column 0 (more of an extension)
            col0                the column in which to plot the first element of the stress matrix
            vertical_margin     the multiple of the allowable allowable limit to scale the y axis by
        Outputs:
            obj as described above
        """
        # If row and col criticals are None, then we're only initialising
        if not row_criticals and not col_criticals:
            # Initialise arrays to zeros
            row_x = np.zeros((1, 1))
            col_x = np.zeros((1, 1))

            row_criticals = {'axial': np.zeros((1, 1)),
                             'moment': np.zeros((1, 1))}
            col_criticals = {'axial': np.zeros((1, 1)),
                             'moment': np.zeros((1, 1))}
        else:
            # Calculate x coordinates
            row_x = np.arange(col0, length+1, 1)
            col_x = np.arange(0.5+col0, length, 1)

        if self.line_objs:
            # Replotting, so clear existing traces
            for key in self.line_objs:
                for nested_key in self.line_objs[key]:
                    try:
                        self.line_objs[key][nested_key].pop(0).remove()
                    except AttributeError:
                        self.line_objs[key][nested_key].remove()

        # Create each axis sequentially
        for key in self.axes:
            # Set axis limits if elements have been added
            if length > 0:
                self.axes[key].set_xlim(col0, length)

            # Plot lines in list of drawing objects to delete later
            self.line_objs[key] = {'row': self.axes[key].plot(row_x, row_criticals[key],
                                                              color=BLUE, linewidth=1,
                                                              label='Row\nlinks'),
                                   'col': self.axes[key].plot(col_x, col_criticals[key],
                                                              color=ORANGE, linewidth=1,
                                                              label='Column\nlinks')}

            # Extract limits from nested dictionary
            if len(modes) == 1:
                allowable_limit = False
                failure_limit = False
                if 'allowable' in modes:
                    if 'moment' in limits.keys():
                        allowable_limit = limits[key]
                    else:
                        allowable_limit = limits['allowable'][key]
                elif 'failure' in modes:
                    if 'moment' in limits.keys():
                        failure_limit = limits[key]
                    else:
                        failure_limit = limits['failure'][key]
                else:
                    raise ValueError('Invalid mode')
            else:
                allowable_limit = limits['allowable'][key]
                failure_limit = limits['failure'][key]

            # Plot allowable limit and scale y-axis
            limit_set = False

            if allowable_limit and failure_limit:
                allowable_col = YELLOW
            else:
                allowable_col = 'k'

            if allowable_limit:
                limit_set = True
                self.axes[key].set_ylim(0, vertical_margin*allowable_limit)
                self.line_objs[key]['allowable_line'] = self.axes[key].axhline(allowable_limit, 0, 1,
                                                                               linestyle='dashed',
                                                                               color=allowable_col)
            if failure_limit:
                limit_set = True
                self.axes[key].set_ylim(0, 1.1*failure_limit)
                self.line_objs[key]['failure_line'] = self.axes[key].axhline(failure_limit, 0, 1,
                                                                             linestyle='dashed',
                                                                             color=ORANGE)
            if not limit_set:
                ymax = 1.2 * \
                    max(max(row_criticals[key]), max(col_criticals[key]))
                if ymax > 0.02:
                    self.axes[key].set_ylim(0, ymax)
                else:
                    # Prevent axes becoming too small
                    self.axes[key].set_ylim(0, 10)

            # Show legend
            self.axes[key].legend(loc='upper right', ncol=2)

        return self.line_objs
