"""The sequential cantilever construction algorithm"""
import os
import re

import pandas as pd

from ...basics import Maximums, data_dir, is_stable
from ...graphics.arrangements import ThreePlotVertical
from ...graphics.colours import GREEN
from ...graphics.video import video_from_directory
from ...modelling.anamodel import calculate_internals
from ...structures import Cantilever
from ..sequential import seqfunc as sequential
from ..sequential.element import Element


def run_algorithm(max_elements=20,
                  limits={},
                  trial=0,
                  local=False,
                  deterministic=False,
                  simple=False,
                  plots='All',
                  save=False,
                  specials_before='',
                  specials_after='',
                  save_stable_structures=False):
    """Runs the sequential cantilever construction algorithm"""

    # Check inputs are valid
    if not limits['allowable']['moment']:
        raise Exception('Allowable moment limit must be set')
    if simple and deterministic:
        raise Exception("Simple version must be stochastic")

    # Set the type of limits we have
    limit_types = []
    if limits['allowable']['moment']:
        limit_types.append('allowable')
    if limits['failure']['moment']:
        limit_types.append('failure')

    # Print test information
    sequential.print_test_info(limit_types, limits)

    # Set up export directories
    export_dir, image_dir = sequential.make_export_directory(local, deterministic, simple,
                                                             specials_before, specials_after,
                                                             limits, trial, save, plots)

    # Initialise variables
    elements = [Element()]
    cantilever = Cantilever(mode='empty', size=max_elements)
    if local:
        edges = []          # Elements around the edges
    else:
        top_row = []        # Elements in top row
    lengths = []

    # Additional data structures
    maximums = Maximums()
    stables = []
    steps_taken = [2]   # First element takes 2 steps to place

    # Manually place first element
    elements[0].location = [0, 0]
    elements[0].place('top', [0, 0])
    cantilever.insert_element(elements[0])
    lengths.append(1)

    if local:
        edges.append(elements[0])   # Append twice
        edges.append(elements[0])
    else:
        top_row.append(elements[0])

    [row_criticals, col_criticals,
     complete_link_internals, ss] = calculate_internals(cantilever)

    # Prepare figure
    figure = ThreePlotVertical(mode='auto', auto_plots=plots, auto_save=save)

    new_square = elements[0].plot(figure, GREEN)
    figure.structure_plot.plot_fixed_support(cantilever)
    figure.structure_plot.plot_link_criticalness(complete_link_internals,
                                                 cantilever,  limits['allowable'],
                                                 'allowable', cbar=True)
    figure.structure_plot.auto_title(cantilever)
    figure.structure_plot.auto_limits(cantilever)

    figure.moment_force_plots.plot_axial_and_moment(row_criticals,
                                                    col_criticals, limits,
                                                    limit_types, cantilever.length,
                                                    cantilever.col0)

    figure.update_now()

    # Export figure and cantilever
    figure.export_figure_and_structure(save, plots, cantilever, image_dir)

    for i in range(1, max_elements):
        print(' ')
        print('Placing element {} (starting at step {})...'.format(i+1,
                                                                   sum(steps_taken)))
        elements.append(Element([-1, cantilever.col0-1],
                                figure))
        active_element = elements[i]

        if is_stable(row_criticals, col_criticals, limits['allowable']):
            # Save stable configuration before changing it
            stables.append([cantilever.length, cantilever.no_elements,
                            cantilever.as_str()])

        # Update virtual sensors of elements in the cantilever
        for j in range(i):
            # print('    Updating sensors of element ' + str(j))
            elements[j].update_sensors(complete_link_internals,
                                       cantilever)

        if local:
            # New element moves around the whole cantilever
            active_element.gather_data(edges, cantilever, limits['allowable'],
                                       figure, local, deterministic, simple)
        else:
            # Update the maximum stress in each column
            sequential.update_top_row_readings(cantilever, top_row, elements, deterministic)
            # New element moves along the top, gathering force readings
            active_element.gather_data(top_row, cantilever, limits['allowable'],
                                       figure, local, deterministic, simple)

        # At the end, decide what to do
        if not active_element.placement_options:
            # Attach to the end if stress is below limit

            if local:
                # Move back to the tip
                while active_element.location[1] < cantilever.length-1:
                    active_element.make_step('right', cantilever, figure)
            else:
                # Set the active element to a top row element
                top_row.append(active_element)
                active_element.make_top_row_element(1)

            # print('    Attaching to end')
            active_element.place('top', [0, cantilever.length])
            active_element.steps_taken += 2

        else:
            active_element.try_placement_options(cantilever, figure, deterministic)

        # Update cantilever map and types
        print('...placed at {} (took {} steps)'.format(active_element.location,
                                                       active_element.steps_taken))
        cantilever.insert_element(active_element)

        # Update the elements in the edge lists
        if local:
            sequential.update_edges_list(edges, active_element)
        else:
            sequential.update_top_row_list(top_row, active_element)

        # Calculate the force distribution on this cantilever
        [row_criticals, col_criticals,
         complete_link_internals, ss] = calculate_internals(cantilever)

        # Update figure
        new_square.remove()                         # Remove old green square
        elements[i-1].plot(figure)                  # Replot old square
        new_square = active_element.plot(figure, GREEN)

        figure.structure_plot.plot_fixed_support(cantilever)
        figure.structure_plot.plot_link_criticalness(complete_link_internals,
                                                     cantilever,
                                                     limits['allowable'],
                                                     'allowable')
        figure.structure_plot.auto_title(cantilever)
        figure.structure_plot.auto_limits(cantilever)

        figure.moment_force_plots.plot_axial_and_moment(row_criticals, col_criticals,
                                                        limits, limit_types,
                                                        cantilever.length, cantilever.col0)

        figure.update_now()

        # Export figure and cantilever
        figure.export_figure_and_structure(save, plots, cantilever, image_dir)

        lengths.append(cantilever.length)

        # Update maximums
        maximums.update(row_criticals, col_criticals, cantilever)

        # Update steps taken
        steps_taken.append(active_element.steps_taken)

        # Break if the failure strength if exceeded
        if 'failure' in limit_types:
            if max(max(row_criticals['moment']), max(col_criticals['moment'])) > limits['failure']['moment']:
                print('Moment limit exceeded; breaking')
                break
            if max(max(row_criticals['axial']), max(col_criticals['axial'])) > limits['failure']['axial']:
                print('Axial force limit exceeded; breaking')
                break

    # Replot last element in correct colour
    new_square.remove()
    active_element.plot(figure)
    print(' ')
    print('Simulation finished')
    print('Final length of cantilever: {} dm'.format(cantilever.length))

    print(maximums)
    print('')
    print(lengths)

    print('Final configuration: ' + cantilever.as_str())

    if save_stable_structures:
        # Convert to panda then save
        stables.append([maximums.maximums['moment'], maximums.structures['moment'].as_str(),
                        '<-- max M'])
        stables.append([maximums.maximums['axial'], maximums.structures['axial'].as_str(),
                        '<-- max F'])
        stables_df = pd.DataFrame(
            stables, columns=['Length', 'No. elements', 'Configuration'])
        try:
            stables_df.to_csv(os.path.join(data_dir(),
                                           'Results',
                                           re.search(r'(.+)_(\d+)m_(\d+)a',
                                                     os.path.basename(export_dir)).group(1),
                                           '{}m_{}a'.format(limits['allowable']['moment'],
                                                            limits['allowable']['axial']),
                                           'Trial_{}.csv'.format(trial)),
                              index=False)
        except FileNotFoundError:
            print("Output directory doesn't exist: can't save stable configurations")

    if save:
        # Convert images to video
        video_from_directory(image_dir, 'step_')

    figure.show_or_save(plots, save, cantilever, export_dir)

    # Return stable configurations
    return stables
