"""The parallel cantilever construction algorithm"""

from time import time

from ... import basics
from ...graphics.arrangements import ThreePlotVertical
from ...graphics.video import video_from_directory
from ...modelling import anamodel as ana
from ...structures import Cantilever
from . import parallelfunc as parfunc


def run_algorithm(max_elements=20,
                  element_delay=10,
                  limits={},
                  plots='All',
                  save=False,
                  trial=0,
                  specials_before='',
                  specials_after='',
                  save_multiple_trials=False,
                  random=False,
                  simple=False):
    """Runs the parallel cantilever construction algorithm"""

    if random and simple:
        raise ValueError("Can only select one of 'random' and 'simple'")

    # Start timer
    start_time = time()

    # Define test directory
    test_dir, image_dir = parfunc.define_test_directory(limits, element_delay,
                                                        specials_before, specials_after,
                                                        trial, random, simple)

    # Create test directory if required
    if save:
        basics.make_dir(test_dir)
        basics.make_or_clean_dir(image_dir)

    step_no = 0
    maximums = basics.Maximums()
    no_timeouts = 0

    # Print some information about the test
    parfunc.print_test_info(limits, save, element_delay,
                            test_dir, trial, random, simple)

    # Set the type of limits we have
    limit_types = []
    if limits['allowable']['moment']:
        limit_types.append('allowable')
    if limits['failure']['moment']:
        limit_types.append('failure')

    # Initialise figure
    figure = ThreePlotVertical(mode='auto', auto_plots=plots, auto_save=save, step_no=step_no)

    # Initialise element in position (0, 0)
    cantilever = Cantilever(mode='empty', size=max_elements, elements=True)
    cantilever.insert_new_element('parallel', figure, [0, 0], 1)
    cantilever.elements[0, 0].place(cantilever)

    # Prepare figure
    [row_criticals, col_criticals,
     complete_link_internals, ss] = ana.calculate_internals(cantilever)
    figure.structure_plot.plot_fixed_support(cantilever)
    figure.structure_plot.plot_link_criticalness(complete_link_internals,
                                                 cantilever,  limits,
                                                 'failure', cbar=True)
    figure.structure_plot.auto_title(cantilever)
    figure.structure_plot.auto_limits(cantilever)

    figure.moment_force_plots.plot_axial_and_moment(row_criticals, col_criticals,
                                                    limits, limit_types,
                                                    cantilever.length, cantilever.col0,
                                                    vertical_margin=4)
    figure.update_now(step_no)
    figure.export_figure_and_structure(save, plots, cantilever,
                                       image_dir, step_no, True)

    # List of Elements objects to save at the end
    if save or (save_multiple_trials and element_delay != 0):
        elements_export = [cantilever.elements.export()]

    # Extra data when this test is part of multiple trials
    if save_multiple_trials:
        multiple_trials_export = [parfunc.multiple_trials_row(step_no, cantilever,
                                                              complete_link_internals,
                                                              no_timeouts)]

    return_code = 0
    new_element = None
    steps_since_last_addition = 0
    while return_code == 0:
        step_no += 1
        steps_since_last_addition += 1
        print('\nBeginning step {}'.format(step_no))

        # Update previously-calculated forces to be reflected in the elements
        cantilever.elements.update_sensors(complete_link_internals, 'all')

        # Check if another element should be added
        if parfunc.new_active_element(steps_since_last_addition, element_delay,
                                      cantilever.no_elements, max_elements,
                                      len(cantilever.active_elements)):
            cantilever.active_elements.append('init')

        # Permute active elements list
        cantilever.active_elements.shuffle()

        print('Advancing elements in order: {}'.format(cantilever.active_elements))

        # Set all active elements to not have taken their action this step
        for active_element in [e for e in cantilever.active_elements if not isinstance(e, str)]:
            active_element.advanced_this_step = False

        # Advance all the active elements by one step (add any timeouts to the counter)
        for element in cantilever.active_elements:
            if isinstance(element, str):
                # Try to initialise a new element
                if cantilever.element_at([-1, 0]) in [1, 2]:
                    # Can't add if there's an element in position (-1, 0)
                    print('Element at [-1, 0]: cannot add new active element')
                else:
                    new_element = cantilever.insert_new_element('parallel', figure,
                                                                sequential=element_delay == 0)
                    new_element.plot()

                    # Reset counter
                    steps_since_last_addition = 0
            else:
                no_timeouts += element.advance(cantilever, limits['allowable'],
                                               max(element_delay, 2),
                                               random, simple)

        # Add the new element to active_elements if it's there
        if new_element is not None:
            cantilever.active_elements.append(new_element)
            new_element = None

        # Remove any placed elements from active_elements
        cantilever.active_elements.remove_placed_elements()

        # Print debug info
        [print(e) for e in cantilever.active_elements]

        if not(plots == 'All' or save):
            # If not plotting at this stage, print the cantilever map
            print(cantilever.map[:cantilever.no_rows+cantilever.row_offset+1,
                                 :cantilever.length+cantilever.col_offset+1])

        # Solve forces
        [row_criticals, col_criticals,
         complete_link_internals, ss] = ana.calculate_internals(cantilever)

        # Replot graphs
        figure.structure_plot.plot_fixed_support(cantilever)
        figure.structure_plot.plot_link_criticalness(complete_link_internals, cantilever, limits,
                                                     'failure')
        figure.structure_plot.auto_title(cantilever)
        figure.structure_plot.auto_limits(cantilever)

        figure.moment_force_plots.plot_axial_and_moment(row_criticals, col_criticals,
                                                        limits, limit_types,
                                                        cantilever.length, cantilever.col0,
                                                        vertical_margin=4)

        # Update figure and add image to temporary directory if required
        figure.update_now(step_no)
        figure.export_figure_and_structure(save, plots, cantilever,
                                           image_dir, step_no, True)

        # Update maximums
        maximums.update(row_criticals, col_criticals, cantilever)

        # Add to export lists if required
        if save or (save_multiple_trials and element_delay != 0):
            elements_export.append(cantilever.elements.export())
        if save_multiple_trials:
            multiple_trials_export.append(parfunc.multiple_trials_row(step_no, cantilever,
                                                                      complete_link_internals,
                                                                      no_timeouts))

        # Break if done
        if cantilever.no_elements == max_elements and len(cantilever.active_elements) == 0:
            return_code = 1
            print('All elements placed')

        # Break if the failure strength if exceeded
        if 'failure' in limit_types:
            if max(max(row_criticals['moment']),
                   max(col_criticals['moment'])) > limits['failure']['moment']:
                print('Moment limit exceeded; breaking')
                return_code = 2
            if max(max(row_criticals['axial']),
                   max(col_criticals['axial'])) > limits['failure']['axial']:
                print('Axial force limit exceeded; breaking')
                return_code = 3

        # Break if set number of steps exceeded (final element has l^2 steps to place)
        if element_delay > 0 and step_no > \
                (max_elements*element_delay) + pow(cantilever.length, 2) * cantilever.no_rows:
            print('Maximum number of steps exceeded; breaking')
            return_code = 4

    if save:
        # Convert images to video
        video_from_directory(image_dir, 'step_')

        # Export pickled cantilever.elements objects
        parfunc.export_elements(elements_export, test_dir, trial)

    if save_multiple_trials:
        # Define slightly different export directory
        export_dir = parfunc.multiple_trials_dir(test_dir)

        # Export picked cantilever.elements objects for first 5 trials or if failure
        if element_delay != 0 and (trial <= 5 or return_code != 1):
            parfunc.export_elements(elements_export, export_dir, trial)

        # Export data csv
        parfunc.multiple_trials_csv(multiple_trials_export, export_dir, trial)

    print('\nSelf-assembly finished in {:.2f} minutes'.format((time()-start_time)/60))
    print('')
    print(maximums)

    figure.show_or_save(plots, save, cantilever, '')

    return return_code


def convert_return_code(return_code):
    """Algorithm returns a number depending on how it finished"""

    if return_code == 1:
        return_str = 'Finished successfully'
    elif return_code == 2:
        return_str = 'Moment limit exceeded'
    elif return_code == 3:
        return_str = 'Axial force limit exceeded'
    elif return_code == 4:
        return_str = 'Maximum number of steps exceeded'
    else:
        raise ValueError('Invalid return code')

    return return_str
