"""process_limit_pair mode"""

import glob
import os
import re

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from ...basics import show_or_save
from ...graphics.colours import BLUE, GREEN, ORANGE
from .shared import (add_origin, averages, read_optimal_data, test_directory,
                     trial_str2int)


def process_limit_pair(algorithm_name, limits, elements_per_trial, plots):
    """Processes the data for a single limit pair, including comparing to the optimal performance"""

    # Compute average performance
    avg_elements_to_length, metadata, maximums_df = averages(test_directory(algorithm_name, limits),
                                                             limits,
                                                             elements_per_trial)
    display_salient_configs(avg_elements_to_length,
                            test_directory(algorithm_name, limits), plots)

    # Load optimum performance
    optimum_elements_to_length = read_optimal_data(limits)

    # Add (0,0) to the start of the dataframes
    optimum_elements_to_length = add_origin(optimum_elements_to_length)
    avg_elements_to_length = add_origin(avg_elements_to_length)
    print('Average final length = {} dm'.format(
        avg_elements_to_length['Average length'].iloc[-1]))

    # Draw comparison graphs
    draw_comparison_graphs(optimum_elements_to_length, avg_elements_to_length,
                           algorithm_name, elements_per_trial, limits, plots)

    # Export metadata and maximums text files
    metadata.to_csv(os.path.join(test_directory(algorithm_name, limits),
                                 'metadata.csv'))
    maximums_df.to_csv(os.path.join(test_directory(algorithm_name, limits),
                                    'maximums.csv'), index=False)


def display_salient_configs(average_df, path, plots):
    """Prints the final cantilever configurations that are longer and shorter than average, and
    shows them on histograms"""

    length_average = int(average_df.iloc[-1]['Average length'])
    print('Average length = {} elements (rounded down to nearest integer)'.format(
        length_average))

    # Pick out shorter than average
    shorter_configs = compare_to_average_length('shorter', path,
                                                length_average)
    shorter_combined = row_histograms(shorter_configs)

    # Pick out longer than average
    longer_configs = compare_to_average_length('longer', path,
                                               length_average)
    longer_combined = row_histograms(longer_configs)

    # Draw average longer and shorter configuration
    plot_average_configs(longer_combined, shorter_combined, path, plots)


def compare_to_average_length(mode, path, length_average, include_equal=False):
    """Compares the length of each configuration to the average length, and if necessary saves it to
    a list"""

    configs_list = []
    print('{} than average cantilevers:'.format(mode.capitalize()))

    for filename in sorted(glob.glob(os.path.join(path, 'Trial_*.csv')), key=trial_str2int):
        trial_data = pd.read_csv(filename)

        length_trial = trial_data.iloc[-3]['Length']

        # Choose correct comparison
        if mode == 'shorter':
            if include_equal:
                valid_comparison = bool(length_trial <= length_average)
            else:
                valid_comparison = bool(length_trial < length_average)
        elif mode == 'longer':
            if include_equal:
                valid_comparison = bool(length_trial >= length_average)
            else:
                valid_comparison = bool(length_trial > length_average)
        else:
            raise ValueError("Mode must be 'shorter' or 'longer'")

        if valid_comparison:
            config_trial = trial_data.iloc[-3]['Configuration']
            configs_list.append(config_trial)
            trial_no = trial_str2int(filename)
            print('  Trial {} is of length {:.0f}: '.format(trial_no, length_trial) + config_trial)

    # If the list is empty, call again but allow equal to average too
    if not configs_list:
        print('  No cantilevers {m} than int(average), trying {m} or equal to average'.format(
            m=mode))
        configs_list = compare_to_average_length(mode, path, length_average, include_equal=True)

    return configs_list


def row_histograms(configs_list):
    """Plots histograms of elements in each row from a list of configurations"""

    # Find the maximum row offset in the list
    max_rows_above = max([[row_str[0] for row_str in config.split('/')].count('-')
                          for config in configs_list])
    # Find the maximum number of rows below and including row 0 in the list
    max_rows_below = max([len([row_str[0] for row_str in config.split('/') if row_str[0] != '-'])
                          for config in configs_list])

    # Make combined array
    elements_in_rows_combined = np.zeros((len(configs_list), max_rows_above+max_rows_below),
                                         dtype=int)

    no_configs_with_rows_above = 0
    max_row_length = 0
    for idx, config_str in enumerate(configs_list):
        # Information for this configuration
        elements_in_rows = config_str.split('/')
        row_offset = len([first_row for first_row in elements_in_rows
                          if first_row[0] == '-'])
        if elements_in_rows[0][0] == '-':
            no_configs_with_rows_above += 1

        # Convert each entry in elements_in_rows to an int
        elements_in_rows_ints = [int(re.search('(\d+)', elements).group(1))
                                 for elements in elements_in_rows]

        # Insert zeros at the start
        elements_in_rows_ints = (max_rows_above-row_offset)*[0] + elements_in_rows_ints

        # Add to next row in elements_in_rows_combined
        elements_in_rows_combined[idx, :len(elements_in_rows_ints)
                                  ] = np.array(elements_in_rows_ints)
        # Set max row length
        max_row_length = max(max_row_length, max(elements_in_rows_ints))

    print('  {:.1f}% of {} configurations have support above'.format(
        no_configs_with_rows_above/len(configs_list)*100,
        len(configs_list)))

    dataframe = pd.DataFrame(elements_in_rows_combined, columns=['Row {}'.format(x)
                                                                 for x in range(-max_rows_above,
                                                                 max_rows_below)])

    return dataframe


def plot_average_configs(longer_df, shorter_df, path, plots):
    """Plots the average configuration for cantilevers longer and shorter than the average"""

    width = 0.8
    # Initialise graph
    plt.figure(figsize=(869/96, 560/96), dpi=96)
    plt.rcParams["font.family"] = "Times New Roman"
    plt.rcParams["font.size"] = "20"
    plt.rcParams["mathtext.fontset"] = "stix"

    plt.barh([int(x.strip('Row ')) - width/4 for x in longer_df.mean().index.tolist()],
             longer_df.mean().tolist(),
             width/2,
             color=BLUE, label='Longer than average')

    plt.barh([int(x.strip('Row ')) + width/4 for x in shorter_df.mean().index.tolist()],
             shorter_df.mean().tolist(),
             width/2,
             color=ORANGE, label='Shorter than average')

    # Labels
    plt.legend(loc='lower right')
    plt.xlabel('Average number of elements in row')
    plt.ylabel('Row number')
    plt.title('Average number of elements in rows\nfor structures longer and shorter than average',
              fontweight='bold')

    plt.gca().invert_yaxis()

    # Show or save
    show_or_save(plt.gcf(), plots, os.path.join(path, 'Average best and worse'))


def draw_comparison_graphs(optimum_elements_to_length, avg_elements_to_length, algorithm_name,
                           elements_per_trial, limits, plots):
    """Draws the graph comparing the construction algorithm to the optimal"""

    # Initialise graph
    fig = plt.figure(figsize=(869/96, 560/96), dpi=96)
    plt.rcParams["font.family"] = "Times New Roman"
    plt.rcParams["font.size"] = "20"
    plt.rcParams["mathtext.fontset"] = "stix"

    # Add lines
    plt.plot(avg_elements_to_length['No. elements'],
             avg_elements_to_length['Average length'],
             label='Construction algorithm (average)', color=BLUE)

    plt.fill_between(avg_elements_to_length['No. elements'],
                     avg_elements_to_length['5% quartile'],
                     avg_elements_to_length['95% quartile'],
                     alpha=0.2, facecolor=BLUE)

    if optimum_elements_to_length is not None:
        plt.plot(optimum_elements_to_length['No. elements'],
                 optimum_elements_to_length['Length'],
                 label='Optimal', color=GREEN)

    # Titles
    plt.xlabel('Number of elements in structure')
    plt.ylabel('Maximum stable cantilever length (dm)')
    plt.axis([0, elements_per_trial,
              0, None])
    plt.legend(loc='lower right')

    # Show or save
    show_or_save(fig, plots, os.path.join(test_directory(algorithm_name, limits),
                                          'Comparison graph'))
