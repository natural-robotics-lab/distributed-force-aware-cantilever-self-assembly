"""Three plots arranged vertically"""
import os

import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

from .subplots import MomentForcePlots, StructurePlot


def initialise_figure_window(top_left_x, top_left_y, width, height, dpi=96):
    """Creates the figure object, then sets the window position and size in the correct manner,
    regardless of the backend"""

    # Initialise figure window
    figure = plt.figure(figsize=(width/dpi, height/dpi), dpi=dpi)

    if plt.rcParams['backend'] == 'TkAgg':
        plt.get_current_fig_manager().window.wm_geometry("+{}+{}".format(
            top_left_x, top_left_y))
    elif plt.rcParams['backend'] == 'Qt5Agg':
        plt.get_current_fig_manager().window.setGeometry(top_left_x+9,
                                                         top_left_y+30,  # Offset for title bar
                                                         width, height)
    else:
        raise ValueError('Not specified how to manage geometry for "{}" backend'.format(
            plt.rcParams['backend']))

    return figure


def auto_parameter_parser(plots, save):
    """Sets margins and automatic update as required for the automatic inputs"""

    if plots == 'All':
        print('Preparing figures...')
        margins = 'wide'
        interactive = True
    elif plots == 'Final':
        interactive = False
        margins = 'tight'
    elif plots == 'None':
        interactive = False
        if save:
            margins = 'wide'
        else:
            margins = 'tight'
    else:
        raise ValueError("plots must be 'All, 'Final', or 'None'")

    return margins, interactive


def set_fonts(fonts):
    """Sets the font size and family"""
    try:
        fontsize = fonts[fonts.index('_')+1:]
        plt.rcParams["font.size"] = fontsize
    except ValueError:
        # Haven't passed a font size
        fontsize = '10'

    # Set fonts
    if 'standard' in fonts:
        title_kwargs = {'weight': 'bold'}
    elif 'latex' in fonts:
        plt.rcParams['font.family'] = 'serif'
        plt.rcParams['font.serif'] = 'cmr' + fontsize
        plt.rcParams['mathtext.fontset'] = 'cm'
        plt.rcParams['axes.unicode_minus'] = False

        title_kwargs = {'fontname': 'cmb' + fontsize}
    elif 'ieee' in fonts:
        plt.rcParams["font.family"] = "Times New Roman"
        plt.rcParams["mathtext.fontset"] = "stix"

        title_kwargs = {'weight': 'bold'}

    return title_kwargs


class Arrangement():
    # Shared methods for all arrangements

    def show_fig(self):
        """Shows the current figure now (if not in interactive mode)"""
        # If in interactive mode, turn off
        if self.interactive:
            plt.ioff()

        self.figure.tight_layout()      # Necassary to scale subplots correctly
        plt.show()

    def update_now(self, step_no=None):
        """Updates the current figure now now (if in interactive mode)"""

        # Draw step number if required
        if step_no is not None:
            step_text = 'Step {}'.format(step_no)
            if self.obj_step_no is not None:
                # Redraw
                self.obj_step_no.set_text(step_text)
            else:
                # Draw
                self.obj_step_no = self.figure.text(0, 0, step_text)

        if self.interactive:
            self.figure.tight_layout()
            plt.draw()
            plt.pause(0.001)

    def export_figure_and_structure(self, save, plots, structure, image_dir, step_no=None,
                                    pbz2=False):
        """Exports the  figure and the structure object if necessary

        step_no = -1 suppresses the export to output.csv"""

        # Don't save if not requested, or only want the final one
        if (not save) or plots == 'Final':
            return

        if step_no is None:
            # Step number not specified: use number of elements
            step_no = structure.no_elements

        self.figure.tight_layout()      # Necessary to scale subplots correctly
        image_path = os.path.join(image_dir,
                                  'step_{:05d}.png'.format(step_no))
        plt.savefig(image_path, bbox_inches='tight')

        # Just export figure if exporting to pbz2 and video
        if pbz2:
            print('  Exported figure')
            return

        if 'auto-generated' in image_dir:
            # Export to output.csv
            trial = int(image_dir.split('_')[-1])
            structure.export_list(os.path.dirname(image_dir), step_no, trial)

        print('    Exported figure and structure: ' + image_path)

    def show_or_save(self, plots, save, structure=None, export_dir=None):
        """Shows or saves the final plot"""

        if plots == 'All' or (plots == 'Final' and not save):
            # Show if not saving
            self.show_fig()
        elif plots == 'Final' and save:
            # If save and 'Final', save final state
            final_string = export_dir[:-12] + \
                str(structure.no_elements).zfill(3)+'_elements'

            self.figure.tight_layout()
            plt.savefig(final_string+'.png', bbox_inches='tight')

            print('Saved to: ' + final_string)
        plt.close(self.figure)


class ThreePlotVertical(Arrangement):
    """Class object for three plots oriented vertically

    Class variables:
        show                whether to show 'All', 'None' or 'Final' plots
        figure              the PyPlot Figure object
        structure_plot      the StructurePlot object
        moment_force_plots  the MomentForcePlots object
        units               Axis units
        obj_step_no         Step number object (if it exits)
        interactive         Bool; whether the figure is in interactive mode
    """

    def __init__(self, mode, units='dm', fonts='standard', step_no=None, **kwargs):
        """ Initialises the plot object


        Common inputs:
            mode                'auto' or 'manual'
            units               Whether axis units are 'dm' or 'bodylengths'
            fonts               'standard', 'ieee', or 'latex' fonts. Add '_fontsize' to
                                set fontsize, eg. 'ieee_15'
        'auto' inputs:
            auto_plots          Which plots: 'All', 'Final' or 'None'
            auto_save           bool; whether plots will be saved or not
        'manual' inputs:
            margins             'tight' or 'wide'
            interactive         bool, interactive plot or not
        """

        # Parse kwargs
        if mode == 'auto':
            margins, self.interactive = auto_parameter_parser(kwargs['auto_plots'],
                                                              kwargs['auto_save'])
        elif mode == 'manual':
            margins = kwargs['margins']
            self.interactive = kwargs['interactive']
        else:
            raise ValueError("mode must be 'auto' or 'manual'")

        # Set units and font size
        self.units = units
        title_kwargs = set_fonts(fonts)

        # Open figure, and make it the correct size
        self.figure = initialise_figure_window(1351, 0, 560, 869)

        # Create axes
        if margins == 'tight':
            # Share x axis between all figures
            gs = self.figure.add_gridspec(3, 2, width_ratios=[1, 0.02])

            ax_structure = self.figure.add_subplot(gs[0, 0])
            ax_distributions = {'moment': self.figure.add_subplot(gs[1, 0],
                                                                  sharex=ax_structure),
                                'axial': self.figure.add_subplot(gs[2, 0],
                                                                 sharex=ax_structure)}

            # Hide structure tick labels
            plt.setp(ax_structure.get_xticklabels(),
                     visible=False)

            # Create colourbar axis
            ax_cbar = self.figure.add_subplot(gs[0, 1])

        elif margins == 'wide':

            # Only share x axis between moment and force figures
            ax_structure = plt.subplot(311)
            ax_distributions = {'moment': plt.subplot(312)}
            ax_distributions['axial'] = plt.subplot(313,
                                                    sharex=ax_distributions['moment'])

            # Colourbar axis is part of ax_structure
            ax_cbar = None

        else:
            raise ValueError("Margins must be either 'tight' or 'wide' ")

        # Always hide moment tick labels
        plt.setp(ax_distributions['moment'].get_xticklabels(),
                 visible=False)

        # Ensure integer ticks
        ax_structure.xaxis.set_major_locator(MaxNLocator(integer=True))
        ax_structure.yaxis.set_major_locator(MaxNLocator(integer=True))
        for key in ax_distributions:
            ax_distributions[key].xaxis.set_major_locator(
                MaxNLocator(integer=True))

        if self.interactive:
            # Set to interactive mode
            plt.ion()
            plt.show()

        # Initialise child classes
        self.structure_plot = StructurePlot(self, margins, title_kwargs, ax_structure, ax_cbar,
                                            self.units)
        self.moment_force_plots = MomentForcePlots(self, ax_distributions, title_kwargs, self.units)

        # Draw step number in top-left if passed
        if step_no is not None:
            self.obj_step_no = self.figure.text(0, 0,
                                                'Step {}'.format(step_no))
        else:
            self.obj_step_no = None

        # Draw now if in interactive mode
        self.update_now()

        if 'auto_plots' in kwargs and kwargs['auto_plots'] == 'All':
            plt.pause(1)        # Extra pause the first time
            print('... done')
