"""Enumerate mode"""

import glob
import itertools
import multiprocessing as mp
import os
import pickle as pkl
import re
import sys
from time import time

import numpy as np
import pandas as pd

from ...basics import data_dir
from ...modelling.anamodel import calculate_internals
from ...structures import Cantilever


def optimisation_dir():
    """Optimisation directory"""

    return os.path.join(data_dir(), 'Optimisation')


def limit_dir(limits):
    """Limit pair directory"""

    return os.path.join(optimisation_dir(),
                        '{}m_{}a'.format(limits['moment'], limits['axial']))


def enumerate_options(no_elements):
    """Generates all possible partitions and analyses them, saving the data to
    a .csv file for later analysis
    """

    # Calculate and enumerate the combinations
    no_combinations = no_partions(no_elements)

    if sys.platform == 'win32' and no_combinations > 10000:
        print('Too many combinations: run on HPC instead')
        return

    # Enumerate these combinations
    start = time()
    partitions = generate_partitions(no_elements)
    print('    Enumeration finished in {:.2f} seconds'.format(time()-start))

    start = time()
    """
    # Solve linearly
    results = []
    for partition in partitions:
        results.append(analyse_structure(partition))
    """
    # Solve in parallel manner
    pool = mp.Pool(mp.cpu_count())
    results = pool.map(analyse_structure, [row for row in partitions])
    pool.close()

    print('    Solving finished in {:.2f} minutes'.format((time()-start)/60))

    # Write to csv file
    start = time()
    np.savetxt(os.path.join(data_dir(), 'Optimisation',
                            'All_combinations_{}_elements.csv'.format(no_elements)),
               results, fmt='%s', delimiter=',')
    print('    Saving finished in {:.2f} seconds'.format(time()-start))


def enumerate_options_large(no_elements, limits):
    """Generates all possible partitions for a large structure, saving them to
    a pickled file for later analysis
    """

    # Get stable portion
    stable_rows, l_next_row_max = get_stable_rows(limit_dir(limits),
                                                  no_elements)
    elements_left = no_elements - sum(stable_rows)
    print('Enumerating large configurations')
    print('The first {} rows are stable, so {} elements are pre-allocated'.format(len(stable_rows),
                                                                                  sum(stable_rows)))
    print('    The stable rows are of lengths: {}'.format(stable_rows))
    print('    The next row has at most {} elements'.format(l_next_row_max))

    # Print information
    print('Finding configurations of {} elements ({} unallocated)'.format(no_elements,
                                                                          elements_left))

    # Enumerate the remaining portion
    configurations = configurations_below_stable(elements_left, stable_rows, l_next_row_max)

    print('    Total of {} different configurations to test'.format(len(configurations)))

    # Export these configurations to a pickle
    filename = os.path.join(limit_dir(limits),
                            'configurations_{}elements.pkl'.format(no_elements))
    with open(filename, 'wb') as f:
        pkl.dump(configurations, f)
    print('Enumerated configurations saved to: {}'.format(filename.replace(data_dir(),
                                                                           'cantilever_data')))


def no_partions(n):
    """Approximates the number of partitions for a given number of elements

    Just an approximation using the first term of closed-form function"""
    p_n = int(1/(4*n*np.sqrt(3))*np.exp(np.pi * np.sqrt((2*n)/3)))

    print('There are approximately {} combinations'.format(p_n))

    return p_n


def generate_partitions(n):
    """ Calculates all unique paritions of a given number

    Based off code written by JoshuaWorthington:
    https://www.geeksforgeeks.org/generate-unique-partitions-of-an-integer/
    Accessed 19/10/2020
    """

    if n == 0:
        return [[0]]

    p = [0] * n     # An array to store a partition
    k = 0           # Index of last element in a partition
    p[k] = n        # Initialize first partition  as number itself

    partitions = []   # List to store the partitions

    # This loop first prints current partition, then generates next partition.The loop stops when
    # the current partition has all 1s
    while True:

        # print current partition
        partitions.append(p[:k+1])

        # Generate next partition

        # Find the rightmost non-one value in p[].
        # Also, update the rem_val so that we know how much value can be accommodated
        rem_val = 0
        while k >= 0 and p[k] == 1:
            rem_val += p[k]
            k -= 1

        # if k < 0, all the values are 1 so there are no more partitions
        if k < 0:
            return partitions

        # Decrease the p[k] found above and adjust the rem_val
        p[k] -= 1
        rem_val += 1

        # If rem_val is more, then the sorted order is violated. Divide rem_val in  different values
        # of size p[k] and copy these values at different positions after p[k]
        while rem_val > p[k]:
            p[k + 1] = p[k]
            rem_val = rem_val - p[k]
            k += 1

        # Copy rem_val to next position and increment position
        p[k + 1] = rem_val
        k += 1


def analyse_structure(elements_in_rows):
    """Analyses the structure defined by the number of elements in each row"""

    # Generate cantilever
    cantilever = Cantilever('from_rows', elements_in_rows=elements_in_rows)

    # Analyse structure
    [row_criticals, col_criticals,
     _, ss] = calculate_internals(cantilever)

    # Calculate maximums
    max_moment = max(max(row_criticals['moment']), max(col_criticals['moment']))
    max_axial = max(max(row_criticals['axial']), max(col_criticals['axial']))

    return [cantilever.as_str(), max_moment, max_axial]


def get_stable_rows(directory, max_elements=None):
    """Gets the stable portion common to the largest 5 optimum cantilevers in the directory
    (with fewer than a specified number of elements if required)"""

    # List all files in this directory
    filenames = sorted(glob.glob(os.path.join(directory, '*elements_*l.csv')),
                       key=lambda x: int(re.search(r'(\d+)elements', x).group(1)))

    # Remove filenames with more than specified number of elements if required
    if max_elements is not None:
        filenames_trimmed = []
        for filename in filenames:
            no_elements = int(re.search(r'(\d+)elements', filename).group(1))

            if no_elements < max_elements:
                filenames_trimmed.append(filename)
    else:
        filenames_trimmed = filenames.copy()

    # Only optimal structures
    filenames_optimal = []

    for filename in filenames_trimmed:
        length = int(re.search(r'(\d+)l.csv', filename).group(1))

        # Add to the optimal filenames if there's no structure of this length yet
        if length > len(filenames_optimal):
            filenames_optimal.append(filename)

    # Only take the last five
    filenames_optimal = filenames_optimal[-5:]

    # Convert each configuration into elements in columns
    elements_in_columns = np.zeros(length, dtype=int)
    for optimal_filename in filenames_optimal:
        elements_in_columns = np.vstack((elements_in_columns,
                                         read_elements_in_columns(optimal_filename, length)))

    identical_columns = np.all(elements_in_columns[1:] == elements_in_columns[1, :], axis=0)

    # Take the stable rows from the longest cantilever and add one (thus increasing length)
    stable_height = elements_in_columns[1, np.where(identical_columns)[0][0]]
    configuration = pd.read_csv(filenames_optimal[-1]).iloc[0]['Configuration']
    row_offset = Cantilever('from_rows', elements_in_rows=configuration).row_offset-1

    return ([int(r)+1 for r in configuration.split('/')[row_offset:stable_height+row_offset]],
            np.where(identical_columns)[0][0]+1)


def read_elements_in_columns(path, x_dimension):
    """Opens the csv file converts each configuration within into a list of elements
    in each column. Only the portion below and right of (0,0) is considered.
    These lists are collated into an array of a specified width"""

    # Read csv
    dataframe = pd.read_csv(path)

    collated_elements_in_columns = np.zeros((dataframe.shape[0], x_dimension),
                                            dtype=int)

    # Loop over rows in the .csv file
    for idx, data_row in dataframe.iterrows():
        # Convert binary map into elements per column
        cantilever = Cantilever('from_rows', elements_in_rows=data_row['Configuration'])
        binary_map = cantilever.map[cantilever.row_offset:,
                                    cantilever.col_offset:]
        elements_in_columns = binary_map.sum(axis=0)

        # Add to output array
        collated_elements_in_columns[idx, -len(elements_in_columns):] = elements_in_columns

    return collated_elements_in_columns


def configurations_below_stable(no_elements_below_stable, stable_rows, l_next_row_max):
    """Lists all the configurations of elements below row 0"""

    # Only one configuration if no more elements to place
    if no_elements_below_stable == 0:
        return [stable_rows]

    # If only one element, also only one configutarion
    if no_elements_below_stable == 1:
        # Manually define else type isn't string
        dataframe = pd.DataFrame(['1'])
    else:
        # Otherwise load
        try:
            dataframe = pd.read_csv(os.path.join(optimisation_dir(),
                                                 'All_combinations_{}_elements.csv'.format(
                no_elements_below_stable)),
                header=None)
        except FileNotFoundError:
            # Enumeration not already done, so we need to do it ourselves
            dataframe = pd.DataFrame(['/'.join(map(str, p))
                                      for p in generate_partitions(no_elements_below_stable)],
                                     columns=[0])

    configurations = []
    for idx, row in dataframe.iterrows():
        partial_configuration = row[0]
        elements_in_rows = [int(r) for r in partial_configuration.split('/')]

        if elements_in_rows[0] <= l_next_row_max:
            # Combine stable rows with elements in rows
            configurations.append(stable_rows+elements_in_rows)

    return configurations


def permutations_above(n_above):
    """Returns a list of ways to arrange the above elements"""

    partitions = generate_partitions(n_above)

    rows_above = []
    for partition in partitions:
        # Partitions with 1 element in any row guaranteed to be sub-optimal
        if 1 in partition:
            continue
        # Make correct format negative
        partition = [-i for i in reversed(partition)]

        for asymmetric_partiton in asymmetric_permutations(partition):
            rows_above.append(asymmetric_partiton)

    return rows_above


def asymmetric_permutations(partition):
    """Returns all the permutations of a partition if it contains rows with odd numbers of elements"""

    odd_rows = []
    odd_row_idx = []

    for idx, elements_in_row in enumerate(partition):
        if elements_in_row % 2 != 0:
            odd_rows.append(elements_in_row)
            odd_row_idx.append(idx)

    permutations = []
    # Loop over all combinations of alignments
    for alignments in itertools.product('lr', repeat=len(odd_rows)):
        odd_row_strs = iter([str(e)+alignments[idx]
                             for idx, e in enumerate(odd_rows)])

        permutation = partition.copy()
        for idx in odd_row_idx:
            permutation[idx] = next(odd_row_strs)

        permutations.append(permutation)

    return permutations
