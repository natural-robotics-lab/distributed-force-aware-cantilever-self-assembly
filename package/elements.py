"""Class for the array of elements"""
import random as rand

import numpy as np

from .cantilevers.parallel.element import \
    Element as ParallelCantileverElement


class Elements():
    """Class for the nested dictionary of Elements"""

    def __init__(self, structure, mode, figure=None, initialise_elements=True):

        self._elements = {}
        self.structure = structure
        self.mode = mode

        # Counters for iteration
        self.iter_mode = 'all'
        self.iter_row_no = 0
        self.iter_col_no = 0

        try:
            if structure.no_elements == 0:
                return
        except AttributeError:
            # Just passed a map to structure, not the whole object
            # Therefore copying for export
            return

        if initialise_elements:
            id_no = 0
            # Loop over rows in structure
            for row_no in range(structure.no_rows)-structure.row_offset+1:
                # Make a dictionary entry with this row number
                self._elements[row_no] = {}

                for col_no in range(structure.no_cols)-structure.col_offset+1:

                    if structure.element_at([row_no, col_no]) == 1:
                        # Add an element here
                        self._elements[row_no][col_no] = self.create_element(id_no,
                                                                             figure,
                                                                             [row_no, col_no])
                        id_no += 1

                        # print('Row {}, col {}'.format(row_no, col_no))
    def __call__(self, iter_mode='all'):
        """Allows to specify the iteration mode when making the iterator"""
        self.iter_mode = iter_mode

        return self

    def __getitem__(self, pos):
        """Gets the element at position [row, col]"""
        try:
            return self._elements[pos[0]][pos[1]]
        except KeyError as err:
            raise KeyError('No element at position {}'.format(pos)) from err

    def __iter__(self):
        """Start iteration at left end of top row

        Inputs:
            mode    iterate through 'all' elements or just 'edge' ones
        """
        if self.structure.no_elements == 0:
            # Can't iterate if no elements
            return self

        # Highest non-empty row
        self.iter_row_no = min([k for k in self._elements
                                if self._elements[k]])
        # Leftmost element in this row
        self.iter_col_no = min(self._elements[self.iter_row_no].keys())-1

        if self.iter_mode == 'top':
            # Only iterate through the top row
            self.iter_row_no = 0
            self.iter_col_no = -1

        return self

    def __next__(self):
        """Iterate through elements"""
        if self.structure.no_elements == 0:
            # Can't iterate if no elements
            raise StopIteration

        # Try next column
        self.iter_col_no += 1
        try:
            elements_in_row = max(self._elements[self.iter_row_no])
        except ValueError:
            # This row is completely empty
            elements_in_row = -1
        if self.iter_col_no <= elements_in_row:
            try:
                next_element = self[self.iter_row_no, self.iter_col_no]
            except KeyError:
                next_element = self.__next__()
        else:
            # Need to move to next row
            self.iter_row_no += 1

            # Stop iteration at the bottom of the structure
            if self.iter_row_no > max([k for k in self._elements
                                       if self._elements[k]]):
                raise StopIteration

            # Reset to start of the row
            col_reset = False
            while not col_reset:
                try:
                    # Get first value in this column
                    self.iter_col_no = min(
                        self._elements[self.iter_row_no].keys())-1
                    col_reset = True
                except ValueError:
                    # Nothing in this row, but there might be in the next one
                    self.iter_row_no += 1

            next_element = self.__next__()

        if self.iter_mode == 'all':
            # Iterating through all elements
            return next_element

        elif self.iter_mode == 'edge':
            # Just iterating through edge elements, so keep iterating if this isn't an edge element

            if self.iter_row_no < 0:
                # Assume elements on the top row are escaping (think harder for extended case)
                return self.__next__()

            if self.structure.element_at([self.iter_row_no,
                                          self.iter_col_no]) == 3:
                # This element has an active element passing over it, so can't release
                return self.__next__()

            # Edge elements have free space below and to left or right

            if self.structure.element_at([self.iter_row_no+1,
                                          self.iter_col_no]) in [1, 3]:
                # Element below
                return self.__next__()
            elif self.structure.element_at([self.iter_row_no, self.iter_col_no-1]) in [1, 3, -1] \
                    and self.structure.element_at([self.iter_row_no, self.iter_col_no+1]) in [1, 3, -1]:
                # Elements or support to left and right
                return self.__next__()

            else:
                # No elements left or right
                return next_element
        elif self.iter_mode == 'top':
            # Just iterating through top row
            if self.iter_row_no > 0:
                raise StopIteration
            else:
                return next_element

        else:
            raise ValueError("mode must be 'all', 'edge', or 'top'")

    def create_element(self, id_no, figure, location):
        """Returns the correct version of the Element depending on the algorithm"""

        if self.mode == 'parallel_cantilever':
            return ParallelCantileverElement(id_no,
                                             figure,
                                             location)
        else:
            raise NotImplementedError('No specified element')

    def export(self):
        """Returns a new Elements object for exporting. Contains copies of the
        Element objects and a just the structure map, not whole structure"""

        # Initialise with just the structure
        elements_copy = Elements(self.structure.copy(), self.mode)

        # Create the dictionary in the copy
        for row_no in range(-1, self.structure.no_rows):
            # Make a dictionary entry with this row number
            elements_copy._elements[row_no] = {}

            for col_no in range(self.structure.no_cols):

                if self.structure.element_at([row_no, col_no]) != 0:
                    # Copy element here
                    elements_copy._elements[row_no][col_no] = self._elements[row_no][col_no].copy(
                        minimal=False)

        # Copy active elements too
        active_elements_copy = [e.copy(minimal=False)
                                for e in self.structure.active_elements]

        return elements_copy, active_elements_copy

    def update_sensors(self, complete_internals, mode='all'):
        """Updates the sensors in the specfied elements"""

        # Add extra zeros to the end of the link_internals arrays
        # in a copy of the array
        complete_internals_extended = complete_internals.copy()

        for internal_type in complete_internals:

            # Column on right
            complete_internals_extended[internal_type] = np.append(
                complete_internals[internal_type],
                np.zeros((len(complete_internals[internal_type]), 1)),
                axis=1)

            # Row on bottom
            complete_internals_extended[internal_type] = np.vstack((
                complete_internals_extended[internal_type],
                np.zeros(complete_internals_extended[internal_type].shape[1])))

        # Correctly set the iteration mode
        self.iter_mode = mode

        # Iterate through the specified elements
        [e.update_sensors(complete_internals, self.structure) for e in self]

    def insert(self, element):
        """Adds a the provided element to this location"""

        row_no = element.location[0]
        col_no = element.location[1]

        # Add this row if it's empty
        if row_no not in self._elements:
            self._elements[row_no] = {}

        # Add element in this column
        self._elements[row_no][col_no] = element

        return self._elements[row_no][col_no]

    def move_element(self, element, initial_location):
        """Moves the element to its actual location, and deletes the old reference"""
        # Don't do anything if the element hasn't moved
        if element.location == initial_location:
            return

        # Create element in new location
        self.insert(element)

        # Remove reference in old location
        del self._elements[initial_location[0]][initial_location[1]]

    def remove_element(self, location):
        """Removes the element at the specified location"""

        del self._elements[location[0]][location[1]]

    def plot_all(self, figure=None):
        """Plots the current cantilever (on the given figure if specified)"""

        for element in self:

            # Set figure if specified
            if figure is not None:
                element.figure = figure

            # Plot element
            element.plot()


class ActiveElements():
    """List of the active elements"""

    def __init__(self, elements_list=None):
        if elements_list is None:
            self._list = []
        else:
            self._list = elements_list

        self.iter_no = 0

    def __repr__(self):
        """Prints the element IDs as a list"""

        element_ids = [(e if isinstance(e, str) else e.id) for e in self._list]

        return str(element_ids)

    def __iter__(self):
        """Start iteration at start of the list"""
        self.iter_no = 0

        return self

    def __next__(self):
        """Returns the next element in the list"""

        if self.iter_no < len(self._list):
            # One more element to iterate through
            next_element = self._list[self.iter_no]
            self.iter_no += 1
            return next_element

        # End of the list
        raise StopIteration

    def __getitem__(self, loc):
        """Returns the element at this location or index"""

        if isinstance(loc, list):
            # loc is a location
            error_str = 'location'

            for element in self._list:
                # Check element is actually an element, not 'init'
                if not isinstance(element, str) and element.location == loc:
                    return element
        else:
            # loc is an index
            error_str = 'index'
            if loc < len(self._list):
                return self._list[loc]

        # No elements here
        raise ValueError('No element at {} {}'.format(error_str, loc))

    def __len__(self):
        """Length of this object"""
        return len(self._list)

    def get_id(self, element_id):
        """Gets the element with the specified id"""

        return [e for e in self._list if e.id == element_id][0]

    def append(self, elements):
        """Appends the new elements to the list"""

        if isinstance(elements, list):
            # List of elements to add
            self._list += elements
        else:
            # Just one element to add
            self._list.append(elements)

    def insert(self, idx, element):
        """Inserts the element into the list at the specified index"""

        self._list.insert(idx, element)

    def pop(self, index):
        """Removes the element at the specified index"""
        self._list.pop(index)

    def remove_placed_elements(self, element_delay=None):
        """Removes any elements that are placed or escaped from the active_elements array"""

        # If any elements have escaped, stop adding any new ones
        if element_delay is not None and element_delay != np.inf \
            and [e for e in self._list
                 if (not isinstance(e, str) and e.mode == 'escaped')]:
            print('First element escaped: not adding any more')
            element_delay = np.inf

        # Remove elements from list
        self._list = [e for e in self._list if (not isinstance(e, str) and
                                                e.mode not in ['escaped', 'placed'])]

        return element_delay

    def shuffle(self):
        """Shuffles the order of the list"""

        rand.shuffle(self._list)

    def reorder(self, ids):
        """Reorders the list so that elements are in the order defined by ids"""

        reordered = []
        for element_id in ids:
            if isinstance(element_id, int):
                element = [e for e in self._list if e.id == element_id][0]
            elif isinstance(element_id, str):
                element = 'init'
            else:
                raise ValueError('Invalid id: {}'.format(element_id))

            reordered.append(element)

        self._list = reordered
