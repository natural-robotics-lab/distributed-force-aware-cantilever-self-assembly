"""Optimums mode"""

import glob
import os
from time import time

import numpy as np
import pandas as pd
from tqdm import tqdm

from ...basics import calculate_criticalness
from ...basics import data_dir as storage_dir
from ...basics import make_or_clean_dir
from ...structures import Cantilever


def optimum_structures(max_elements, limits):
    """ Analyses the pre-calculated data and finds the longest structure for given limits and
    numbers of elements"""

    # Check the .csv files exist up to and including the maximum number of elements
    data_dir = os.path.join(storage_dir(), 'Optimisation')
    check_for_csvs(data_dir, max_elements)

    # Prepare output directory
    output_dir = os.path.join(data_dir, '{}m_{}a'.format(limits['moment'],
                                                         limits['axial']))
    make_or_clean_dir(output_dir)

    # Loop over the allowable number of elements
    start = time()
    for no_elements in range(1, max_elements+1):
        print('Processing {} elements...'.format(no_elements))

        results = iterate_over_csv(data_dir, no_elements, limits)

        # Save the longest configuration
        save_longest_structures(results, output_dir, no_elements)
        print('')

    print('Exporting configurations finished in {:.2f} seconds'.format(time()-start))


def check_for_csvs(directory, max_elements):
    """Checks there are .csv files in the given directory with data up to and including the given
    number of elements"""

    # Get list of files
    csv_files = glob.glob(directory+'/All_combinations_*_elements.csv')

    # Check the necessary .csv files are there
    for no_elements in range(1, max_elements+1):
        filename = 'All_combinations_{}_elements.csv'.format(no_elements)

        filename = os.path.join(directory, filename)

        # Raise error if we're missing a file
        if filename not in csv_files:
            raise Exception('Missing file: ' + filename)


def iterate_over_csv(data_dir, no_elements, limits):
    """Loads and iterates over the csv files from the enumeration stage"""

    # Load data as pd dataframe
    filename = data_dir+'/All_combinations_{}_elements.csv'.format(no_elements)

    data = pd.read_csv(filename, delimiter=',',
                       names=['Configuration', 'Maximum moment', 'Maximum axial force'])

    # Progress bar
    pbar = tqdm(total=len(data))

    results_list = []
    for _, row in data.iterrows():
        results_list.append(check_row_stable(row, limits))
        pbar.update(1)

    # Close progress bar
    pbar.close()

    return results_list


def check_row_stable(row, limits):
    """Checks if the current row is stable.

    Inputs:
        row     the row of the csv file as a Pandas dataframe
        limits  limits dictionary
    Output:
        list    [length, max_criticalness, configuration] (all zero when not stable)
    """

    if row['Maximum moment'] < limits['moment'] and row['Maximum axial force'] < limits['axial']:
        criticalness = max(calculate_criticalness(row['Maximum moment'], limits['moment']),
                           calculate_criticalness(row['Maximum axial force'], limits['axial']))

        configuration_str = configuration_string(row['Configuration'])
        return [cantilever_length(configuration_str), criticalness, configuration_str]
    else:
        # Return zeros if not stable
        return [0, 0, '0']


def configuration_string(configuration):
    """ Converts the configuration into a string if it's not already"""
    if type(configuration) == str:
        return configuration
    else:
        return str(int(configuration))


def cantilever_length(configuration):
    """Returns the length of a cantilever defined as a string"""

    return Cantilever('from_rows', elements_in_rows=configuration).length


def save_longest_structures(results, output_dir, no_elements):
    """Saves the longest structures to a given directory"""

    # Convert list to array and find maximum length
    results_df = pd.DataFrame(results, columns=['Length', 'Criticalness', 'Configuration'])
    max_length = np.max(results_df['Length'])

    # Extract configurations where we reach this length
    longest_configurations = results_df[results_df['Length'] == max_length]

    # Save to a .csv file
    longest_configurations[['Criticalness', 'Configuration']].to_csv(
        os.path.join(output_dir, '{}elements_{}l.csv'.format(no_elements, max_length)),
        index=False)
