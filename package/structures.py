""" Classes for the structures that can be constructed from our elements """
import bz2
import os
import pickle as pkl

import numpy as np
import pandas as pd
from scipy.ndimage import label

from .cantilevers.parallel.element import \
    Element as ParallelCantileverElement
from .elements import Elements, ActiveElements


class Structure():
    """High-level structure class

    Fields:
        map             the binary map
        types           the Types class
        no_elements     the number of elements
        length          the length of the structure
    Init:
        mode            'empty', 'load', 'from_map'
        size            the size of the empty matrix to create
        binary_map      the binary structure map
        path            path to the existing structure txt file
    """

    def __init__(self, mode='empty', size=0, binary_map=None, active_elements=None):
        """Initialises the object"""

        # Initialise map and types objects
        if mode == 'empty':
            # Initialise empty structure of given size
            self.map = np.zeros((size, size)).astype(int)
        elif mode == 'from_map':
            # Converts from a binary map
            self.map = binary_map
        else:
            raise ValueError('Invalid mode passed to Structure')

        # Set active elements list
        self.active_elements = active_elements

        # Set other fields
        try:
            self.count_elements()
            self.calculate_length()
            self.calculate_offset()
            self.calculate_dimensions()
            self.calculate_size()

        except IndexError:
            # Structure is poorly defined, so we can't set these things properly
            self.no_elements = 0
            self.length = 0
            self.no_rows = 0
            self.no_cols = 0
            self.no_rows_complete = 0
            self.no_cols_complete = 0
            self.row_offset = 0
            self.col_offset = 0

    def __repr__(self):
        """Prints the head of the structure map, and the string representation"""

        final_str = '{} with string representation:\n'.format(self.configuration())
        final_str += '  {}\n'.format(self.as_str())
        final_str += 'And top left of map:\n'
        for row in self.map[:min(10, self.no_rows_complete)]:
            rowstr = ''
            for col in row[:min(20, self.no_cols_complete)]:
                rowstr += '{:2} '.format(col)
            final_str += rowstr + '\n'

        return final_str

    def copy(self):
        """Returns a copy of this structure"""

        return Cantilever(mode='from_map', binary_map=np.copy(self.map))

    def configuration(self):
        """Returns the configuration of the structure (currently just 'Cantilever')"""
        if isinstance(self, Cantilever):
            return 'Cantilever'

        # If not one of these, something is wrong
        raise TypeError('Structure must be a Cantilever')

    def export_list(self, directory, index, trial_no):
        """Exports all the configurations of a construction sequence in a single text file"""

        # Index is step number if specified, otherwise set to number of elements
        if index is None:
            index = self.no_elements

        # Make text file if it doesn't exist
        with open(os.path.join(directory, 'Trial_{}.csv'.format(trial_no)), "a") as output_file:
            output_file.write('{},{}\n'.format(index, self.as_str()))

        return

    def element_at(self, position, force_releasing_no=1):
        """ Gets the element at position = [row, col], taking into account offsets

        force_releasing_no is the number to represent elements that are force_releasing as"""

        offset_position = [position[0] + self.row_offset,
                           position[1] + self.col_offset]

        # If either are negative, return zeros instead of wrapping around
        if offset_position[0] < 0 or offset_position[1] < 0:
            return 0

        try:
            next_element = self.map[offset_position[0], offset_position[1]]

            try:
                # Count elements force_releasing as active
                if next_element == 1 and \
                        'force_releasing' in self.elements[position].mode:
                    next_element = force_releasing_no
            except (AttributeError, TypeError):
                # Not specified an elements object
                pass

            return next_element
        except IndexError:
            # Out of bounds; return empty
            return 0

    def count_elements(self):
        """Counts the number of elements in the structure"""

        self.no_elements = np.count_nonzero(self.map == 1)
        self.no_elements += np.count_nonzero(self.map == 2)
        self.no_elements += 2*np.count_nonzero(self.map == 3)

        return self.no_elements

    def calculate_dimensions(self):
        """ Calculates the dimensions of a structure with a non-regular
        shape

        Outputs: no_rows, no_cols
            no_rows   the number of rows in the structure shape
            no_cols   the number of columns in the structure shape
        """
        # Create copy to work on
        map_local = np.copy(self.map)

        # Replace -1 with 0
        map_local[map_local == -1] = 0

        # Replace 2 and 3 with 1 (include active elements)
        map_local[map_local == 2] = 1
        map_local[map_local == 3] = 1

        # Flatten to one column
        col_flatten = map_local.sum(axis=1)
        self.no_rows = np.count_nonzero(col_flatten)

        # Flatten to one row
        row_flatten = map_local.sum(axis=0)
        self.no_cols = np.count_nonzero(row_flatten)

        return self.no_rows, self.no_cols

    def calculate_size(self):
        """Calculates the size of the structure map, including empty and
        boundary empty cells

        Outputs:
            no_rows_complete, no_cols_complete
            no_rows_complete   the total number of rows in the map
            no_cols_complete   the total number of columns in the map
        """

        self.no_rows_complete, self.no_cols_complete = self.map.shape

        return self.no_rows_complete, self.no_cols_complete

    def calculate_offset(self):
        """Calculates the row and column offset of the structure map
        due to the boundary.

        Outputs:
            row_offset  row number of "root" element
            col_offset  column number of "root" element
        """
        if -1 not in self.map:
            # Default case with no boundary
            self.col_offset = 0

            try:
                # Might be a row offset if the boundary isn't defined yet
                self.row_offset = np.nonzero(self.map[:, 0])[0][0]
            except IndexError:
                # No elements in structure
                self.row_offset = 0
        else:
            # Need to account for boundary too
            boundaries = label(self.map == -1)[0]
            left_boundary = boundaries == 1

            # Matrix offsets
            self.row_offset = np.sum(np.invert(left_boundary[:, 0]))
            self.col_offset = np.sum(left_boundary[self.row_offset, :])

        return self.row_offset, self.col_offset

    def move_parallel_element(self, element, initial_location):
        """Updates the structure map as the parallel active elements move around it"""

        # Set old position to 0
        self.map[self.row_offset+initial_location[0],
                 self.col_offset+initial_location[1]] = 0

        # Set new position to 2
        self.map[self.row_offset+element.location[0],
                 self.col_offset+element.location[1]] = 2

        if np.any(self.map[0, :] != 0):
            self.map = np.vstack([np.zeros(self.no_cols_complete).astype(int),
                                  self.map])

        # Update structure fields
        self.count_elements()
        self.calculate_length()
        self.calculate_dimensions()
        self.calculate_size()
        self.calculate_offset()
        self.count_attachments()

        # Update cantilever fields
        if isinstance(self, Cantilever):
            self.calculate_first_filled_col_index()
            self.calculate_first_column()
        else:
            raise NotImplementedError('No structure of specified type')

        # Update structure elements object
        self.elements.move_element(element, initial_location)


class Cantilever(Structure):
    """Set the binary cantilever map as a special class with associated
    methods and functions

    Fields:
        no_rows                 the number of rows in the cantilever shape
        no_cols                 the number of columns in the cantilever shape
        no_rows_complete        the total number of rows in the map
        no_cols_complete        the total number of columns in the map
        row_offset              row number of "root" element
        col_offset              colum number of "root" element
        first_filled_col_index  index of the first filled column with elements
        col0                    x coord on the left of the first column
        attachments             no. attachment points
    """

    def __init__(self, mode='empty', size=0, path=None, elements_loaded=None, step_no=0,
                 binary_map=None, active_elements_list=[], elements=False, elements_in_rows=None):
        """Initialises the object"""

        # Set map here if loading from file
        if mode == 'load_csv':
            # Try to load Trial_n.csv
            try:
                if 'Results' in path:
                    # Loading a multiple trials file
                    dataframe = pd.read_csv(path)
                else:
                    # Loading a standard trial file
                    dataframe = pd.read_csv(path, names=['No. elements', 'Configuration'])

                # Step number is actually length or number of elements
                binary_map = cantilever_map_from_rows(dataframe.loc[step_no-1, 'Configuration'])
            # Reraise errors with more relevant messages
            except FileNotFoundError as err:
                raise FileNotFoundError('No file at: ' + path) from err
            except KeyError as err:
                raise KeyError('File has no structure with step {}'.format(
                    step_no)) from err

            mode = 'from_map'
        elif mode == 'load_pbz2':
            # Specified path, so open file (also requires step number)
            data = bz2.BZ2File(path, 'rb')
            elements_list = pkl.load(data)

            # Choose step number
            try:
                elements_loaded = elements_list[step_no][0]
                active_elements_list = elements_list[step_no][1]
            except IndexError as err:
                raise IndexError('Structure only has {} steps'.format(
                    len(elements_list))) from err

            # Load map object
            binary_map = elements_loaded.structure.map
            mode = 'from_map'

            # Initialise Elements object
            elements = True
        elif mode == 'from_rows':
            # Set the map here if loading from rows
            binary_map = cantilever_map_from_rows(elements_in_rows)
            mode = 'from_map'
        elif mode not in ['empty', 'from_map']:
            raise ValueError('Invalid mode: ' + mode)

        # Set whether to initialise active elements
        if elements:
            active_elements = ActiveElements(active_elements_list)
        else:
            active_elements = None

        # Parent initialisation
        super().__init__(mode, size, binary_map, active_elements=active_elements)

        if self.no_elements > 0:
            # Child initialisation
            self.calculate_first_filled_col_index()
            self.calculate_first_column()
            self.count_attachments()

        else:
            # Structure is poorly defined, so we can't set these things properly
            self.first_filled_col_index = 0
            self.col0 = 0
            self.attachments = 0
            self.elements = None

        # Add elements if defined
        try:
            elements_loaded.cantilever = self
            self.elements = elements_loaded
            return
        except AttributeError:
            pass

        if elements:
            # Also create a dictionary of element objects
            self.elements = Elements(self, 'parallel_cantilever')
        else:
            self.elements = None

    def calculate_length(self):
        """Calculates the length of a given structure, including ones extending left
        and up from the origin, or those with the longest part not in row 0"""

        if -1 in self.map:
            # Cantilever has a defined boundary

            boundary_length = np.count_nonzero(np.sum(self.map == -1, axis=0))

            col_flatten = self.map.sum(axis=0)[boundary_length:]

        else:
            # Default behaviour: boundary is a vertical line left of structure
            col_flatten = self.map.sum(axis=0)

        self.length = np.count_nonzero(col_flatten)

        return self.length

    def insert_new_element(self, mode, figure, location=None, element_type=2, sequential=False):
        """Creates a new element then inserts it into the correct location in the
        cantilever object

        sequential is a flag to say whether this is actually simulating the sequential case,
        if so we should track what columns are valid"""

        if mode == 'parallel':
            new_element = ParallelCantileverElement(self.no_elements,
                                                    figure,
                                                    location,
                                                    sequential=sequential)
        else:
            raise NotImplementedError

        # Add a 2 to the cantilever map if not specified
        self.insert_element(new_element, element_type=element_type)

        # Return the new_element object
        return new_element

    def insert_element(self, new_element, element_type=1):
        """Adds the provided new element to the cantilever map"""

        # Add the element to the map
        insertion_location = [new_element.location[0]+self.row_offset,
                              new_element.location[1]+self.col_offset]
        self.map[insertion_location[0], insertion_location[1]] = element_type

        # Ensure there's a row of zeros above the cantilever to expand into
        if 1 in self.map[0, :]:
            self.map = np.vstack([np.zeros(self.no_cols_complete).astype(int),
                                  self.map])

            # Update dimensions
            self.calculate_size()
            self.calculate_offset()

        # Ensure there's a defined boundary column with space to expand into
        if 1 in self.map[:, 0]:
            # Prepare column to insert: 0s to row_offet, then -1
            col_insert = -1 * np.ones((self.no_rows_complete, 1)).astype(int)
            col_insert[: self.row_offset, :] = np.zeros((self.row_offset, 1))

            # Insert the column
            self.map = np.hstack([col_insert, self.map])

            # Update dimensions
            self.calculate_size()
            self.calculate_offset()

        # Add to elements array if defined
        if self.elements is not None:
            self.elements.insert(new_element)

        # Update other fields
        self.count_elements()
        self.calculate_length()
        self.calculate_dimensions()
        self.calculate_size()
        self.calculate_offset()
        self.calculate_first_filled_col_index()
        self.calculate_first_column()
        self.count_attachments()

    def get_row(self, row_no):
        """Returns the specified row, given the offset"""
        offset_row_no = row_no + self.row_offset

        return self.map[offset_row_no, :]

    def calculate_first_column(self):
        """Calculates the x coord on the left of the first column"""

        # Update internal variables
        self.calculate_offset()
        self.calculate_first_filled_col_index()

        # Calculate col0
        self.col0 = self.first_filled_col_index - self.col_offset

        return self.col0

    def calculate_first_filled_col_index(self):
        """Finds the index of the first filled column in the cantilever map"""

        # Remove any boundary elements
        map_binary = np.copy(self.map)
        map_binary[map_binary == -1] = 0
        map_binary[map_binary == 2] = 1

        # Sum down columns
        sums = map_binary.sum(axis=0)

        try:
            self.first_filled_col_index = np.nonzero(sums)[0][0]
        except IndexError:
            # No first filled column
            self.first_filled_col_index = 0

        return self.first_filled_col_index

    def count_attachments(self):
        """Calculates the number of attachment points of the cantilever"""

        # Update row and column offsets
        self.calculate_offset()

        # Create copy of the map with active elements as 1 not 2
        map_local = self.map.copy()
        map_local[map_local == 2] = 1

        # Count number of elements attached to the top of the boundary
        side_attachments = 0
        top_attachments = 0

        if self.row_offset > 0:
            top_attachments = map_local[self.row_offset-1,
                                        0:self.col_offset].sum()
        if self.col_offset > 0:
            side_attachments = map_local[self.row_offset:,
                                         self.col_offset].sum()

        if self.col_offset == 0:
            # No boundary
            side_attachments = map_local[:, 0].sum()

        self.attachments = int(top_attachments + side_attachments)

        return self.attachments

    def validate(self):
        """Trims the cantilever map and checks it's valid.

        Inputs:
            cantilever      the binary cantilever map
        Outputs:
            validity            One of: 'empty' (there's no cantilever)
                                        'boundary' (boundary improperly defined)
                                        'attachment' (not connected to support)
                                        True (valid cantilever)
            total_attachments   the number of attachments to the boundary
        """
        # Check there are elements in the cantilever
        if 1 not in self.map:
            return 'empty', 0

        # Check boundary is flat and filled
        boundary_coords = np.argwhere(self.map == -1)
        if boundary_coords.size != 0:
            # Calculate row and column offset
            row_offset = min(boundary_coords[:, 0])
            col_offset = max(boundary_coords[:, 1]+1)

            # Check the boundary is a rectgangular
            boundary_ideal = np.zeros((self.no_rows_complete,
                                       self.no_cols_complete))
            for i in range(row_offset, self.no_rows_complete):
                boundary_ideal[i, 0:col_offset] = -1*np.ones(col_offset)

            boundary_ideal_coords = np.argwhere(boundary_ideal == -1)

            if not np.array_equal(boundary_coords, boundary_ideal_coords):
                return('boundary'), 0

        # Check cantilever is connected to the boundary
        if self.attachments == 0:
            return 'attachment', 0

        # Check continuity of cantilever
        continuity = check_continuity(self.map)
        if not continuity:
            return 'continuity', 0

        return True, self.attachments

    def get_highest_element(self):
        """Returns the location of the leftmost element of the top row of the cantilever"""

        # Get row location
        row_id = -1
        top_row = None
        while True:
            row_id += 1
            test_row = self.map[row_id, :]
            if 1 in test_row:
                top_row = test_row
                break

        # Get col id
        col_id = np.argwhere(top_row == 1)[0][0]

        # Offset these ids
        element_location = [row_id-self.row_offset, col_id - self.col_offset]

        return element_location

    def as_str(self):
        """Converts the cantilever map into a string of row numbers, separated by
        slashes"""

        # Remove offset
        map_condensed = self.map[self.row_offset:, self.col_offset:]

        # Create list
        elements_in_rows = np.sum(map_condensed == 1, axis=1)
        elements_in_rows_list = list(elements_in_rows[elements_in_rows != 0])

        # Convert to string
        configuration_string = '/'.join(map(str, elements_in_rows_list))

        return configuration_string


def check_continuity(cantilever_map):
    """Checks all the elements in the cantilever are attached to each other"""

    # Calculate dimensions and find top left element
    M, N = cantilever_map.shape     # Dimensions of array, not cantilever
    all_element_coords = np.where(cantilever_map == 1)
    start = [all_element_coords[0][0], all_element_coords[1][0]]

    remaining_cantilever_map = np.copy(cantilever_map)  # Working copy
    mark_neighbours(remaining_cantilever_map, start, M, N)

    # Check if the cantilever has all been reached
    if 1 in remaining_cantilever_map:
        return False
    else:
        return True


def mark_neighbours(cantilever_map, position, M, N):
    """Recursive function that finds an element in the cantilver map"""
    # Set self to 2
    cantilever_map[position[0], position[1]] = 2

    # Get adjacent coordinates
    adjacent_coordinates = [[position[0]-1, position[1]],  # above
                            [position[0]+1, position[1]],  # below
                            [position[0], position[1]-1],  # left
                            [position[0], position[1]+1]]  # right

    # Remove coordinates from list if they're negative or not cantilever there
    for coordinate in adjacent_coordinates:
        if coordinate[0] >= 0 and coordinate[0] < M and coordinate[1] >= 0 and coordinate[1] < N:
            # Coordinate is valid
            if cantilever_map[coordinate[0]][coordinate[1]] == 1:
                # Finally set to 2
                # print('Setting element in position ' + str(coordinate))
                cantilever_map[coordinate[0]][coordinate[1]] = 2

                # There's an element here
                mark_neighbours(cantilever_map, coordinate, M, N)


def cantilever_map_from_rows(elements_in_rows):
    """Constructs the cantilever object from the number of elements in each row"""

    # Convert to list if not already
    if isinstance(elements_in_rows, str):
        elements_in_rows_list = [int(x) for x in elements_in_rows.split('/')]
    else:
        elements_in_rows_list = elements_in_rows

    # Extract shape
    no_rows = len(elements_in_rows_list)
    no_cols = int(elements_in_rows_list[0])

    # Generate matrix
    binary_map = np.zeros((no_rows, no_cols+1), dtype=int)

    for row_id, elements_in_row in enumerate(elements_in_rows_list):
        binary_map[row_id, 1:elements_in_row+1] = 1
        binary_map[row_id, 0] = -1

    # Add row of 0 to the start
    binary_map = np.insert(binary_map, 0, np.zeros(
        no_cols+1, dtype=int), axis=0)

    return binary_map
