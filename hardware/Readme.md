# Hardware

This directory contains files relating to the hardware. Each agent has two parts: `main_body.stl` and `separate_cross.stl`. In addition to these 3D printed parts, the following components were used (prices are correct at the time of ordering):

| Item                                       | Onecall order code                                           | Item cost (£) | Quantity required (single complete agent) | Total cost (£) |
| ------------------------------------------ | ------------------------------------------------------------ | ------------- | ----------------------------------------- | -------------- |
| Force sensitive resistors (Ohmite FSR05BE) | [3216600](https://onecall.farnell.com/ohmite/fsr05be/force-sensing-resistor-02ah9087/dp/3216600?ost=3216600) | 4.35          | 8                                         | 34.80          |
| Neodynium magnets, 8 mm x 3 mm             | [1888095](https://onecall.farnell.com/duratool/d01766/magnets-rare-earth-8-x-3mm-pk10/dp/1888095?ost=1888095) | 0.26          | 8                                         | 2.08           |
| Washers, M5 x 30 mm                        | [FN00763](https://onecall.farnell.com/unbranded/m5-id-x-30mm-od-m/m5-id-x-30mm-od-mudguard-washers/dp/FN00763?ost=fn00763) | 0.06          | 40                                        | 2.40           |
| LED, 3mm, red                              | [1581113](https://onecall.farnell.com/multicomp/mcl034md/led-3mm-he-red-9-8mcd-625nm/dp/1581113?st=1581113) | 0.08          | 4                                         | 0.32           |

Each support component requires two additional magnets, and pairs of support components are attached with an M3 nut and bolt.


## Assembly procedure

1. 3D print all parts.
2. Insert magnets into the required connection holes. Araldite Rapid 2-component epoxy was used to bond the magnets to the agent body.
3. Solder flying leads to the force sensors and LEDs.
4. Attach force sensors to each required magnet. The FSRs have an adhesive backing, which was complemented with double-sided tape was used for this purpose. It's also useful to wrap some electrical tape around the whole agent body near the electrical contacts to increase durability.
5. Each FSR on a female magnet should have a paper spacer on the front of it. We used a holepunch and sticky-back paper labels to create small self-adhesive spacers for this purpose.
6. Attach LEDs to the middle of each required face. Electrical tape was used for this.
7. Add enough steel washers onto the pegs of the agent body to achieve the required mass (280 g was used in the paper)).
8. Attach the separate cross to the rear of the agent body and use electrical to secure it in place.

## Completed modules

| Agent                      | Support                        |
| -------------------------- | ------------------------------ |
| ![Agent module](agent.jpg) | ![Support module](support.jpg) |

