"""The data gathered while traversing"""
import copy

import numpy as np

from ...basics import calculate_criticalness


class TraverseData():
    """ Class for the data the active element collects while traversing the structure

    Fields:
        data        the main data dictionary, containing lists of the criticalness:
                        {'row': {'moment': [],
                                 'axial': []},
                         'col': {'moment': [],
                                 'axial': []}}
        valid_cols  list of the valid columns so the active element can go straight to the correct
                    location (for comparing to parallel)
    """

    def __init__(self, local=False):
        self.data = {'row': {'moment': [],
                             'axial': []},
                     'col': {'moment': [],
                             'axial': []}}

        # If local data, create separate lists for the top and bottom of the cantilever
        if local:
            for link_type in self.data:
                for internal_type in self.data[link_type]:
                    self.data[link_type][internal_type] = {'top': [],
                                                           'bottom': []}

        self.array_length = 0

        # Variable to record which columns are valid or not; set later
        self.valid_cols = None

        # Default hyperparameters
        self.index_factor = 2
        self.width_factor = 1

    def copy(self):
        """Returns a copy of this TraverseData object"""

        return copy.deepcopy(self)

    def append_criticalness_message_passing(self, top_row, limits):
        """ Appends data to the criticalness list when using the message-passing variant

        Inputs:
            top_row     the top_row object of the to element
            limits      the allowable limits
        """

        for link_type in self.data:
            for internal_type in self.data[link_type]:

                # Append criticalness
                self.data[link_type][internal_type].append(calculate_criticalness(
                    top_row.max_internals_in_col[link_type][internal_type],
                    limits[internal_type]))

    def append_criticalness_local(self, edge_element, limits, location):
        """ Appends data to the criticalness list when using the local variant

        Inputs:
            top_row     the top_row object of the to element
            limits      the allowable limits
            location    the location of the active element
        """

        # Define surface of cantilever the active element is on
        if location[0] < 0:
            surface = 'top'
        elif location[0] > 0:
            surface = 'bottom'

            # Initialise below list if not done already
            if not self.data['row']['moment']['bottom']:
                array_length = len(self.data['row']['moment']['top'])
                for link_type in self.data:
                    for internal_type in self.data[link_type]:
                        self.data[link_type][internal_type]['bottom'] = [0] * array_length
        else:
            raise ValueError("Can't append to traverse data when in row 0")

        # Might need to extend the arrays
        new_cols = location[1] + 1 - len(self.data['row']['moment'][surface])
        if new_cols > 0:
            # Add to all arrays
            for link_type in self.data:
                for internal_type in self.data[link_type]:
                    for surf in self.data[link_type][internal_type]:
                        self.data[link_type][internal_type][surf] += [0]*new_cols

        # Define side to read the row links off
        side = 0

        # Update row fields (take link on correct side)
        for internal_type in self.data['row']:
            if internal_type == 'moment':
                # Take moment regardless of sign
                critical_row_link = abs(
                    edge_element.row_internals[internal_type][side])
            else:
                # Only take axial if it's positive
                critical_row_link = max(0,
                                        edge_element.row_internals[internal_type][side])

            # Calculate relevant criticalness
            criticalness = calculate_criticalness(critical_row_link,
                                                  limits[internal_type])
            # Put in the correct location in the list
            self.data['row'][internal_type][surface][location[1]] = criticalness

        # Set col links (select most critical link)
        for internal_type in self.data['col']:
            if internal_type == 'moment':
                # Take maximum magnitude of moment
                critical_col_link = max(np.abs(edge_element.col_internals[internal_type]))
            else:
                # Take maximum of axial (-ve is infinitely strong)
                critical_col_link = max(edge_element.col_internals[internal_type])

            # Calculate relevant criticalness
            criticalness = calculate_criticalness(critical_col_link,
                                                  limits[internal_type])

            # Put in the correct location in the list
            self.data['col'][internal_type][surface][location[1]] = criticalness

    def column_valid(self, location, cantilever):
        """Record if this column is valid to place in or not (if required)"""

        # Don't do anything if not actually recording this information
        if self.valid_cols is None:
            return

        # Only works below the cantilever and for non-extended structures
        if location[0] < 0:
            return

        # Tip is always valid
        if location[0] == 0:
            self.valid_cols.append(location[1])
            return

        # Location is valid if element above and left is 1 or -1
        if cantilever.element_at([location[0]-1, location[1]]) in [1, -1] \
                and cantilever.element_at([location[0], location[1]-1]) in [1, -1]:
            self.valid_cols.append(location[1])

    def compress_local(self, local=True):
        """Converts the arrays within traverse_data to be the same length as the cantilever
        At time of calling, there are two entries per unit length"""

        # Don't do anything if not the local variant
        if not local:
            return

        for link_type in self.data:
            for internal_type in self.data[link_type]:
                # In the parallel algorithm arrays can be different lengths, so align by column 0
                forward_data = self.data[link_type][internal_type]['top']
                backward_data = self.data[link_type][internal_type]['bottom']

                comb_array = np.zeros((2, max(len(forward_data),
                                              len(backward_data))))
                comb_array[0, :len(forward_data)] = forward_data
                comb_array[1, :len(backward_data)] = backward_data

                # Take maximum value in each column
                updated = np.max(np.array(comb_array), axis=0)

                # Sometimes floating point rounding leads to small negative numbers, so remove them
                updated[updated < 0] = 0

                # Update list
                self.data[link_type][internal_type] = list(updated)

    def stochastic_calculate_column_probabilities(self, simple=False, print_debug=True):
        """Calculates the probability distribution along columns

        Outputs:
            prob_combined   probability of placing in each column
        """

        # Initialise arrays
        scaled_traverse_data = {'row': {'moment': [],
                                        'axial': []},
                                'col': {'moment': [],
                                        'axial': []}}
        self.array_length = len(self.data['row']['moment'])

        # Simple algorithm
        if simple:
            # Collate criticalnesses
            max_criticalnesses = pow(np.max(np.array([self.data['row']['moment'],
                                                      self.data['col']['axial'],
                                                      self.data['row']['moment'],
                                                      self.data['col']['axial']]),
                                            axis=0), 2)

            # Convert to probabilities
            prob_combined = max_criticalnesses / sum(max_criticalnesses)

            if print_debug:
                print('  Combined probability = [' +
                      ', '.join('{:.3f}'.format(i) for i in prob_combined) + ']')

            return prob_combined

        # Combine all traverse data into one
        for link_type in self.data:
            for internal_type in self.data[link_type]:
                raw_array = np.array(self.data[link_type][internal_type])

                scaled_traverse_data[link_type][internal_type] = pow(raw_array, self.index_factor)

        # Shift indices so column link reinforcements are made in adjacent columns
        self.shift_indices(scaled_traverse_data)

        # Combine into a single urgency array
        combined_urgency = np.zeros(self.array_length)

        for idx in range(self.array_length):

            # Create separate urgency distributions for moment and axial forces
            urgency_dist_row_moment = urgency_distribution(scaled_traverse_data['row']['moment'][idx],
                                                           self.array_length, idx,
                                                           self.width_factor)
            urgency_dist_col_moment = urgency_distribution(scaled_traverse_data['col']['moment'][idx],
                                                           self.array_length, idx,
                                                           self.width_factor)
            urgency_dist_row_axial = urgency_distribution(scaled_traverse_data['row']['axial'][idx],
                                                          self.array_length, idx,
                                                          self.width_factor)
            urgency_dist_col_axial = urgency_distribution(scaled_traverse_data['col']['axial'][idx],
                                                          self.array_length, idx,
                                                          self.width_factor)

            # Sum distributions
            combined_urgency += urgency_dist_row_moment + urgency_dist_col_moment + \
                urgency_dist_row_axial + urgency_dist_col_axial

        # Convert to probability
        prob_combined = combined_urgency/sum(combined_urgency)

        if print_debug:
            print('  Combined probability = [' +
                  ', '.join('{:.3f}'.format(i) for i in prob_combined) + ']')

        return prob_combined

    def shift_indices(self, scaled_data_dictionary):
        """Shifts indices so column link reinforcements are made in adjacent columns"""

        # Shift indices one right
        for internal_type in scaled_data_dictionary['col']:
            scaled_data_dictionary['col'][internal_type] = np.concatenate(
                ([0], scaled_data_dictionary['col'][internal_type][:-1]))


def urgency_distribution(peak_urgency, length, idx_centre, width_factor):
    """Creates an urgency distribution of given length at given centre"""

    urgency_dist = np.zeros(length)

    if peak_urgency != 0:
        # Don't add anything if the urgency is zero
        for idx in range(length):

            urgency_dist[idx] = peak_urgency / np.sqrt(2*np.pi*width_factor/peak_urgency) * \
                np.exp(- pow(idx-idx_centre, 2) /
                       (2*width_factor/peak_urgency))

    return urgency_dist
