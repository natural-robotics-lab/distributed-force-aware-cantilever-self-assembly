"""Script to enumerate all the structures with a given number of elements.

There are six modes, each called with different parameters:

    enumerate
    -------------------------------
        Tests all possible combinations of N elements and records M_max and F_max.

        Inputs:
            no_elements     The number of elements
        Outputs:
            A file called All_combinations_XX_elements.csv where XX is the number of elements
            selected. This file contains all configurations of this many elements, and the maximum M
            and F throughout the structure for this configuration.

    optimums
    -------------------------------
        Analyses these precomputed data to find optimum structures for given limits.

        Inputs:
            no_elements     Either a specific number of elements or 'all' to test up to the maximum
                            number of elements in our data
            M_allowable     Allowable moment
            F_allowable     Allowable axial force
        Outputs:
            Directory of the limit pair, containing longest stable configurations of up to
            no_elements elements.

    enumerate_large
    -------------------------------
        Enumerates possible configurations for a specified limit pair and number of elements. We
        take the portion common to the previous 5 stable structures, and use that to fix the length
        of the top few rows.

        Inputs:
            no_elements     The number of elements
            M_allowable     Allowable moment
            F_allowable     Allowable axial force


    optimums_large
    -------------------------------
        Analyses these precomputed data to find optimum structures for given limits. Requires that
        there's already an existing directory with some results for this limit pair, as we build on
        those. Allows for larger structures than the standard function.

        Inputs:
            no_elements     A specific number of elements to test
            M_allowable     Allowable moment
            F_allowable     Allowable axial force
        Outputs:
            Additional entries in directory of the limit pair, containing longest
            stable configurations of up to no_elements elements.

    collate
    -------------------------------
        Collates the optimum data for all limit pairs in the directory.

        Inputs:
            fonts           'latex' or Times New Roman ('tnr') fonts
            font_size       Size of fonts in the plots
            plots           Whether to 'show' or 'save' the plots
                            (use save_pdf / save_eps to also save vector images)
        Outputs:
            elements_to_length.csv and length_to_elements.csv in the limit pair directory
            'Collated_data' graphs in the 'Optimisation' directory

    draw
    -------------------------------
        Draws specific stable cantilever configurations.

        Inputs:
            length          The length of cantilever to plot
            M_allowable     Allowable moment
            F_allowable     Allowable axial force
            plots           Whether to 'show' or 'save' the plots
                            (use save_pdf / save_eps to also save vector images)
        Outputs:
            Images of all the stable configurations of specified length, saved in the limit pair 
            directory with filename "l[length]_[config_no]"
"""

import glob
import os
import re
import sys
from time import time

from package.basics import data_dir
from package.cantilevers.structural_optimisation.collate import collate_results
from package.cantilevers.structural_optimisation.draw import draw_result
from package.cantilevers.structural_optimisation.enumerate import (
    enumerate_options, enumerate_options_large)
from package.cantilevers.structural_optimisation.optimums import \
    optimum_structures
from package.cantilevers.structural_optimisation.optimums_large import \
    large_optimum_structures

# 'enumerate', 'optimums', 'enumerate_large', 'optimums_large', 'collate', or 'draw'
MODE = 'draw'
NO_ELEMENTS = 15
FONTS = 'tnr'
FONT_SIZE = 20
PLOTS = 'show'      # 'show' or 'save'

# Limits when calculating optimums
LIMITS = {'moment': 139,
          'axial': 579}


# Must be before the input parsing section
def max_no_elements():
    """Returns the maximum number of elements we've tested the combinations up to"""
    csv_files = glob.glob(os.path.join(data_dir(),
                                       'Optimisation/All_combinations_*_elements.csv'))

    tested_max_elements = sorted(
        [int(re.search(r'combinations_(\d+)_elements', filename).group(1)) for filename in csv_files])

    max_elements = 0
    # Go through list and check for continuity
    for idx, _ in enumerate(tested_max_elements[1:]):
        if tested_max_elements[idx+1] == tested_max_elements[idx]+1:
            max_elements = tested_max_elements[idx+1]
        else:
            break
    return max_elements


max_no_elements()

# If called with arguments, update the limits
if len(sys.argv) > 1:
    MODE = sys.argv[1]

    if MODE in ['enumerate_large', 'optimums_large']:
        NO_ELEMENTS = int(sys.argv[2])
        LIMITS['moment'] = int(sys.argv[3])
        LIMITS['axial'] = int(sys.argv[4])

    if MODE in ['enumerate', 'optimums', 'draw']:
        try:
            NO_ELEMENTS = int(sys.argv[2])
        except ValueError:
            if sys.argv[2] == 'all':
                NO_ELEMENTS = max_no_elements()
            else:
                raise ValueError(
                    'Invalid number of elements: must be int or "all"')

    if MODE in ['optimums', 'draw']:
        LIMITS['moment'] = int(sys.argv[3])
        LIMITS['axial'] = int(sys.argv[4])

    if MODE in ['collate']:
        FONTS = sys.argv[2]
        FONT_SIZE = int(sys.argv[3])
        PLOTS = sys.argv[4]

    if MODE == 'draw':
        PLOTS = sys.argv[5]


def main():
    """Main function"""
    start = time()

    if MODE == 'enumerate':
        enumerate_options(NO_ELEMENTS)
    elif MODE == 'optimums':
        optimum_structures(NO_ELEMENTS, LIMITS)
    elif MODE == 'enumerate_large':
        enumerate_options_large(NO_ELEMENTS, LIMITS)
    elif MODE == 'optimums_large':
        large_optimum_structures(NO_ELEMENTS, LIMITS)
    elif MODE == 'collate':
        collate_results(FONTS, FONT_SIZE, PLOTS)
    elif MODE == 'draw':
        draw_result(NO_ELEMENTS, LIMITS, PLOTS)
    else:
        raise ValueError(
            'Invalid mode: must be "enumerate", "optimums", "optimums_large", "collate", or "draw"')

    print('Overall script finished in {:.2f} minutes'.format(
        (time()-start)/60))


# Run main function
if __name__ == "__main__":
    main()
