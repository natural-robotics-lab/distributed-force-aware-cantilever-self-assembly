"""run mode"""

import multiprocessing as mp
import os
import sys
from itertools import repeat

import fasteners

from ...basics import data_dir, make_dir, make_or_clean_dir
from ...cantilevers.parallel.paralg import convert_return_code
from ...cantilevers.parallel.paralg import \
    run_algorithm as run_parallel_algorithm
from ...cantilevers.sequential.seqalg import \
    run_algorithm as run_sequential_algorithm
from .shared import test_directory


def run_trials(algorithm_name, no_trials, elements_per_trial, limits, element_delay):
    """ Run the specified number of trials"""

    # Can only run parallel_delay0 if element delay is zero
    if algorithm_name == 'parallel_delay0' or element_delay == 0:
        if algorithm_name != 'parallel_delay0' or element_delay != 0:
            print('Unsure whether you meant to run parallel_delay0 trials:')
            response = input('Would you like to continue with parallel_delay0? (y/n)\n')

            if response.lower() == 'y':
                algorithm_name = 'parallel_delay0'
                element_delay = 0
            else:
                print('Aborting tests')
                return

    # Set the algorithm class (sequential or parallel)
    alg_class = algorithm_name.split('_')[0]

    if alg_class == 'sequential' and ('local' not in algorithm_name
                                      and 'messagepassing' not in algorithm_name):
        raise ValueError("Sequential algorithm name must include 'local' or 'messagepassing")

    # Make test directory
    make_dir(os.path.join(data_dir(), 'Results', algorithm_name))

    export_dir = test_directory(algorithm_name, limits)

    # Separate directory for element delay within limit directory for parallel algorithm
    # (Not the case for element_delay == 0: linear case)
    if alg_class == 'parallel' and element_delay != 0:
        make_dir(export_dir)
        export_dir = os.path.join(export_dir,
                                  '{}delay'.format(element_delay))

    # Make output directory and note maximum elements per trial
    make_or_clean_dir(export_dir)
    with open(os.path.join(export_dir, 'Readme.md'), 'w') as file:
        file.write('Trials run up to {} elements'.format(elements_per_trial))

    # Prepare parameters
    local = bool('local' in algorithm_name)
    simple = bool('simple' in algorithm_name)
    random = bool('random' in algorithm_name)

    # Run trials
    """
    # Solve linearly
    make_or_clean_dir(export_dir)
    for trial_no in list(range(1, no_trials+1)):
        run_stochastic_trial(algorithm_name, trial_no, elements_per_trial, local,
                             simple, random, limits, element_delay)
    """

    # Solve in parallel manner through multiprocessing module

    pool = mp.Pool(mp.cpu_count())
    pool.starmap(run_stochastic_trial, zip(repeat(algorithm_name),
                                           range(1, no_trials+1),
                                           repeat(elements_per_trial),
                                           repeat(local),
                                           repeat(simple),
                                           repeat(random),
                                           repeat(limits),
                                           repeat(element_delay)))
    pool.close()

    # Remove lock file
    try:
        os.remove(os.path.join(export_dir, '.~lock'))
    except FileNotFoundError:
        pass


def run_stochastic_trial(algorithm_name, trial_no, elements_per_trial, local, simple, random, limits,
                         element_delay):
    """Runs a trial of the stochastic algorithm"""

    print('  Running trial {}...'.format(trial_no))

    # Format limits in the correct manner
    limits_reformatted = {'allowable': limits,
                          'failure': {'moment': None,
                                      'axial':  None}}

    # Set algorithm class
    algorithm_class = algorithm_name.split('_')[0]

    if sys.platform == 'win32':         # On desktop, print to the output directory

        # Set the export directory
        export_dir = test_directory(algorithm_name, limits)

        if algorithm_class == 'parallel' and element_delay != 0:
            export_dir = os.path.join(export_dir,
                                      '{}delay'.format(element_delay))

        sys.stdout = open(os.path.join(export_dir, 'Trial_{}.out'.format(trial_no)), 'w')

    if algorithm_class == 'sequential':
        # Start trial
        run_sequential_algorithm(max_elements=elements_per_trial,
                                 limits=limits_reformatted,
                                 trial=trial_no,
                                 local=local,
                                 deterministic=False,
                                 simple=simple,
                                 plots='None',
                                 save_stable_structures=True)

        if sys.platform == 'win32':         # Reinstate print on desktop
            sys.stdout = sys.__stdout__

        return None

    elif algorithm_class == 'parallel':

        # Start trial
        return_code = run_parallel_algorithm(max_elements=elements_per_trial,
                                             element_delay=element_delay,
                                             limits=limits_reformatted,
                                             plots='None',
                                             save=False,
                                             trial=trial_no,
                                             save_multiple_trials=True,
                                             simple=simple,
                                             random=random)

        if sys.platform == 'win32':         # Reinstate print on desktop
            sys.stdout = sys.__stdout__

        save_return_code(export_dir, return_code, trial_no)

        return return_code

    else:
        raise ValueError('Multiple trials only valid for sequential or parallel algorithms')


def save_return_code(export_dir, return_code, trial_no):
    """Saves the return codes to a status.out file in the export directory"""

    # Define filenames
    return_codes_file = os.path.join(export_dir, 'status.out')
    lockfile = os.path.join(export_dir, '.~lock')

    # String to append
    str_to_write = 'Trial {}: {}\n'.format(trial_no, convert_return_code(return_code))

    # Acquire lock
    rw_lock = fasteners.InterProcessReaderWriterLock(lockfile)
    rw_lock.acquire_write_lock()

    # Append to file
    with open(return_codes_file, 'a') as f:
        f.write(str_to_write)

    # Release lock
    rw_lock.release_write_lock()
