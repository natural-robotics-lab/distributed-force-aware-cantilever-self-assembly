"""Compare mode for the parallel algorithm"""
import glob
import os
import re

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib.ticker import MaxNLocator

from ...basics import calculate_criticalness, data_dir, show_or_save, collated_label
from ...graphics.colours import colours_iterator, custom_sns_palette
from .shared import add_origin, limit_directory, limits_in_directory


def process_parallel_delay0():
    """Processes the trials with delay0 (sequential) to compare the parallel data to"""

    algorithm_dir = os.path.join(data_dir(), 'Results', 'parallel_delay0')

    # Write a readme file to this directory
    with open(os.path.join(algorithm_dir, 'Readme.md'), 'w') as file:
        file.write('This directory contains the results of the parallel algorithm with delay = 0\n')
        file.write(
            'This simulates the sequential algorithm with active links and M and F calculated at each step.')

    # Loop over the limit directories
    for limit_dir in limit_directories(algorithm_dir):
        print('Processing for {}...'.format(os.path.split(limit_dir)[-1]))

        failed_trials = print_trial_report(limit_dir)

        average_over_trials(limit_dir, failed_trials, 'save')

    return


def limit_directories(base_dir):
    """Returns a list of the limit directories in this directory"""
    limit_dirs = glob.glob(os.path.join(base_dir, '*m_*a'))

    limit_dirs.sort(key=lambda x: int(re.search(r'(\d+)m_(\d+)a', x).group(1)))

    return limit_dirs


def add_sequential_case(limits, length_steps_dfs, trials_collated_dfs, max_internals_dfs):
    """Adds the sequential case the the start of the lists of dataframes"""

    print('Adding parallel_delay0')

    limit_dir = os.path.join(data_dir(),
                             'Results',
                             'parallel_delay0',
                             '{}m_{}a'.format(limits['moment'], limits['axial']))

    if not os.path.exists(limit_dir):
        raise FileNotFoundError('No sequential case for {}m_{}a'.format(
            limits['moment'], limits['axial']))

    length_steps_dfs.insert(0, pd.read_csv(os.path.join(limit_dir, 'length_steps.csv')))
    trials_collated_dfs.insert(0, pd.read_csv(os.path.join(limit_dir, 'trials_collated.csv')))
    max_internals_dfs.insert(0, pd.read_csv(os.path.join(limit_dir, 'max_internals.csv')))


def compare_parallel_trials(algorithm_name, limits, plots):
    """Compares the different element delays for a given limit pair"""

    limit_dir = limit_directory(algorithm_name, limits)

    # Load step no vs length dataframes for this delay
    length_steps_dfs = []
    trials_collated_dfs = []
    max_internals_dfs = []
    for element_delay in delays(limit_dir):
        print('Processing for {}...'.format(
            element_delay_label(element_delay).lower()))
        failed_trials = print_trial_report(os.path.join(limit_dir,
                                                        '{}delay'.format(element_delay)))

        (length_steps_df, trials_collated_df,
         max_internals_df) = average_over_trials(os.path.join(limit_dir,
                                                              '{}delay'.format(element_delay)),
                                                 failed_trials,
                                                 plots)

        length_steps_dfs.append(length_steps_df)
        trials_collated_dfs.append(trials_collated_df)
        max_internals_dfs.append(max_internals_df)

    # Add sequential case
    add_sequential_case(limits, length_steps_dfs, trials_collated_dfs, max_internals_dfs)

    # Plot the graph of cantilever length against number of steps
    plot_length_steps(length_steps_dfs, trials_collated_dfs, delays(limit_dir, True))
    show_or_save(plt.gcf(), plots, os.path.join(limit_dir, 'length_steps'))

    # Plot barplots showing consistency of behaviour
    barplot_df = generate_barplot_df(trials_collated_dfs, delays(limit_dir, True), limits)

    for col_title, filename in barplot_titles():

        barplot_fig = plot_barplots(barplot_df, col_title)
        show_or_save(barplot_fig, plots, os.path.join(limit_dir, filename))


def collate_parallel_data(algorithm_name, plots):
    """Collates the previously analysed data into a single plot across all limit pairs"""

    # Dictionary of dataframe lists
    length_steps = {}
    trials_collated = {}
    max_internals = {}
    barplot_dfs = []

    for limits in limits_in_directory(algorithm_name):
        print('Loading for {}m_{}a'.format(limits['moment'], limits['axial']))

        # Load dataframes for this limit pair into a list
        limit_dir = limit_directory(algorithm_name, limits)

        length_steps_dfs, trials_collated_dfs, max_internals_dfs = read_preprocessed_data(limit_dir)

        # Add sequential case
        add_sequential_case(limits, length_steps_dfs, trials_collated_dfs, max_internals_dfs)

        # Save these dataframes to the dictionary
        length_steps[limit_dir] = length_steps_dfs
        trials_collated[limit_dir] = trials_collated_dfs
        max_internals[limit_dir] = max_internals_dfs

        # Create list of dataframes to collate for the barplot drawing
        barplot_df = generate_barplot_df(trials_collated_dfs, delays(limit_dir, True), limits)
        barplot_df.insert(0, 'Link strength', collated_label(limits['moment'],
                                                             limits['axial']))

        # Rename delay column appropriately for legend
        barplot_df['Delay (steps)'] = barplot_df['Delay (steps)'].apply(element_delay_label)

        barplot_dfs.append(barplot_df)

    barplot_df = pd.concat(barplot_dfs)

    # Plot the graph of cantilever length against number of steps
    length_steps_fig = plot_length_steps_collated(algorithm_name, length_steps, trials_collated)
    show_or_save(length_steps_fig, plots, os.path.join(data_dir(),
                                                       'Results',
                                                       algorithm_name,
                                                       'length_steps_collated'))

    # Plot barplots showing consistency of behaviour
    for col_title, filename in barplot_titles():
        barplot_fig = plot_barplots(barplot_df, col_title)
        show_or_save(barplot_fig, plots, os.path.join(data_dir(),
                                                      'Results',
                                                      algorithm_name,
                                                      filename+'_collated'))

    m_f_combined_fig = combined_moment_axial_criticalness(barplot_df)
    show_or_save(m_f_combined_fig, plots, os.path.join(data_dir(),
                                                       'Results',
                                                       algorithm_name,
                                                       'barplot_criticalness_collated'))

    return


def delays(parent_dir, include_sequential=False):
    """List of the XXdelay directories in the given directory"""

    delay_paths = glob.glob(os.path.join(parent_dir, '*delay'))

    delay_ints = [int(re.search(r'(\d+)delay', string)[1])
                  for string in delay_paths]

    # Sort ascending
    delay_ints.sort()

    if include_sequential:
        # Add sequential case
        delay_ints.insert(0, 0)

    return delay_ints


def trials(parent_dir, failed_ids=None):
    """List of the Trial_x.csv files in the given directory"""

    trial_paths = glob.glob(os.path.join(parent_dir, 'Trial_*.csv'))

    trial_ints = [int(re.search(r'Trial_(\d+)', string)[1])
                  for string in trial_paths]

    # Remove any failed trials from the list
    if failed_ids is not None:
        trial_ints = [trial_id for trial_id in trial_ints
                      if trial_id not in failed_ids]

    return sorted(trial_ints)


def print_trial_report(directory):
    """Prints a report of how many trials were successful, and why the failed ones didn't complete"""

    # Read into list of lines
    with open(os.path.join(directory, 'status.out')) as f:
        data_list = f.readlines()

    # Order by trial number
    data_list.sort(key=lambda x: int(re.search(r'Trial (\d+):', x)[1]))

    # Collate return code strings that result in error messages
    failures = []
    for output_msg in data_list:
        if 'successfully' not in output_msg:
            failures.append('    ' + output_msg.strip('\n'))

    # Print information
    print('  {} / {} trials were successful'.format(len(data_list)-len(failures),
                                                    len(data_list)))
    for f_str in failures:
        print(f_str)

    # Return the failed trial numbers
    return [int(re.search(r'Trial (\d+):', msg).group(1)) for msg in failures]


def average_over_trials(directory, failed_ids, plots):
    """Loads the trails in the specified directory"""

    # Dictionaries to be converted to dataframes
    length_steps_dict = {'Step number': [0]}
    max_internals_dict = {'Length': np.zeros(1),
                          'Moment': np.zeros(1),
                          'Axial force': np.zeros(1)}

    trials_collated_dict = {'Trial': [],
                            'Steps taken': [],
                            'Length reached': [],
                            'Timedout elements': [],
                            'Max M': [],
                            'Max F': []}

    for trial in trials(directory, failed_ids):
        # Load trial csv file as dataframe
        trial_df = pd.read_csv(os.path.join(directory,
                                            'Trial_{}.csv'.format(trial)))

        # Add new entries to step number if required
        trial_steps = max(trial_df['Step no'])
        if trial_steps > max(length_steps_dict['Step number']):
            length_steps_dict['Step number'] = list(range(trial_steps+1))

        # Add dictionary entry of lengths
        length_steps_dict['Trial {} length'.format(
            trial)] = trial_df['Length of placed elements'].tolist()

        # Add new entries to lengths if required
        trial_length_reached = max(trial_df['Length of placed elements'])
        if trial_length_reached > max(max_internals_dict['Length']):
            max_internals_dict['Length'] = np.arange(1, trial_length_reached+1)

        # Extract arrays of M and F
        moments = trial_df.groupby(['Length of placed elements']).max()[
            'Maximum moment (Ndm)'].to_numpy()
        axials = trial_df.groupby(['Length of placed elements']).max()[
            'Maximum axial force (N)'].to_numpy()

        # Pad to same length
        if len(moments) > len(max_internals_dict['Moment']):
            max_internals_dict['Moment'] = np.pad(max_internals_dict['Moment'],
                                                  (0, len(moments)-len(max_internals_dict['Moment'])))
            max_internals_dict['Axial force'] = np.pad(max_internals_dict['Axial force'],
                                                       (0, len(axials)-len(max_internals_dict['Axial force'])))
        elif len(moments) < len(max_internals_dict['Moment']):
            moments = np.pad(moments, (0,
                                       len(max_internals_dict['Moment'])-len(moments)))
            axials = np.pad(axials, (0,
                                     len(max_internals_dict['Axial force'])-len(axials)))

        # Update internals dict
        max_internals_dict['Moment'] = np.maximum(max_internals_dict['Moment'], moments)
        max_internals_dict['Axial force'] = np.maximum(max_internals_dict['Axial force'], axials)

        # Update collated trials data dict
        trials_collated_dict['Trial'].append(trial)
        trials_collated_dict['Steps taken'].append(trial_steps)
        trials_collated_dict['Length reached'].append(trial_length_reached)
        trials_collated_dict['Timedout elements'].append(
            trial_df['No. elements (timed out)'].iloc[-1])
        trials_collated_dict['Max M'].append(max(trial_df['Maximum moment (Ndm)']))
        trials_collated_dict['Max F'].append(max(trial_df['Maximum axial force (N)']))

    # Pad shorter trials with their final length, and record number of steps taken
    max_steps = max(length_steps_dict['Step number'])

    for trial in trials(directory, failed_ids):
        trial_key = 'Trial {}'.format(trial)

        # Extract number of steps taken and final length
        trials_collated = len(length_steps_dict[trial_key+' length'])-1
        final_length = length_steps_dict[trial_key+' length'][-1]

        # Pad list with final length
        length_steps_dict[trial_key+' length'].extend([final_length] * (max_steps-trials_collated))

    # Convert to dataframes
    length_steps_df = pd.DataFrame(length_steps_dict)
    trials_collated_df = pd.DataFrame(trials_collated_dict)
    max_internals_df = add_origin(pd.DataFrame(max_internals_dict)).reset_index(drop=True)

    # Add average, max, and min column to lengths
    length_steps_df['Average length'] = length_steps_df.iloc[:, 1:].mean(axis=1)
    length_steps_df['Max length'] = length_steps_df.iloc[:, 1:-1].max(axis=1)
    length_steps_df['Min length'] = length_steps_df.iloc[:, 1:-2].min(axis=1)
    length_steps_df['5% quartile'] = length_steps_df.iloc[:, 1:-1].quantile(0.05, axis=1)
    length_steps_df['95% quartile'] = length_steps_df.iloc[:, 1:-1].quantile(0.95, axis=1)

    # Export to csvs if necessary
    if plots == 'save':
        length_steps_df.to_csv(os.path.join(directory, 'length_steps.csv'),
                               index=False, float_format='%.3f')
        trials_collated_df.to_csv(os.path.join(directory, 'trials_collated.csv'),
                                  index=False)
        max_internals_df.to_csv(os.path.join(directory, 'max_internals.csv'),
                                index=False, float_format='%.3f')

    return length_steps_df, trials_collated_df, max_internals_df


def read_preprocessed_data(limit_dir):
    """Reads the preprocessed .csv files in this directory"""

    length_steps_dfs = []
    trials_collated_dfs = []
    max_internals_dfs = []

    for element_delay in delays(limit_dir):
        delay_dir = os.path.join(limit_dir, '{}delay'.format(element_delay))

        # Add the dataframe for this element delay to the list of dataframes
        length_steps_dfs.append(pd.read_csv(os.path.join(delay_dir,
                                                         'length_steps.csv')))
        trials_collated_dfs.append(pd.read_csv(os.path.join(delay_dir,
                                                            'trials_collated.csv')))
        max_internals_dfs.append(pd.read_csv(os.path.join(delay_dir,
                                                          'max_internals.csv')))

    return length_steps_dfs, trials_collated_dfs, max_internals_dfs


def plot_length_steps(length_steps_dfs, trials_collated_dfs, element_delays, axis=None):
    """Plots the length against the number of steps taken for different element delays.

    Inputs:
        length_steps_dfs    List of dataframes with length reached at each step across
                            trials
        trials_collated_dfs List of dataframes with steps taken until simulation finished
                            across trials
        element_delays      Delays associated with the dataframes
        axis                Axis to plot on (initialises a new figure if not specified)
    """

    # Initialise new figure if axis not specified
    if axis is None:
        plt.figure(figsize=(869/96, 560/96), dpi=96)
        plt.rcParams["font.family"] = "Times New Roman"
        plt.rcParams["font.size"] = "20"
        plt.rcParams["mathtext.fontset"] = "stix"
        axis = plt.gca()
        titles = True       # Add axis titles and legend
    else:
        titles = False      # Don't add axis titles and legend

    # Iterators for colour
    colours = colours_iterator()

    # Draw each trace
    for length_steps_df, trials_collated_df, element_delay in zip(length_steps_dfs,
                                                                  trials_collated_dfs,
                                                                  element_delays):
        # Define label and colour correctly
        label = element_delay_label(element_delay)
        colour = next(colours)

        # Average line
        axis.plot(length_steps_df['Step number'], length_steps_df['Average length'],
                  color=colour, label=label)

        # Error bars
        axis.fill_between(length_steps_df['Step number'],
                          length_steps_df['5% quartile'],
                          length_steps_df['95% quartile'],
                          alpha=0.3, facecolor=colour)

    if titles:
        axis.set_ylim([0, None])
        axis.yaxis.set_major_locator(MaxNLocator(integer=True))
        axis.set_xlabel('Number of steps')
        axis.set_ylabel('Cantilever length (placed elements)')
        axis.legend(loc='lower right')

        # Show full range if drawing on its own plot
        axis.set_xlim([0, max([max(df['Step number'])
                               for df in length_steps_dfs])])
    return axis


def generate_barplot_df(trials_collated_dfs, element_delays, limits):
    """Converst the dataframe lists to a single dataframe in the format required to draw barplots 
    with Seaborn"""

    # Add column to the start of each dataframe with delay
    for delay, trials_collated_df in zip(element_delays, trials_collated_dfs):
        if delay == 0:
            delay_label = 'Sequential'
        else:
            delay_label = delay

        trials_collated_df.insert(0, 'Delay (steps)', delay_label)

        # Convert moment and axial force to criticalness
        trials_collated_df['Maximum moment criticalness'] = trials_collated_df['Max M'].apply(
            calculate_criticalness, args=[limits['moment']])
        trials_collated_df['Maximum axial force criticalness'] = trials_collated_df['Max F'].apply(
            calculate_criticalness, args=[limits['axial']])

    return pd.concat(trials_collated_dfs)


def barplot_titles():
    """Returns a list of the dataframe titles and associated filenames of data to draw
    barplots of"""

    return [['Steps taken', 'barplot_steps'],
            ['Length reached', 'barplot_length'],
            ['Timedout elements', 'barplot_timedout'],
            ['Maximum moment criticalness', 'barplot_moment'],
            ['Maximum axial force criticalness', 'barplot_axial']]


def plot_barplots(barplot_df, y_plot):
    """Plots the length against the number of steps taken for different element delays.

    The minimum number of steps for each trial is noted with a triangle, and the average with
    a cross

    Inputs:
        barplot_df      Dataframe containing data in the correct fasion for boxpot drawing
        y_plot          Column title of the data to plot on the y axis
    """

    # Set parameters for graph size
    fig_height = 300/96
    if 'Link strength' in barplot_df.columns:
        fig_width = 260/96 * len(barplot_df['Link strength'].unique())
        x_plot = 'Link strength'
        grouping = 'Delay (steps)'

    else:
        fig_width = 700/96
        x_plot = 'Delay (steps)'
        grouping = None

    plt.rcParams["font.family"] = "Times New Roman"
    plt.rcParams["font.size"] = "20"
    plt.rcParams["mathtext.fontset"] = "stix"

    sns.set_palette(custom_sns_palette())
    box_plot = sns.catplot(kind='bar', x=x_plot, y=y_plot, ci='sd', data=barplot_df, hue=grouping,
                           legend=False, height=fig_height, aspect=fig_width/fig_height)

    # Rework titles
    if y_plot == 'Steps taken':
        plt.ylabel('Timesteps taken')
    elif y_plot == 'Length reached':
        plt.ylabel('Final length (dm)')
    elif y_plot == 'Maximum moment criticalness':
        plt.ylabel('Maximum moment\ncriticalness')
    elif y_plot == 'Maximum axial force criticalness':
        plt.ylabel('Maximum axial force\ncriticalness')

    # Show a legend if grouping
    if grouping is not None:
        if y_plot == 'Steps taken':
            legend_loc = 'upper right'
        elif y_plot == 'Length reached':
            legend_loc = 'upper left'
        else:
            legend_loc = 'best'

        box_plot.ax.legend(ncol=2, loc=legend_loc)

    return plt.gcf()


def element_delay_label(element_delay):
    """Converts the element delay to a string (delay of 0 is given a different label)"""

    try:
        delay_int = int(element_delay)
    except ValueError as err:
        if element_delay.lower() == 'sequential':
            return 'Sequential'
        else:
            raise TypeError from err

    if delay_int > 0:
        return 'Delay = {}'.format(element_delay)
    else:
        return 'Sequential'


def plot_length_steps_collated(algorithm_name, length_steps, trials_collated,):
    """Plots the length_steps collated graph

    Inputs:
        length_steps    a dictionary of lists of dataframes
        trials_collated     a dictionary of lists of dataframes
    """

    # Initialise graph with necessary subplots
    no_subplots = len(limits_in_directory(algorithm_name))
    plt.rcParams["font.family"] = "Times New Roman"
    plt.rcParams["font.size"] = "20"
    plt.rcParams["mathtext.fontset"] = "stix"
    fig, axes = plt.subplots(1, no_subplots,
                             figsize=(700/96*no_subplots,
                                      500/96),
                             dpi=96, sharey=True)

    max_steps = 0
    for limits, axis in zip(limits_in_directory(algorithm_name), axes):
        # Extract the necessary dataframes list for this limit directory
        limit_dir = limit_directory(algorithm_name, limits)

        # Plot graph
        plot_length_steps(length_steps[limit_dir], trials_collated[limit_dir],
                          delays(limit_dir, True), axis)

        # Save max steps for later scaling of axes
        max_steps = max(max_steps, max([max(df['Steps taken'])
                                        for df in trials_collated[limit_dir][1:]]))

    for axis, limits in zip(axes, limits_in_directory(algorithm_name)):
        # Add subplot titles
        axis.set_title(collated_label(limits['moment'], limits['axial'], links=True),
                       fontweight='bold')
        # Set axis limits
        axis.set_ylim(0, None)
        axis.set_xlim(0, max_steps)
        axes[-1].yaxis.set_major_locator(MaxNLocator(integer=True))

    axes[0].legend(loc='upper left', ncol=2)

    # Create large hidden frame to give global xaxis title
    fig.add_subplot(111, frameon=False)
    fig.axes[-1].tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
    fig.axes[-1].set_xlabel('Number of steps')
    axes[0].set_ylabel('Cantilever length (placed elements)')

    return fig


def combined_moment_axial_criticalness(barplot_df):
    """Collates the maximum moment and axial force criticalnesses onto a single figure"""

    plt.rcParams["font.family"] = "Times New Roman"
    plt.rcParams["font.size"] = "20"
    plt.rcParams["mathtext.fontset"] = "stix"

    # Plot comparison of M and F on the same graph
    fig, axes = plt.subplots(1, 2,
                             figsize=(260/96*5, 300/96), dpi=96,
                             sharey=True)
    plt.subplots_adjust(wspace=0.01)

    for col_title, axis in zip(['Maximum moment criticalness',
                                'Maximum axial force criticalness'], axes):
        sns.set_palette(custom_sns_palette())

        sns.barplot(x='Link strength', y=col_title, ci='sd',
                    data=barplot_df, hue='Delay (steps)',
                    ax=axis)

        axis.spines['right'].set_color('none')
        axis.spines['top'].set_color('none')

        axis.set_xlabel('')
        axis.set_ylabel('')
        axis.axhline(1, 0, 1, linestyle='dashed', color='k')

        # Only show one legend
        if axis == axes[0]:
            axis.get_legend().remove()
        else:
            axis.legend(title='', ncol=2)

    axes[0].text(0.5, 1, 'Moment', horizontalalignment='center', fontweight='bold',
                 verticalalignment='top', transform=axes[0].transAxes)
    axes[1].text(0.5, 1, 'Axial force', horizontalalignment='center', fontweight='bold',
                 verticalalignment='top', transform=axes[1].transAxes)

    # Create large hidden frame to give global xaxis title
    fig.add_subplot(111, frameon=False)
    fig.axes[-1].tick_params(labelcolor='none', top=False,
                             bottom=False, left=False, right=False)
    fig.axes[-1].set_xlabel('Link strength')
    fig.axes[0].set_ylabel('Maximum criticalness')

    return fig
