"""File containing the basic functions used in the structure scripts"""
import os
import platform

import matplotlib.pyplot as plt


def data_dir():
    """Returns the path to the data directory on this system"""

    repository_dir = os.path.dirname(os.path.dirname(__file__))

    return os.path.join(repository_dir, 'output')


def export_dir_path(algorithm, local, deterministic, simple,
                    specials_before, specials_after, limits):
    """ Creates the export directory path string"""

    export_dir = os.path.join(data_dir(),
                              'auto-generated_structures',
                              algorithm + '_')

    if local:
        export_dir += 'local_'
    else:
        export_dir += 'messagepassing_'

    if simple:
        export_dir += 'simple_'

    if deterministic:
        export_dir += 'deterministic_'

    if specials_before:
        export_dir += specials_before + '_'

    # Use allowable limits in filename
    try:
        subdict = limits['allowable']
    except KeyError:
        # No subdictionary
        subdict = limits

    export_dir += str(subdict['moment']
                      ).replace('.', '-') + 'm'

    if subdict['axial']:
        export_dir += '_' + \
            str(subdict['axial']).replace('.', '-') + 'a'

    if specials_after:
        export_dir += '_' + specials_after

    return export_dir


def make_or_clean_dir(filepath):
    """Either make the directory at path dir, or remove files from it if
    it exists"""

    try:
        os.mkdir(filepath)
    except FileExistsError:
        old_files = [f for f in os.listdir(filepath)]

        for f in old_files:
            os.remove(os.path.join(filepath, f))


def make_dir(filepath):
    """Makes a directory at path dir without removing old files"""
    try:
        os.mkdir(filepath)
    except FileExistsError:
        pass


def limit_exceeded(arr, limit):
    """Checks to see if the value in the array is greater than the limit. Returns
    false if the limit is not set"""

    if limit:
        return max(arr) > limit
    else:
        return False


def calculate_criticalness(internal_value, limit):
    """Calculates the criticalness of a link with given internal state and limit.
    The internal value is either a stress, axial force, or moment, and the limit
    is the corresponding limit (or None)
    """
    if limit:
        # Limit is specified
        return internal_value/limit
    else:
        return 0.0


def is_stable(row_criticals, col_criticals, limits):
    """Whether the structure is stable or not (all links below limits)"""

    for limit_type in limits:
        max_row_criticalness = calculate_criticalness(row_criticals[limit_type].max(),
                                                      limits[limit_type])
        max_col_criticalness = calculate_criticalness(col_criticals[limit_type].max(),
                                                      limits[limit_type])

        if any([crit >= 1 for crit in [max_row_criticalness, max_col_criticalness]]):
            # Distributed algorithm says stable if maximum_criticalness < 1
            return False

    return True


class Maximums:
    """Class to store the maximum force and moment throughout each test"""

    def __init__(self):
        # Initialise with empty dictionary
        self.maximums = {'moment': 0.0,
                         'axial': 0.0}

        self.structures = {'moment': [],
                           'axial': []}

    def update(self, row_links, col_links, structure):
        """Update the dictionary if necessary"""

        for key in self.maximums:
            max_links = max(max(row_links[key]), max(col_links[key]))

            if max_links >= self.maximums[key]:
                self.maximums[key] = max_links
                self.structures[key] = structure.copy()

    def __str__(self):
        """Prints the maximum moment and axial force"""
        string = 'Maximums:\n'

        string += '  Moment (Ndm)   Axial force (N)\n'
        string += '--------------------------------\n'
        string += str(round(self.maximums['moment'], 3)).center(16)
        string += str(round(self.maximums['axial'], 3)).center(16) + '\n'

        return string


def active_element_override(mode, idx_centre, structure):
    """Overrides whether to include a link above or left when an active element is in the
    Von Neumann neighbourhood"""

    # Initialise return variable as true (assume we can place here)
    place_adjacent = True
    m = idx_centre[0]
    n = idx_centre[1]

    if structure.map[tuple(idx_centre)] == 2:
        # Current location is active
        active_element = [e for e in structure.active_elements
                          if e.location == [m-structure.row_offset,
                                            n-structure.col_offset]][0]
    else:
        active_element = None

    if mode == 'left':
        # If the element here is active, check if it's attached on its west side
        if active_element is not None and not (active_element.active_link == 'w' or
                                               'w' in active_element.passive_links):
            place_adjacent = False
        elif active_element is None and structure.map[m, n-1] == 2:
            # If the element here isn't active, check if the element on the west is active
            # and connected on its east side
            adjacent_element = [e for e in structure.active_elements
                                if e.location == [m-structure.row_offset,
                                                  n-1-structure.col_offset]][0]
            if not (adjacent_element.active_link == 'e' or
                    'e' in adjacent_element.passive_links):
                place_adjacent = False

    elif mode == 'above':
        # If the element here is active, check if it's attached on its north side
        if active_element is not None and not (active_element.active_link == 'n' or
                                               'n' in active_element.passive_links):
            place_adjacent = False
        elif active_element is None and structure.map[m-1, n] == 2:
            # If the element here isn't active, check if the element on the north is active
            # and connected on its south side
            adjacent_element = [e for e in structure.active_elements
                                if e.location == [m-1-structure.row_offset,
                                                  n-structure.col_offset]][0]
            if not (adjacent_element.active_link == 's' or
                    's' in adjacent_element.passive_links):
                place_adjacent = False

    return place_adjacent


def show_or_save(fig, plots, filename):
    """Either shows or saves the figure to the given filename"""

    if plots == 'show':
        fig.tight_layout()
        plt.show()
    elif 'save' in plots:
        plt.savefig(filename+'.png', bbox_inches='tight')

        # Only save a vector if asked
        if 'eps' in plots:
            vector_ext = '.eps'
        elif 'pdf' in plots:
            vector_ext = '.pdf'
        else:
            vector_ext = None
        if vector_ext is not None:
            plt.savefig(filename+vector_ext, bbox_inches='tight')

        plt.close()
        print('    Saved figures to '+filename)
    else:
        raise ValueError('Invalid plot mode passed: must be "save" or "show"')


def collated_label(moment, axial, links=False):
    """Returns the label of this line"""

    if moment == 139 and axial == 579:
        label = 'Weak'
    elif moment == 426 and axial == 1159:
        label = 'Medium'
    elif moment == 869 and axial == 1738:
        label = 'Strong'
    else:
        label = '$M_{allowable}$ = ' + str(moment) + ' Ndm, $F_{allowable}$ = ' + str(axial) + ' N'
        return label

    # Add 'links' if necessary
    if links:
        label += ' links'

    return label
