"""Draw mode"""

import os

import matplotlib.pyplot as plt
import pandas as pd

from ...basics import data_dir, show_or_save
from ...graphics.subplots import StructurePlot
from ...structures import Cantilever


def draw_result(length, limits, draw_mode):
    """Draws the optimal structures for a specified case"""

    directory = os.path.join(data_dir(), 'Optimisation',
                             '{}m_{}a'.format(limits['moment'], limits['axial']))

    # Read csv
    df = pd.read_csv(os.path.join(directory, 'length_to_elements.csv'))

    # Generate list of configurations
    configurations_string = df.loc[df['Length'] == length]['Configuration(s)'].values[0]
    configurations = configurations_string.replace("'", "").split(', ')

    # Initialise graph
    fig = plt.figure(figsize=(869/96, 560/96), dpi=96)
    fig.patch.set_visible(False)
    structure_plot = StructurePlot(None, 'wide', None, fig.gca(), 'Cantilever')

    for idx, configuration in enumerate(configurations):
        # Draw this configuration
        cantilever = Cantilever('from_rows', elements_in_rows=configuration)

        # Draw cantilever
        structure_plot.plot_structure(cantilever, False)

        # Scale and hide axes
        structure_plot.axis.set_title(None)
        structure_plot.axis.axis('scaled')
        structure_plot.axis.axis("off")
        structure_plot.auto_limits(cantilever, predrawn=False)

        # Show or save
        show_or_save(fig, draw_mode, os.path.join(directory,
                                                  'l{}_{}'.format(cantilever.length, idx+1)))

        if draw_mode == 'show':
            # Reset structure plot if shown
            fig = plt.figure(figsize=(869/96, 560/96), dpi=96)
            fig.patch.set_visible(False)
            structure_plot = StructurePlot(None, 'wide', None, fig.gca(), 'Cantilever')
