""" The Element class, and TopRowElement child class"""
import numpy as np

from ...basics import calculate_criticalness
from ...graphics.colours import BLUE, CYAN, ORANGE, PURPLE, YELLOW
from .seqfunc import PlacementOption
from .traverse_data import TraverseData

try:
    from ...structures import Cantilever
except ImportError:
    # Don't need these imports, it's just for type hinting
    pass


class Element:
    """Class for the new element to be added. Initialises with a moving element above the first
    element in the cantilever.

    General fields:
        location            [row col] of the element
        colour              element colour
        row_internals       internals in row links: {'moment': [left, right],
                                                     'axial': [left, right]}
        col_internals       internals in column links: {'moment': [above, below],
                                                       'axial': [above, below]}
        is_top_row          bool; is this a top row element
        top_row             the TopRowElement class instance
        steps_taken         the number of steps this element has taken
    Fields set while traversing:
        placement_options   sorted list of elements of the placement_option class, saying where
                            this element could place
    """

    def __init__(self, location=None, figure=None):
        if location is None:
            self.location = [-1, 0]
        else:
            self.location = location
        self.colour = YELLOW
        self.row_internals = {'moment': [0.0, 0.0], 'axial': [0.0, 0.0]}
        self.col_internals = {'moment': [0.0, 0.0], 'axial': [0.0, 0.0]}
        self.placement_options = []
        self.is_top_row = False
        self.top_row = TopRowElement()

        self.steps_taken = 0

        if figure:
            self.draw_frame(figure, gathering=True)

    def make_top_row_element(self, offset):
        """Converts this element into a top row element

        Inputs:
            offset  the offset between where the element currently is and where it will be when it
                    places (when placing at the tip, the element is above the current tip when this
                    function is called, so requires an offset)
        """

        self.is_top_row = True

        column = self.location[1] + offset

        self.top_row.make_top_row_element(column)

    def place(self, element_type, new_location=None, deterministic=False):
        """Place the new element somewhere

        Inputs:
            element_type    'standard', 'axial', or 'moment'
            new_location    [row col] where the new element should be placed
            stochastic      Whether this is the stochastic algorithm, in which case everything is
                            plotted blue
        """

        if not deterministic:
            element_type = 'standard'

        if new_location is None:
            # Default to plotting at the current location
            new_location = self.location
        else:
            # Otherwise update the element's location to the given one
            self.location = new_location

        if element_type == 'standard':
            self.colour = BLUE
        elif element_type == 'axial':
            self.colour = PURPLE
        elif element_type == 'moment':
            self.colour = CYAN
        else:
            raise Exception('Invalid element_type: {}'.format(element_type))

    def plot(self, figure, colour=None):
        """Plot the element on the given axis with the correct colour

        Inputs:
            figure  the axis to plot this on
            colour  (optional) element colour
        Outputs:
            obj     the object of the fill shape
        """
        centre = self.location[1]+0.5, -self.location[0]

        if colour is None:
            colour = self.colour

        obj = figure.structure_plot.plot_square(centre, colour)

        return obj

    def draw_frame(self, figure, gathering=False):
        """Draw then deletes the next frame if required

        Inputs:
            figure          the custom figure object to plot on
            gathering       whether the active element is gathering data or now
        """

        if figure.interactive:

            # Colour orange if gathering data
            if gathering:
                colour = ORANGE
            else:
                colour = YELLOW

            motion = self.plot(figure, colour)
            figure.update_now()
            motion.remove()

    def make_step(self, direction, cantilever: 'Cantilever', figure, gathering=False):
        """Steps the element one column in the direction given, including changing rows

        Inputs:
            direction       'left' or 'right'
            cantilever      the cantilever object
            figure          the custom figure object to plot on
            gathering       bool, whether the agent is gathering data
        Outputs:
            rows_changed     number of rows changed in this step:   +ve: moved down
                                                                    0:  same row
                                                                    -ve: moved up
        """

        # Counter for how many rows have been changed
        rows_changed = 0

        # Set the desired column difference and direction to move if there's an obstruction
        if direction == 'left':
            col_diff = -1
        elif direction == 'right':
            col_diff = 1
        else:
            raise ValueError('Invalid direction specified')

        # Set row direction
        coords_adjacent = [self.location[0], self.location[1]+col_diff]
        side = np.sign(self.location[0])
        if cantilever.element_at(coords_adjacent) == 0:
            # No element in the adjacent position
            row_direction = -side
        else:
            # Element in adjacent position
            row_direction = side

        # Change col first if possible, otherwise change col after changing row
        if cantilever.element_at([self.location[0], self.location[1]+col_diff]) == 0:
            self.location[1] += col_diff
            self.steps_taken += 1
            changed_col = True
        else:
            changed_col = False

        # Change row until continuity is achieved
        while True:
            if changed_col:
                # Element is already in the new column
                coords_adjacent = [self.location[0] +
                                   row_direction, self.location[1]]

                # Draw now if already in the right column
                if cantilever.element_at(coords_adjacent) == 1:
                    self.draw_frame(figure, gathering)

                while cantilever.element_at(coords_adjacent) == 0:
                    # Loop until element above or below is 1
                    rows_changed += row_direction
                    self.location[0] += row_direction
                    self.steps_taken += 1

                    # Update test location coordinates
                    coords_adjacent[0] += row_direction

                    self.draw_frame(figure, gathering)
                break
            else:
                # Element isn't in the new column yet

                while cantilever.element_at(coords_adjacent) == 1:
                    # Loop until until elements in adjacent colum are 0
                    rows_changed += row_direction
                    self.location[0] += row_direction
                    self.steps_taken += 1

                    # Update test location coordinates
                    coords_adjacent[0] += row_direction

                    # Draw frame if we're still moving
                    if cantilever.element_at(coords_adjacent) == 1:
                        self.draw_frame(figure, gathering)

                # Change column now
                self.location[1] += col_diff

                # Draw the frame
                self.draw_frame(figure, gathering)
                break

        # Check this new location is actually valid
        if self.location[1] < 0:
            raise Exception('Element moved into boundary: {}'.format(self.location))

        # Return whether we've changed row or not
        return rows_changed

    def pass_tip(self, cantilever: 'Cantilever', figure, gathering=False):
        """Animates the element passing the tip of the cantilever"""

        # Store if we're moving up or down
        row_direction = -np.sign(self.location[0])

        # Move right and up/down once
        self.location[1] += 1
        self.location[0] += row_direction
        self.steps_taken += 1

        # Move up (or down) until the tip is passed
        while cantilever.element_at([self.location[0], self.location[1]-1]):
            # Draw if there's an element left
            if cantilever.element_at([self.location[0], self.location[1]-1]):
                self.draw_frame(figure, gathering)

            self.location[0] += row_direction
            self.steps_taken += 1

        # Move left and onto the next row
        self.location[1] -= 1

        self.draw_frame(figure, gathering)

    def update_sensors(self, complete_internals, cantilever: 'Cantilever'):
        """Update the readings of the sensors of this element.

        Inputs:
            complete_internals  complete link internals, dictionary with
                                'stress', 'moment' and 'axial'
        """
        # Calculate offset index in the link stresses matrix
        idx = [self.location[0]+cantilever.row_offset,
               2*(self.location[1]+cantilever.col_offset)]

        # Update sensors to actual readings
        for key in self.row_internals:
            self.row_internals[key] = [complete_internals[key][idx[0]][idx[1]],
                                       complete_internals[key][idx[0]][idx[1]+2]]
            self.col_internals[key] = [complete_internals[key][idx[0]][idx[1]+1],
                                       complete_internals[key][idx[0]+1][idx[1]+1]]

    def is_critical(self, limits):
        """Bool; whether this element is critical or not"""

        if self.location[1] >= 0:
            # Right of support, so left link
            link_side = 0
        else:
            # Left of support, so right link
            link_side = 1

        row_moment_criticalness = calculate_criticalness(self.row_internals['moment'][link_side],
                                                         limits['moment'])
        row_axial_criticalness = calculate_criticalness(self.row_internals['axial'][link_side],
                                                        limits['axial'])

        col_moment_criticalness = calculate_criticalness(max(self.col_internals['moment']),
                                                         limits['moment'])
        col_axial_criticalness = calculate_criticalness(max(self.col_internals['axial']),
                                                        limits['axial'])

        if any(x >= 1 for x in [row_moment_criticalness, row_axial_criticalness,
                                col_moment_criticalness, col_axial_criticalness]):
            return True
        else:
            return False

    def gather_data(self, edges, cantilever: 'Cantilever', limits, figure, local, deterministic,
                    simple):
        """Moves the element along the top row, gathering sensor data as it goes.

        Inputs:
            edges               list of elements on the edges
            cantilever          current cantilever object
            limits              the limits dictionary: 'moment', 'axial'
            figure              the custom figure object to plot on
            local               bool, whether local variant or not
            deterministic       bool, whether to use the deterministic version
            simple              bool, whether to use the simple version
        """

        # Counter for the number of steps taken
        steps = 0

        # Initialise lists for stochastic algorithm
        if not deterministic:
            traverse_data = TraverseData(local)

        # List for symmetry of above support
        symmetry = []

        for external_element in edges:
            # Make a step in the correct direction
            if self.location[0] < 0:
                if self.location[1] == cantilever.length-1:
                    # Above and at tip
                    self.pass_tip(cantilever, figure,
                                  gathering=True)
                    rows_changed = 0

                    steps += 2
                else:
                    # Above cantilever
                    rows_changed = self.make_step('right', cantilever, figure, gathering=True)
                    steps += 1
            else:
                # Below cantilever
                self.make_step('left', cantilever, figure,
                               gathering=True)
                rows_changed = 0

                steps += 1

            # Check the element and top_row list are in sync
            if external_element.location[1] != self.location[1]:
                raise ValueError(
                    "Top row element's location doesn't correspond to active element's")

            if rows_changed:
                symmetry += [self.location[1]]*abs(rows_changed)

            if deterministic:
                # Deterministic algorithm

                # Calculate criticalness of row and column links
                criticalness = external_element.calculate_max_link_criticalness(local, limits)

                self.deterministic_add_placement_option(criticalness, local=local)
            else:
                # Stochastic algorithm
                if local:
                    traverse_data.append_criticalness_local(external_element, limits,
                                                            self.location)
                else:
                    traverse_data.append_criticalness_message_passing(external_element.top_row,
                                                                      limits)

        if not deterministic:
            # Convert arrays to same length as cantilever
            traverse_data.compress_local(local)

            # Generate list of placement options from traverse_data
            self.stochastic_generate_placement_options(traverse_data, simple)

        return steps

    def calculate_max_link_criticalness(self, local, limits):
        """Calculates the maximum criticalness of of this element

        Inputs:
            local       bool, whether using the local variant or not
            limits      the limits dictionary: 'moment', 'axial'
        Outputs:
            max_criticalness        maximum criticalness in links attached to this element:
                                    {'row': {'value', 'type'},
                                    'col': {'value', 'type'}}
        """
        if not local:
            # Passing data down columns, so use the top_row_element method
            return self.top_row.link_criticalness_in_column(limits)

        # Calculate criticalness in all links
        criticalnesses = {'moment': {}, 'axial': {}}
        for internal_type in criticalnesses:
            criticalnesses[internal_type] = {'top': calculate_criticalness(self.col_internals[internal_type][0],
                                                                           limits[internal_type]),
                                             'right': calculate_criticalness(self.row_internals[internal_type][1],
                                                                             limits[internal_type]),
                                             'bottom': calculate_criticalness(self.col_internals[internal_type][1],
                                                                              limits[internal_type]),
                                             'left': calculate_criticalness(self.row_internals[internal_type][0],
                                                                            limits[internal_type])}

        # Convert to absolute values
        for side in criticalnesses['moment']:
            # Moment criticalness: take abs
            criticalnesses['moment'][side] = abs(criticalnesses['moment'][side])
        for side in criticalnesses['axial']:
            # Axial criticalness: take abs only if above 0
            criticalnesses['axial'][side] = max(0.0,
                                                criticalnesses['axial'][side])

        max_criticalness = {'row': {'value': 0.0,
                                    'type': 'axial'},
                            'col': {'value': 0.0,
                                    'type': 'axial'}}

        # Update maximum criticalness in row links
        side = 'left'   # Take left link

        if criticalnesses['moment'][side] >= criticalnesses['axial'][side]:
            max_criticalness['row']['value'] = criticalnesses['moment'][side]
            max_criticalness['row']['type'] = 'moment'
        else:
            max_criticalness['row']['value'] = criticalnesses['axial'][side]
            max_criticalness['row']['type'] = 'axial'

        # Update maximum criticalness in column links
        max_moment_criticalness = max(criticalnesses['moment']['top'],
                                      criticalnesses['moment']['bottom'])
        max_axial_criticalness = max(criticalnesses['axial']['top'],
                                     criticalnesses['axial']['bottom'])

        if max_moment_criticalness >= max_axial_criticalness:
            max_criticalness['col']['value'] = max_moment_criticalness
            max_criticalness['col']['type'] = 'moment'
        else:
            max_criticalness['col']['value'] = max_axial_criticalness
            max_criticalness['col']['type'] = 'axial'

        # Return maximum criticalnesses
        return max_criticalness

    def deterministic_add_placement_option(self, criticalness, local=False):
        """Adds another placement option to the list if necessary"""

        # Explicitly iterate keys so they go in the right order
        for link_type in ['col', 'row']:
            if criticalness[link_type]['value'] > 1:
                if local:
                    placement_option = PlacementOption(self.location[1],
                                                       criticalness[link_type]['value'],
                                                       criticalness[link_type]['type'])
                else:
                    placement_option = PlacementOption(self.location[1],
                                                       criticalness[link_type]['value'],
                                                       criticalness[link_type]['type'])

                # Add to ordered location in list
                self.insert_placement_option(placement_option)

    def insert_placement_option(self, option):
        """ Inserts another placement option into the sorted list. Based on:
        https://www.geeksforgeeks.org/python-program-to-insert-an-element-into-sorted-list/

        Inputs:
            option  the PlacementOption object to be inserted
        """
        # Create list if it doesn't exist
        if len(self.placement_options) == 0:
            index = 0

        else:
            # If list exists, add to correct place based on urgency
            for i in range(len(self.placement_options)):
                if option.urgency >= self.placement_options[i].urgency:
                    index = i
                    break
                else:
                    index = i+1

        self.placement_options.insert(index, option)

    def stochastic_generate_placement_options(self, traverse_data, simple):
        """ Create placement options list from traverse_data"""

        # Return empty list if all criticalnesses below 1
        if max([max(traverse_data.data['row']['moment']),
                max(traverse_data.data['row']['axial']),
                max(traverse_data.data['col']['moment']),
                max(traverse_data.data['col']['axial'])]) < 1:
            return

        # Calculate probability distributions
        column_probs = traverse_data.stochastic_calculate_column_probabilities(simple)

        # Calculate no. placement options to draw
        no_placement_options = len(column_probs)

        # Draw placement options
        while len(self.placement_options) < no_placement_options:

            # Draw random sample
            col_no = np.random.choice(len(column_probs), p=column_probs)

            # Extract the placement options in this column
            last_options = [option for option in self.placement_options
                            if option.column == col_no]

            if len(last_options) == 0:
                # Column hasn't been chosen yet, so append to the list if required
                self.placement_options.append(PlacementOption(col_no,
                                                              column_probs[col_no],
                                                              'standard'))

    def try_placement_options(self, cantilever: 'Cantilever', figure, deterministic):
        """Try to add at locations of high stress in order of urgency

        Inputs:
            cantilever      the cantilever object
            figure          the custom figure object to plot on
            deterministic   bool, whether to use the deterministic version
        """

        # Print debug string
        print('  Possible placement options:')
        for option in self.placement_options:
            info_string = '    Column {}, '.format(option.column)

            if deterministic:
                info_string += 'urgency = '
            else:
                info_string += 'probability = '

            info_string += str(round(option.urgency, 4))

            if deterministic:
                info_string += '({})'.format(option.mechanism)

            print(info_string)

        # Try to add at locations of high stress in order of urgency

        element_placed = False

        # Loop until we place the element
        for option in self.placement_options:
            # We have options left to try
            element_placed = self.test_placement_option(option, cantilever,
                                                        deterministic, figure)

            # Don't check any more options once placed
            if element_placed:
                self.steps_taken += 1    # Placing takes one step
                return

        return

    def test_placement_option(self, option, cantilever: 'Cantilever', deterministic, figure):
        """Tests the current placement option

        Inputs:
            option              the PlacementOption object we're testing
            cantilever          the cantilever object
            deterministic       bool, whether to use the deterministic version
            figure              the custom figure object to plot on
        Outputs:
            element_placed      bool, whether we placed in this location or not
        """

        # Move to the bottom side of the cantilever
        if self.location[0] < 0:
            # Move to tip
            while self.location[1] < cantilever.length-1:
                self.make_step('right', cantilever, figure)
            # Pass tip
            self.pass_tip(cantilever, figure)

        # On the correct side of the cantilever, now go to the column
        desired_col = option.column

        while desired_col < self.location[1]:
            self.make_step('left', cantilever, figure)
        while desired_col > self.location[1]:
            self.make_step('right', cantilever, figure)

        # Check if this is a valid placement location
        if self.validate_placement(cantilever):
            # Place element
            self.place(option.mechanism, deterministic=deterministic)
            return True
        else:
            # Invalid, return false
            return False

    def validate_placement(self, cantilever: 'Cantilever'):
        """Determines whether the current location is a valid placement location,
        ie. continuous along the row """

        # Coordinates left and right of current element
        coords_left = [self.location[0], self.location[1]-1]
        coords_right = [self.location[0], self.location[1]+1]

        # Return true if either of these locations is filled
        if cantilever.element_at(coords_left) != 0 or cantilever.element_at(coords_right) != 0:
            return True

        # Return false otherwise
        return False


class TopRowElement:
    """ Class containing objects and methods used only by top row elements

    Fields:
        column                  The column this element the top of
        max_internals_in_col    Value for the maximum moment and axial force in row and column
                                links down this column:
                                    {'row': {'moment', 'axial'},
                                     'col': {'moment', axial'}}
    """

    def __init__(self):
        self.column = 0
        self.max_internals_in_col = {'row': {'moment': 0.0,
                                             'axial': 0.0},
                                     'col': {'moment': 0.0,
                                             'axial': 0.0}}

    def make_top_row_element(self, col):
        """Makes this element a top row element in the given column"""
        self.column = col

    def set_max_internals(self, max_internals, deterministic):
        """Sets the values of max_internals_in_col for this top row element """

        if deterministic:
            self.deterministic_max_internals(max_internals)
        else:
            self.stochastic_max_internals(max_internals)

    def link_criticalness_in_column(self, limits):
        """For a given top row element, this function calculates the criticalness of the links in
        the column to the left, and also of the column links

        Inputs:
            element     the element object of this top row element
            limits      the limits dictionary: 'moment', 'axial'
        Outputs:
            max_criticalness        maximum criticalness in links in this column:
                                    {'row': {'value', 'type'},
                                    'col': {'value', 'type'}}
        """

        max_criticalness = {'row': {'value': 0.0,
                                    'type': 'axial'},
                            'col': {'value': 0.0,
                                    'type': 'axial'}}

        # Update maximum criticalness in links
        for link_type in self.max_internals_in_col:
            for internals_type in self.max_internals_in_col[link_type]:
                criticalness_instance = calculate_criticalness(
                    self.max_internals_in_col[link_type][internals_type],
                    limits[internals_type])

                if criticalness_instance > max_criticalness[link_type]['value']:
                    max_criticalness[link_type]['value'] = criticalness_instance
                    max_criticalness[link_type]['type'] = internals_type

        return max_criticalness

    def stochastic_max_internals(self, max_internals):
        """Set max_internals_in_col for this top row element when using the stochastic version"""

        # Immediately update max_internals_in_col
        for link_type in max_internals:
            for internal_type in max_internals[link_type]:
                self.update_max_internals_in_col(internal_type, max_internals)

    def deterministic_max_internals(self, max_internals):
        """Set max_internals_in_col for this top row element when using the deterministic version"""

        for internal_type in max_internals['row']:

            # Update field in the top row element
            self.update_max_internals_in_col(internal_type, max_internals)

    def update_max_internals_in_col(self, internal_type, max_internals):
        """Actually updates the variables the maximum internals types"""

        for link_type in max_internals:
            self.max_internals_in_col[link_type][internal_type] = \
                max_internals[link_type][internal_type]
