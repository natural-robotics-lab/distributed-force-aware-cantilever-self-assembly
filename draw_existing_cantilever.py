"""A script draw graphs for existing cantilevers"""
import os
import numpy as np

import package.modelling.anamodel as ana
from package.basics import data_dir
from package.graphics.arrangements import ThreePlotVertical
from package.structures import Cantilever

FILENAME = os.path.join(data_dir(), 'Results', 'parallel', '139m_579a', '10delay', 
                        'Trial_1.pbz2')
STEP = 100
LIMITS = {'moment': 139,
          'axial': 579}


def main():
    """Main function"""

    # If loading a pbz2 file, we're loading a parallel trial, so there's more data than a sequential one
    parallel = bool('.pbz2' in FILENAME)

    # Load cantilever into e and types
    cantilever = Cantilever(mode='load_{}'.format(FILENAME.split('.')[-1]),
                            path=FILENAME,
                            step_no=STEP)

    print('Loaded cantilever: ' + FILENAME.split(data_dir())[1])

    print('Cantilever formed of {} elements and has length {} dm'.format(cantilever.no_elements,
                                                                         cantilever.length))


    [row_criticals, col_criticals, complete_link_internals,
        ss] = ana.calculate_internals(cantilever)

    for key, label, units in zip(['axial', 'moment'], ['axial force', 'bending moment'], ['N', 'Ndm']):

        row_link_max_index = np.argmax(row_criticals[key])
        print('Maximum {} in row links is {:.2f} {}'.format(label,
                                                            row_criticals[key][row_link_max_index],
                                                            units))

        col_link_max_index = np.argmax(col_criticals[key])
        print('Maximum {} in column links is {:.2f} {}'.format(label,
                                                               row_criticals[key][col_link_max_index],
                                                               units))

    figure = ThreePlotVertical(mode='manual', interactive=False,
                               units='dm', fonts='ieee_15',
                               margins='tight')

    if parallel:
        for element in cantilever.elements:
            # Set the figure object
            element.figure = figure
            # Plot
            element.plot()

        # Plot fixed support
        figure.structure_plot.plot_fixed_support(cantilever)
        # Set limits
        figure.structure_plot.auto_title(cantilever)
        figure.structure_plot.auto_limits(cantilever)

    else:
        figure.structure_plot.plot_structure(cantilever)

    figure.structure_plot.plot_link_criticalness(complete_link_internals, cantilever,
                                                 LIMITS, 'allowable', True)

    figure.moment_force_plots.plot_axial_and_moment(row_criticals, col_criticals,
                                                    LIMITS, ['allowable'],
                                                    cantilever.length,
                                                    cantilever.col0)

    figure.show_fig()
    print('End')


# Run main function
if __name__ == "__main__":
    main()
